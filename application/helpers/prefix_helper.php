<?php
	function get_prefix($table){
		$table = strtolower($table);
		switch ($table) {
		    case 'jabatan':
		    	return 'JBT';
		        break;
		    case 'tenaga_ahli':
		    	return 'TNA';
		        break;
		    case 'kinerja':
		    	return 'KNJ';
		        break;
		    case 'golongan':
		    	return 'GOL';
		        break;
		    case 'pendidikan':
		    	return 'PEN';
		        break;
		    case 'pegawai':
		    	return 'PEG';
		        break;
		    default:
		    	return 'TRX';
		}
	}