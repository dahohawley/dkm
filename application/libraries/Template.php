<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Template {
		var $template_data = array();
		function set($name, $value)
		{
			$this->template_data[$name] = $value;
		}
	
		function load($template = '', $view = '' , $view_data = array(), $return = FALSE)
		{               
			$this->CI =& get_instance();
			$this->set('contents', $this->CI->load->view($view, $view_data, TRUE));			
			return $this->CI->load->view($template, $this->template_data, $return);
		}
		function cardOpen($title = ''){
			return '<div class="card">
					<div class="card-header">
						'.$title.'
					</div>';
		}
		function cardBodyOpen(){
			return '<div class="card-body">';
		}
		function cardBodyClose(){
			return '</div>
				</div>';
		}
					

		function drawMenu($menu){
			$txt = '';
			if(is_array($menu)){
				foreach ($menu as $menu) {
					$txt .= '<li><a href="#"><i class="'.$menu['icon'].'"></i> '.$menu['parent'].'<span class="fa arrow"></span></a>';
						if(array_key_exists('list', $menu)){
							foreach ($menu['list'] as $list) {
							    $txt .= '<ul class="nav nav-second-level">
							        <li>
							            <a href="'.site_url($list['url']).'">'.$list['label'].'</a>
							        </li>
							    </ul>';
							}
						}

					$txt .= '</li>';
				}
				return $txt;
			}
		}
}