<div class="col-md-4 offset-md-4">
	<div class="card">
		<div class="card-header">
			<span class="fa fa-sign-in"></span> Login
		</div>
		<div class="card-body">
			<form action="#" class="form-horizontal" id="form-signIn">
				<div class="form-group row">
					<label class="control-label col-md-4">Username</label>
					<div class="col-md-8">
						<input type="text" class='form-control' name="username" id="username">
					</div>
				</div>
				<div class="form-group row">
					<label class="control-label col-md-4">Password</label>
					<div class="col-md-8">
						<input type="password" class='form-control' name="password" id="password">
					</div>
				</div>
				<div class="form-group row">
					<label class="control-label col-md-4"></label>
					<div class="col-md-8">
						<button class="btn btn-primary" id="button-signIn">
							<span class="fa fa-sign-in"></span> Sign in
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$("#form-signIn").submit(function(e){
			e.preventDefault();
			$.ajax({
				url:"<?php echo site_url('rbac/authentication/login')?>",
				type:"POST",
				data:$("#form-signIn").serialize(),
				beforeSend:function(res){
					buttonSpinner('button-signIn');
				},
				success:function(res){
					res = JSON.parse(res);
					if(res.code == 200){
						$("#button-signIn").html('<span class="fa fa-spinner fa-spin"></span> Success, Redirecting.....');
						location.reload();
					}else{
						custom_notification('danger',res.info);
						removeButtonSpinner('button-signIn');
					}
				},
				error:function(res){
					removeButtonSpinner('button-signIn');
					custom_notification('danger','Failed to sign in! Please try again.');
				}
			});
		});
	});
</script>