<?php
	// Test Connection 
	if(!$fp = @fsockopen("www.google.com", 80, $bum, $error, 5)){
		die('Require internet to install!');
	}
?>
<!DOCTYPE html>
<html>
<head>

	<style type="text/css">
		@keyframes bounce {
			 0%, 20%, 60%, 100% {
			   -webkit-transform: translateY(0);
			   transform: translateY(0);
			 }
			 40% {
			   -webkit-transform: translateY(-10px);
			   transform: translateY(-10px);
			 }
			 80% {
			   -webkit-transform: translateY(-5px);
			   transform: translateY(-5px);
			 }
		}
		.installationImage { 
			max-height: 200px;
			max-width: 200px;
			animation: bounce 1s;
			animation-iteration-count: infinite;
		}
	</style>
	<title>Izitechno Framework 	— Setup User</title>
	<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/library/jquery.validate/dist/jquery.validate.min.js') ?>"></script>
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link rel="shortcut icon" href="<?php echo base_url('assets/Framework/itfw_logo.png')?>" type="image/x-icon">
	<link rel="icon" href="<?php echo base_url('assets/Framework/itfw_logo.png')?>" type="image/x-icon">
</head>
<body>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
	   <a class="navbar-brand" href="#">
	    <img src="<?php echo base_url('assets/Framework/itfw_logo.png')?>" alt="Logo" style="width:50px;">
	   </a>
	   <a class="navbar-brand" href="#">Izitechno Framework Instalation</a>
	</nav>
	<br>
	<br>
	<br>
	<div class="container">
		<div class="row" id="rowInstallation">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						Instalation
					</div>
					<div class="card-body">
						<div id="alert-row"></div>
						<ul class="nav nav-tabs" id="myTab" role="tablist">
						    <li class="nav-item">
						        <a class="nav-link active" id="database-tab" data-toggle="tab" href="#database" role="tab" aria-controls="database" aria-selected="true">Database</a>
						    </li>
						    <li class="nav-item">
						        <a class="nav-link" id="account-tab" data-toggle="tab" href="#account" role="tab" aria-controls="account" aria-selected="false">Account</a>
						    </li>
						    <li class="nav-item">
						        <a class="nav-link" id="userInformation-tab" data-toggle="tab" href="#userInformation" role="tab" aria-controls="userInformation" aria-selected="false">User Information</a>
						    </li>
						</ul>
						<div class="tab-content" id="myTabContent">
				    		<!-- Database Setup -->
							    <div class="tab-pane fade show active" id="database" role="tabpanel" aria-labelledby="database-tab">
							    <form id="formDatabase" method="POST" action="#">
							    	<br><br>
							    	<div class="form-group row">
							    		<label class="control-label col-md-2 text-right">Host</label>
							    		<div class="col-md-10">
							    			<input type="text" class='form-control' value="localhost" name="host" id="host">
							    		</div>
							    	</div>
							    	<div class="form-group row">
							    		<label class="control-label col-md-2 text-right">Username</label>
							    		<div class="col-md-10">
							    			<input type="text" class='form-control' value="root" name="db-username" id="db-username">
							    		</div>
							    	</div>
							    	<div class="form-group row">
							    		<label class="control-label col-md-2 text-right">Password</label>
							    		<div class="col-md-10">
							    			<input type="text" class='form-control' value="" name="db-password" id="db-password">
							    		</div>
							    	</div>
							    	<div class="form-group row">
							    		<label class="control-label col-md-2 text-right">Schema Name</label>
							    		<div class="col-md-10">
							    			<input type="text" class='form-control' value="" name="schema_name" id="schema_name" required>
							    		</div>
							    	</div>
							    	<button class="btn btn-outline-primary pull-right" role="button">
							    		<span class="fa fa-arrow-right"></span> Account Setup
							    	</button>
							    	</center>
							    </form>
							    </div>
							<!-- Account Setup -->
							    <div class="tab-pane fade" id="account" role="tabpanel" aria-labelledby="account-tab">
							    	<br><br>
							    	<form method="POST" action="#" id="formAccount">
							    		<div class="form-group row">
							    			<label class="control-label col-md-2 text-right">Username</label>
							    			<div class="col-md-10">
							    				<input type="text" class='form-control' value="" name="username" id="username">
							    			</div>
							    		</div>
							    		<div class="form-group row">
							    			<label class="control-label col-md-2 text-right">Password</label>
							    			<div class="col-md-10">
							    				<input type="password" class='form-control' value="" name="password" id="password">
							    			</div>
							    		</div>
							    		<div class="form-group row">
							    			<label class="control-label col-md-2 text-right">Confirmation Password</label>
							    			<div class="col-md-10">
							    				<input type="password" class='form-control' name="confPass" id="confPass">
							    			</div>
							    		</div>
							    		<div class="form-group row">
							    			<label class="control-label col-md-2 text-right">Email</label>
							    			<div class="col-md-10">
							    				<input type="email" class='form-control' name="email" id="email">
							    			</div>
							    		</div>

								    	<button class="btn btn-outline-primary pull-right" role="button">
								    		<span class="fa fa-arrow-right"></span> User information
								    	</button>
							    	</form>
							    	<button class="btn btn-outline-primary pull-left" onclick="gotoDatabase()">
							    		<span class="fa fa-arrow-left"></span> Database Setup
							    	</button>
							    </div>
							<!-- User Information -->
								<br><br>
							    <div class="tab-pane fade" id="userInformation" role="tabpanel" aria-labelledby="userInformation-tab">
							    	<form id="form-userInformation" method="POST" action="#">
							    		<div class="form-group row">
							    			<label class="control-label col-md-2 text-right">First Name</label>
							    			<div class="col-md-10">
							    				<input type="text" class='form-control' name="first_name" id="first_name">
							    			</div>
							    		</div>
							    		<div class="form-group row">
							    			<label class="control-label col-md-2 text-right">Last Name</label>
							    			<div class="col-md-10">
							    				<input type="text" class='form-control' value="" name="last_name" id="last_name">
							    			</div>
							    		</div>
							    		<div class="form-group row">
							    			<label class="control-label col-md-2 text-right">Gender</label>
							    			<div class="col-md-10">
							    				<select name="gender" class="form-control" id="gender">
							    					<option value="m">Male</option>
							    					<option value="f">Female</option>
							    				</select>
							    			</div>
							    		</div>
							    		<div class="form-group row">
							    			<label class="control-label col-md-2 text-right">Birth date</label>
							    			<div class="col-md-10">
							    				<input type="date" class='form-control' name="birth_date" id="birth_date">
							    			</div>
							    		</div>
							    		<div class="form-group row">
							    			<label class="control-label col-md-2 text-right">Birth Place</label>
							    			<div class="col-md-10">
							    				<input type="text" class='form-control' value="" name="birth_place" id="birth_place">
							    			</div>
							    		</div>
							    		<button class="btn btn-outline-success pull-right">
							    			<span class="fa fa-check"></span> Finish
							    		</button>
							    	</form>
						    		<button class="btn btn-outline-primary pull-left" onclick="gotoAccount()">
						    			<span class="fa fa-arrow-left"></span> Account Setup
						    		</button>
							    </div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row" id="rowProgress" style="display: none;">
			<div class="col-md-3">
				<img src="<?php echo base_url('assets/Framework/logo.png')?>" alt="Logo" class="installationImage">
			</div>
			<div class="col-md-9">
				<h3>Installing : <i id="installationName"></i> </h3> <br>
				<div class="progress">
				  <div class="progress-bar" id="progressbar" style="width:0%">0%</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>

<script type="text/javascript">
	var currentPercentage = 0;
	var maxPercentage = 20;
	$("#account-tab").attr('class','nav-link disabled');
	$("#userInformation-tab").attr('class','nav-link disabled');

	$(document).ready(function(){
		$("#formDatabase").validate({
			rules : {
				"db-username" : "required",
				"host" : "required",
				"schema_name" : "required",
			},
			submitHandler:function(){
				$("#account-tab").attr('class','nav-link');
				$("#account-tab").trigger('click');
			},
			invalidHandler:function(){
				$("#account-tab").attr('class','nav-link disabled');
				$("#userInformation-tab").attr('class','nav-link disabled');
			}
		});

		$("#formAccount").validate({
			rules : {
				"username" : "required",
				"password" : "required",
				"confPass" : {
					required : true,
					equalTo: "#password"
				},
				"email" : "required",
			},
			messages : {
				"confPass" : {
					equalTo : "Confirmation Password didnt match"
				},
			},
			submitHandler:function(){
				$("#userInformation-tab").attr('class','nav-link');
				gotoUserInformation();
			},
			invalidHandler:function(){
				$("#userInformation-tab").attr('class','nav-link disabled');
			}
		});

		$("#form-userInformation").validate({
			rules : {
				"first_name" : "required",
				"last_name" : "required",
				"gender" : "required",
				"birth_date" : "required",
				"birth_place" : "required",
			},
			submitHandler:function(){
				$("#rowInstallation").fadeOut();
				$("#rowProgress").fadeIn();
				installDB();
			}
		});

	});

	function startProgress (percentage,installationName){
		$("#installationName").text(installationName);
		maxPercentage = percentage;
		time=setInterval(function(){
			if(currentPercentage<maxPercentage){
				currentPercentage+= 1;
				setPercentage(currentPercentage);
			}
		},1000);
	}
	function setPercentage(setPercentage){
		currentPercentage = setPercentage;
		$("#progressbar").text(setPercentage+'%');
		$('#progressbar').css('width', setPercentage+'%');
		$("#progressbar").attr('aria-valuenow="'+setPercentage+'"');
	}
	function installDB(){
		startProgress(30,'Installing Database');
		$.ajax({
			url:"<?php echo site_url('ITFW/installDB') ?>",
			data : $("#formDatabase").serialize(),
			type:"POST",
			success:function(res){
				res = JSON.parse(res);
				if(res.code == 200){
					console.log(res.info);
					createAccount();
				}else{
					if(res.code == 401){
						clearInterval(time);
						setPercentage(0);
						$("#alert-row").append('<div class="alert alert-danger">'+res.info+'</div>');
						$("#database-tab").trigger('click');
						$("#rowInstallation").fadeIn();
						$("#rowProgress").fadeOut();
					}
				}
			},
			error:function(){
				alert("Error installing framework. please try to drop database first!");
				location.reload();
			}
		});
	}
	function createAccount(){
		clearInterval(time);
		setPercentage(30);
		$("#database-tab").attr('class','nav-link disabled');
		startProgress(60,'Creating Account');
		$.ajax({
			url:"<?php echo site_url('ITFW/createUser') ?>",
			data : $("#formAccount").serialize(),
			type:"POST",
			success:function(res){
				res = JSON.parse(res);
				id_account =  res.data;
				createUser(id_account);
			}
		});
	}
	function createUser(){
		setPercentage(60);
		startProgress(80,'Creating User');
		$("#account-tab").attr('class','nav-link disabled');
		$.ajax({
			url:"<?php echo site_url('ITFW/createUserInformation') ?>",
			data : $("#form-userInformation").serialize(),
			type:"POST",
			success:function(res){
				res = JSON.parse(res);
				setPercentage(100);
				startProgress(100,'Finishing');
				location.reload();
			}
		});
	}

	function gotoDatabase(){
		$("#database-tab").trigger('click');
	}
	function gotoAccount(){
		$("#account-tab").trigger('click');
	}
	function gotoUserInformation(){
		$("#userInformation-tab").trigger('click');
	}
</script>
