<?php
	
	if(!$fp = @fsockopen("www.google.com", 80, $bum, $error, 5)){
		die('Require internet to install!');
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Izitechno Framework</title>
	<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>
	<link rel="shortcut icon" href="<?php echo base_url('assets/Framework/itfw_logo.png')?>" type="image/x-icon">
	<link rel="icon" href="<?php echo base_url('assets/Framework/itfw_logo.png')?>" type="image/x-icon">
</head>
<body>

	<div class="container">
		<br>
		<br>
		<br>
		<br>
		<br>
		<center>
			<h1>Initializing Framework</h1>
		</center>
		<br><br>
		<center id="statusText">Please Wait... </center>
		<div class="progress">
		  <div class="progress-bar" id="progressbar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>
		</div>
	</div>
</body>
</html>

<script type="text/javascript">
	setup();
	currentPercentage = 0;
	time=setInterval(function(){
		addPercentage(1);
		if(currentPercentage==90){
			clearInterval(time);
		}
	},1000);
	function addPercentage(percentage){
		currentPercentage += percentage;
		$("#progressbar").text(currentPercentage+'%');
		$('#progressbar').css('width', currentPercentage+'%');
		$("#progressbar").attr('aria-valuenow="'+currentPercentage+'"');
	}

	function setup(){
		$.ajax({
			url:"<?php echo site_url('ITFW/setup') ?>",
			success:function(res){
				res = res.split("___");
				res = JSON.parse(res[1]);
				if(res.code == 200){
					console.log(res.code);
					clearInterval(time);
					percentageLeft = 100 - currentPercentage;
					addPercentage(percentageLeft);
					$("#statusText").text("Success, Please wait... we will redirecting you to setup");
					setTimeout(function() {
						location.reload();
					}, 2000);
				}else{
					$("#statusText").text("Failed installing framework");
				}
			}
		});
	}
</script>