<?php
	echo $this->layout_model->drawHeader();
	echo $this->layout_model->drawFooter();
?>
<!-- As a link -->
<nav class="navbar navbar-light bg-light">
	<a class="navbar-brand" href="#">DKM</a>
</nav>
<br><br>
<div class="container">
	<div id="notif-row">
		<?php
			echo $this->session->flashdata('notif');
		?>
	</div>
	<?php
		echo $contents;
	?>
</div>
