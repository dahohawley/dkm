<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Donatur_model extends CI_Model {
	protected $_table = 'donatur';
	protected $_key;

	public function loadList($mode = 'LOAD_ALL',$params = array()){
		$recordsTotal 		= $this->db->count_all_results($this->_table);

		if(isset($params['search']) && $params['search']){
				$this->db->start_cache();

				$this->db->or_like('lower(nama_donatur)',strtolower($params['search']),'both');
				$this->db->or_like('lower(tanggal_lahir)',strtolower($params['search']),'both');
				$this->db->or_like('lower(tempat_lahir)',strtolower($params['search']),'both');
				$this->db->or_like('lower(jenis_kelamin)',strtolower($params['search']),'both');

				$this->db->stop_cache();
		}

		if(isset($params['pagination'])){
			$limit 			= $params['pagination']['limit'];
			$offset 		= $params['pagination']['offset'];
		}else{
			$limit 			= 0;
			$offset 		= 0;
		}

		if(isset($params['order'])){
			$this->db->order_by($params['order']['column'],$params['order']['dir']);
		}

		$data 						= $this->db->get($this->_table,$limit,$offset);
		$recordsFiltered 			= $this->db->count_all_results($this->_table);

		$result 					= new stdClass;
		$result->data 				= $data->result();
		$result->recordsTotal 		= $recordsTotal;
		$result->recordsFiltered 	= $recordsFiltered;

		return $result;
	}
	public function save( $params = array() ){
		$CI =& get_instance();
		$CI->load->model('core/general_model');

		if(isset($params['donatur_id']) && !empty($params['donatur_id'])){
			$donatur_id 				= $params['donatur_id'];

			$this->db->where('donatur_id',$donatur_id);
			$this->db->set($params);
			$this->db->update($this->_table);

			$resultData = array(
				'donatur_id' => $donatur_id,
			);
			$result = $CI->general_model->result(200,'Berhasil Merubah data.',$resultData);
		}else{
			unset($params['donatur_id']);
			$this->db->insert($this->_table,$params);

			$resultData = array(
				'insert_id' => $this->db->insert_id(),
			);
			$result = $CI->general_model->result(200,'Berhasil Menambah Data',$resultData);
		}
		return $result;
	}
	public function delete( $params = array() ){
		$this->db->where('donatur_id',$params['donatur_id']);
		$this->db->delete($this->_table);

		$CI =& get_instance();
		$CI->load->model('general_model');
		$result = $this->general_model->result(200,'Sukses Menghapus Data #'.$params['donatur_id']);
		return $result;
	}
	protected function getNewCode(){
		$q 						= $this->db->query("SELECT max(donatur_id) as lastCode from $this->_table")->row();
		$lastCode 				= $q->lastCode;
		$nextValue	 			= explode('-', $lastCode)[1]+1;
		$nextValue				= str_pad($nextValue, 6,'0',STR_PAD_LEFT);
		$prefix 				= 'DON-';
		$newCode 				= $prefix.$nextValue;
		return $newCode;
	}
}
