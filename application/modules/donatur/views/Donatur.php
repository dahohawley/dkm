<?php echo $this->template->cardOpen('Donatur'); ?>

<?php echo $this->template->cardBodyOpen(); ?>
<div class="btn-group" id="action_button">
	<button class="btn btn-primary" id="buttonAdd" onclick="addData()">
		<span class="fa fa-plus"></span> Tambah
	</button>
</div>

<table class="table table-hover table-bordered" id="table-donatur" style="width: 100%">
	<thead>
		<tr>
			<th class="text-center">Donatur Id</th>
			<th class="text-center">Nama Donatur</th>
			<th class="text-center">Tanggal Lahir</th>
			<th class="text-center">Tempat Lahir</th>
			<th class="text-center">Jenis Kelamin</th>
			<th class="text-center">Tanggal Donasi setiap Bulan</th>
			<th class="text-center">Aksi</th>
		</tr>
	</thead>
</table>
<?php echo $this->template->cardBodyClose(); ?>
<script type="text/javascript">
	let loadContact = (donatur_id) => {
		return new Promise((resolve, reject) => {
			$.ajax({
				url: "<?php echo site_url('contact/loadList') ?>",
				type: "POST",
				data: {
					ref_id: donatur_id,
					ref_code: "DONATUR"
				},
				success: function(res) {
					res = JSON.parse(res);
					resolve(res);
				}
			});
		});
	}

	$(document).ready(function() {
		$("#table-donatur").DataTable({
			processing: true,
			serverSide: true,
			ajax: {
				url: "<?php echo site_url('donatur/loadList') ?>",
				type: "POST"
			},
			columns: [{
					data: "donatur_id"
				},
				{
					data: "nama_donatur"
				},
				{
					data: "tanggal_lahir",
					render: function(data, type, row, meta) {
						return moment(data).format('DD MMMM YYYY');
					}
				},
				{
					data: "tempat_lahir"
				},
				{
					data: "jenis_kelamin"
				},
				{
					data: "tanggal_donasi"
				},
				{
					data: "aksi",
					render: function(data, type, row) {
						txt = '';
						txt += '<button class="btn btn-primary btn-xs edit"';
						txt += '	donatur_id = "' + row.donatur_id + '";'
						txt += '	nama_donatur = "' + row.nama_donatur + '"';
						txt += '	tanggal_lahir = "' + row.tanggal_lahir + '"';
						txt += '	tempat_lahir = "' + row.tempat_lahir + '"';
						txt += '	jenis_kelamin = "' + row.jenis_kelamin + '"';
						txt += '>';
						txt += '	<span class="fa fa-pencil-alt"></span> Ubah';
						txt += '</button> ';

						txt += '<button class="btn btn-danger btn-xs delete"';
						txt += '	donatur_id = "' + row.donatur_id + '";'
						txt += '	nama_donatur = "' + row.nama_donatur + '"';
						txt += '	tanggal_lahir = "' + row.tanggal_lahir + '"';
						txt += '	tempat_lahir = "' + row.tempat_lahir + '"';
						txt += '	jenis_kelamin = "' + row.jenis_kelamin + '"';
						txt += '> ';
						txt += '	<span class="fa fa-trash"></span> Hapus';
						txt += '</button> ';

						txt += '<button class="btn btn-info btn-xs contact"';
						txt += '	donatur_id = "' + row.donatur_id + '";'
						txt += '	nama_donatur = "' + row.nama_donatur + '"';
						txt += '	tanggal_lahir = "' + row.tanggal_lahir + '"';
						txt += '	tempat_lahir = "' + row.tempat_lahir + '"';
						txt += '	jenis_kelamin = "' + row.jenis_kelamin + '"';
						txt += '>';
						txt += '	<span class="fa fa-user"></span> Kontak';
						txt += '</button>';
						return txt;
					},
					className: "text-center"
				},
			],
		});
		$(document).on('click', '.edit', function() {
			donatur_id = $(this).attr('donatur_id');
			nama_donatur = $(this).attr('nama_donatur');
			tanggal_lahir = $(this).attr('tanggal_lahir');
			tempat_lahir = $(this).attr('tempat_lahir');
			jenis_kelamin = $(this).attr('jenis_kelamin');
			$("#donatur_id").val(donatur_id);
			$("#nama_donatur").val(nama_donatur);
			$("#tanggal_lahir").val(tanggal_lahir);
			$("#tempat_lahir").val(tempat_lahir);
			$("#jenis_kelamin").val(jenis_kelamin);
			$("#modal-addTitle").text('Ubah data #' + donatur_id);
			$("#modal-add").modal('show');
		});
		$(document).on('click', '.delete', function() {
			donatur_id = $(this).attr('donatur_id');
			nama_donatur = $(this).attr('nama_donatur');
			tanggal_lahir = $(this).attr('tanggal_lahir');
			tempat_lahir = $(this).attr('tempat_lahir');
			jenis_kelamin = $(this).attr('jenis_kelamin');

			conf = confirm("Apakah anda yakin ingin menghapus data #" + donatur_id + " ?");
			if (conf) {
				$.ajax({
					url: "<?php echo site_url('donatur/deleteData') ?>",
					type: "POST",
					data: {
						donatur_id: donatur_id
					},
					success: function(res) {
						res = JSON.parse(res);
						if (res.code == 200) {
							custom_notification('success', res.info);
							refreshTable();
						} else {
							custom_notification('danger', res.info);
						}
					}
				});
			}
		});
		$(document).on('click', '.contact', function() {
			donatur_id = $(this).attr('donatur_id');
			$("#addNewContact").data('contact-id', donatur_id);
			$("#row-contact").empty();
			loadContact(donatur_id).then((res) => {
				modal = $("#modal-contact");
				modal.modal('show');
				contactData = res.data;
				$.each(contactData, function(col, row) {
					txt = '';
					txt += '<div class="card text-left">';
					txt += '	<img class="card-img-top" src="holder.js/100px180/" alt="">';
					txt += '	<div class="card-body">';
					txt += '	<h4 class="card-title">';
					txt += '	<div class="row">';
					txt += '		<div class="col-md-6">';
					txt += row.contact_name + '&nbsp;<span class="badge badge-primary">' + row.relation_name + '</span>';
					txt += '		</div>';
					txt += '		<div class="col-md-6">';
					txt += '			<div class="float-right">';
					txt += '				<button class="btn btn-primary btn-sm add-detail-contact" data-contact-id="' + row.contact_id + '" >';
					txt += '					Tambah Detail';
					txt += '				</button>';
					txt += '			</div>';
					txt += '		</div>';
					txt += '	</div>';
					txt += '	</h4>';
					txt += '<hr>';
					txt += '	<table class="table table-hover table-bordered">';

					contactDetail = row.contact_detail;
					$.each(contactDetail, function(col, rowDetail) {
						txt += '		<tr>';
						txt += '			<th width="200">' + rowDetail.contact_type_name + '</th>';
						txt += '			<th width="400">' + rowDetail.values + '</th>';
						txt += '			<th class="text-center">';

						if (rowDetail.contact_type_id == 1) {
							txt += '				<button class="btn btn-info btn-sm sendWhatsapp" data-toggle="tooltip" data-values="' + rowDetail.values + '" title="Kirim Whatsapp" data-placement="top"><span class="fa fa-bell"></span></button>';
						}

						txt += '			</th>';
						txt += '		</tr>';
					});

					txt += '	</table>';
					txt += '	</div>';
					txt += '</div>';

					$("#row-contact").append(txt);
				});
				$("[data-toggle='tooltip']").tooltip();
			}).catch();
		});
		/* Handler onclick add contact */
		$(document).on('click', '.add-detail-contact', function() {
			$("#formAdd-detail")[0].reset();
			contact_id = $(this).data('contact-id');
			$(".modal").modal('hide');
			$("#formAdd-detail").find('#contact_detail_id').val(0);
			$("#formAdd-detail").find('#contact_id').val(contact_id);
			$("#modal-add-contact-detail").modal('show');
		});
		$(document).on('click', '.sendWhatsapp', function() {
			phoneNumber = $(this).data('values');
			text = "Assalamualaikum wr wb. Kami dari DKM ... ingin mengingatkan anda untuk melakukan donasi di masjid .. . terimakasih.";
				window.open('https://api.whatsapp.com/send?phone='+phoneNumber+'&text='+text, '_blank');
		});
		$("#table-donatur_filter").append($("#action_button"));
		$.validator.addMethod(
			'alpha_dash_space',
			function(value, element) {
				return value.match("^[a-zA-Z0-9 ]*$");
			},
			'Please insert only alphabet or Space'
		);

		$("#addNewContact").click(function() {
			contact_id = $("#addNewContact").data('contact-id');
			$(".modal").modal('hide');

			$("#formAddContact")[0].reset();
			$("#formAddContact").find('#ref_id').val(contact_id);
			$("#formAddContact").find('#ref_code').val("DONATUR");
			$("#formAddContact").find('#contact_id').val(0);

			$("#modal-add-contact").modal('show');
		});

		$("#formAdd").validate({
			rules: {
				donatur_id: {
					required: true,
				},
				nama_donatur: {
					required: true,
					alpha_dash_space: true,
				},
				tanggal_lahir: {
					required: true
				},
				tempat_lahir: {
					required: true,
					alpha_dash_space: true,
				},
				jenis_kelamin: {
					required: true,
				},
				tanggal_donasi: {
					min: 1,
					max: 28,
					required: true,
				}
			},
			submitHandler: function() {
				$.ajax({
					url: "<?php echo site_url('donatur/save') ?>",
					type: "POST",
					data: $("#formAdd").serialize(),
					beforeSend: function() {
						buttonSpinner('btnAddSave');
					},
					success: function(res) {
						res = JSON.parse(res);
						if (res.code == 200) {
							custom_notification('success', res.info);
							refreshTable();
						} else {
							custom_notification('danger', res.info);
						}
					},
					complete: function() {
						$(".modal").modal('hide');
						removeButtonSpinner('btnAddSave');
					}
				});
			}
		});

		$("#formAddContact").validate({
			rules: {
				contact_id: {
					required: true,
				},
				contact_name: {
					required: true,
				},
				ref_id: {
					required: true,
				}
			},
			submitHandler: function() {
				$.ajax({
					url: "<?php echo site_url('contact/save') ?>",
					type: "POST",
					data: $("#formAddContact").serialize(),
					beforeSend: function() {
						buttonSpinner('btnAddContact');
					},
					success: function(res) {
						res = JSON.parse(res);
						if (res.code == 200) {
							custom_notification('success', res.info);
							refreshTable();
						} else {
							custom_notification('danger', res.info);
						}
					},
					complete: function() {
						$(".modal").modal('hide');
						removeButtonSpinner('btnAddContact');
					}
				});
			}
		});
		/* Submit Contact Detail */
		$("#formAdd-detail").validate({
			rules: {
				contact: {
					required: true,
				},
				contact_detail_id: {
					required: true,
				},
				contact_type_id: {
					required: true,
				},
				values: "required"
			},
			submitHandler: function() {
				$.ajax({
					url: "<?php echo site_url('contact/contact_detail/save') ?>",
					type: "POST",
					data: $("#formAdd-detail").serialize(),
					beforeSend: function() {
						buttonSpinner('btn-save-detail');
					},
					success: function(res) {
						res = JSON.parse(res);
						if (res.code == 200) {
							custom_notification('success', res.info);
							refreshTable();
						} else {
							custom_notification('danger', res.info);
						}
					},
					complete: function() {
						$(".modal").modal('hide');
						removeButtonSpinner('btn-save-detail');
					}
				});
			}
		});
	});

	function refreshTable() {
		$("#table-donatur").DataTable().ajax.reload();
	}

	function addData() {
		$("#modal-addTitle").text('Tambah data baru');
		$("#donatur_id").val(0);
		$("#formAdd")[0].reset();
		$("#modal-add").modal('show');
	}
</script>

<div id="modal-add" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="modal-addTitle">Tambah Crud name</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<form method="POST" action="#" class="form-horizontal" id="formAdd">
					<input type="hidden" name="donatur_id" id="donatur_id">
					<div class="form-group row">
						<label class="control-label col-md-3">Nama Donatur</label>
						<div class="col-md-9">
							<input type="text" class='form-control' name="nama_donatur" id="nama_donatur">
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-3">Tanggal Lahir</label>
						<div class="col-md-9">
							<input type="date" class='form-control' name="tanggal_lahir" id="tanggal_lahir">
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-3">Tempat Lahir</label>
						<div class="col-md-9">
							<input type="text" class='form-control' name="tempat_lahir" id="tempat_lahir">
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-3">Jenis Kelamin</label>
						<div class="col-md-9">
							<select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
								<option value="Laki-Laki">Laki-Laki</option>
								<option value="Perempuan">Perempuan</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 col-form-label">Tanggal donasi setiap bulan</label>
						<div class="col-sm-9">
							<input type="number" class="form-control" id="tanggal_donasi" name="tanggal_donasi">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 col-form-label">No Telepon</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="no_telepon" name="no_telepon">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" onclick='$("#formAdd").submit()' id="btnAddSave">
					<span class="fa fa-check"></span> Simpan
				</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Modal Contact-->
<div class="modal fade" id="modal-contact" tabindex="-1" role="dialog" aria-labelledby="Contact Donatur" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Kontak Donatur</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row mb-3">
					<div class="col-md-12">
						<div class="float-right">
							<button class="btn btn-primary btn-sm" id="addNewContact" data-contact-id="">
								Tambah Kontak
							</button>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12" id="row-contact">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Modal add contact -->
<div class="modal fade" id="modal-add-contact" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Tambah Kontak Baru</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form method="POST" action="#" class="form-horizontal" id="formAddContact">
					<input type="hidden" name="contact_id" id="contact_id">
					<input type="hidden" name="ref_code" id="ref_code">
					<input type="hidden" name="ref_id" id="ref_id" class="form-control">

					<div class="form-group row">
						<label class="control-label col-md-3">Contact Name</label>
						<div class="col-md-9">
							<input type="text" class='form-control' name="contact_name" id="contact_name">
						</div>
					</div>

					<div class="form-group row">
						<label class="control-label col-md-3">Relation</label>
						<div class="col-md-9">
							<select name="contact_relation_type_id" id="contact_relation_type_id" class="form-control">
								<?php
								foreach ($listRelation->data as $data) { ?>
									<option value="<?php echo $data->contact_relation_type_id ?>"><?php echo $data->relation_name ?></option>
								<?php
								}
								?>
							</select>
						</div>
					</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button class="btn btn-primary" id="btnAddContact">Save</button>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-add-contact-detail" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Tambah detail kontak</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<!-- Form Contact detail -->
				<form method="POST" action="#" class="form-horizontal" id="formAdd-detail">
					<input type="hidden" name="contact_detail_id" id="contact_detail_id">
					<input type="hidden" class="form-control" name="contact_id" id="contact_id">

					<div class="form-group row">
						<label class="control-label col-md-3">Contact Type</label>
						<div class="col-md-9">
							<select class="form-control" name="contact_type_id" id="contact_type_id">
								<?php
								foreach ($contactType->data as $data) { ?>
									<option value="<?php echo $data->contact_type_id ?>"><?php echo $data->contact_type_name ?></option>
								<?php
								}
								?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-3">Values</label>
						<div class="col-md-9">
							<input type="text" class='form-control' name="values" id="values">
						</div>
					</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button class="btn btn-primary" id="btn-save-detail">Save</button>
				</form>
			</div>
		</div>
	</div>
</div>