<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	include_once APPPATH . '/modules/core/controllers/Controller.php';

	class donatur extends Controller{

		public function __construct(){
			parent::__construct();
		}
		public function index(){
			// dipake untuk namain card
			$this->using('datatable');
			$this->using('jquery.validate');
			$this->using('moment');

			$this->load->model('contact/Contact_relation_type_model');
			$this->load->model('contact/Contact_type_model', 'contact_type');

			$listRelation 		= $this->Contact_relation_type_model->loadList();
			$contactType = $this->contact_type->loadList();
			
			$this->template->load(DEF_TEMPLATE_INSIDE,'Donatur',get_defined_vars());	
		}
		public function loadList(){
			$this->load->model('core/general_model');
			$result = $this->general_model->result();
			$params = $this->input->post();
			$dataTable = false;
			if(isset($params['draw'])){
				$dataTable = true;
				$search = $params['search']['value'];
				$pagination = array(
					'limit' => $params['length'],
					'offset' => $params['start']
				);

				$ordering = array(
					'column' => $params['columns'][ $params['order'][0]['column'] ]['data'],
					'dir' => $params['order'][0]['dir']
				);

				$loadParams = array(
					'search' => $search,
					'pagination' => $pagination,
					'order' => $ordering
				);

			}else{
				$loadParams = array();
			}
			$this->load->model('donatur_model','model');
			$data = $this->model->loadList('LOAD_ALL',$loadParams);
 	 		if($dataTable){
 	 			$result = array(
 	 				'draw' => $params['draw'],
 	 				'recordsTotal' => $data->recordsTotal,
 	 				'recordsFiltered' => $data->recordsFiltered,
 	 				'data' => $data->data
 	 			);
 	 		}else{
 	 			$result->data = $data;
 	 		}
 	 		echo json_encode($result);
 	 	}
		public function save(){
			$params = $this->input->post();


			$this->form_validation->set_data($params);
			$this->form_validation->set_rules('nama_donatur','Nama Donatur','required|callback_alpha_dash_space');
			$this->form_validation->set_rules('tanggal_lahir','Tanggal Lahir','required');
			$this->form_validation->set_rules('tempat_lahir','Tempat Lahir','required');
			$this->form_validation->set_rules('jenis_kelamin','Jenis Kelamin','required');

			if ($this->form_validation->run() == TRUE) {
				/* Save Donatur */
				$this->load->model('donatur_model');
				$dataDonatur = $params;
				unset($dataDonatur['no_telepon']);
				$result = $this->donatur_model->save($dataDonatur);
				$donatur_id 	= $result->data['insert_id'];

				/* Create Contact */
				$this->load->model('contact/contact_model','contact');
				$dataContact 	= array(
					'ref_id' 					=> $donatur_id,
					'contact_name' 				=> $params['nama_donatur'],
					'contact_relation_type_id' 	=> 1,
					'ref_code' 					=> 'DONATUR'
				);
				$result 		= $this->contact->save($dataContact);
				$contact_id 	= $result->data['insert_id'];

				/* Create contact detail */
				$this->load->model('contact/contact_detail_model','contact_detail');
				$dataDetail 	= array(
					'contact_id' 		=> $contact_id,
					'contact_type_id' 	=> 1,
					'values' 			=> $params['no_telepon'],
					'created_dtm' 		=> date('Y-m-d') 	
				);
				$result 		= $this->contact_detail->save($dataDetail);
				
			} else {
				$this->load->model('general_model');
				$result = $this->general_model->result(401,validation_errors());
			}
			echo json_encode($result);
		}
		public function deleteData(){
 			$params = $this->input->post();
				$this->load->model('donatur_model');
 			$result = $this->donatur_model->delete($params);
 			echo json_encode($result);
 		}
	 	function alpha_dash_space($fullname){
	 	    if (! preg_match('/^[a-zA-Z\s]+$/', $fullname)) {
	 	        $this->form_validation->set_message('alpha_dash_space', '%s Hanya bisa diisi oleh huruf dan spasi.');
	 	        return FALSE;
	 	    } else {
	 	        return TRUE;
	 	    }
	 	}
}