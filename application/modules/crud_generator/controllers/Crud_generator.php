<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . '/modules/core/controllers/Controller.php';

class Crud_generator extends Controller{
	CONST TEST_MODE = TRUE;

	public function index(){
		$this->using('jquery.validate');
		$this->template->load(DEF_TEMPLATE_INSIDE,'index',get_defined_vars());
	}
	public function saveCrud(){
		$params = $this->input->post();
		$this->load->model('Table_factory');
		$this->Table_factory->initTableFactory($params,self::TEST_MODE);
		$result = $this->Table_factory->createTable($params);

		if($result->code == 200){
			$this->load->model('Generator_model');

			$result = $this->Generator_model->prepareDir($params['crud_name']);
			if($result->code == 200){
				$this->load->model('Controller_factory');
				$this->load->model('model_factory');
				$this->load->model('view_factory');

				$this->Controller_factory->createFile($params);
				$this->model_factory->createModel($params);
				$this->view_factory->createFile($params);
			}
		}
		echo json_encode($result);
		return true;
	}
}	