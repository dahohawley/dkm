<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Javascript_model extends CI_Model {

	private $_script;
	private $_jQuery;

	public function getScript($params){
		$this->openScript();
		
		$this->jQueryOpen();

		$this->initDataTable($params);
		$this->handlerEdit($params);
		$this->handlerDelete($params);
		$this->saveHandler($params);

		$this->jQueryClose();
		$this->_script.= $this->_jQuery;

		$this->addDataHandler($params);

		$this->closeScript();
		

		return $this->_script;
	}


	// DataTable
	public function initDataTable($params){
		$crudName 		= $params['crud_name'];

		$this->_jQuery .="		$(\"#table-".$params['crud_name']."\").DataTable({\n";
		$this->_jQuery .="			processing:true,\n";
		$this->_jQuery .="			serverSide:true,\n";
		$this->_jQuery .="			ajax : {\n";
		$this->_jQuery .="				url:\"<?php echo site_url('$crudName/loadList') ?>\",\n";
		$this->_jQuery .="				type:\"POST\"\n";
		$this->_jQuery .="			},\n";
		$this->_jQuery .="			columns :[\n";
		$this->_jQuery .="				{data:\"".$params['primary_key']."\"},\n";

		foreach ($params['columnName'] as $key => $columnName) {
			switch ($params['column-type'][$key]) {
				case 'text':
					$this->_jQuery .="				".$this->columnTypeText($columnName)."\n";
					break;
				case 'int':
					$this->_jQuery .="				".$this->columnTypeText($columnName)."\n";
					break;
				case 'int_rp':
					$this->_jQuery .="				".$this->columnTypeMoney($columnName)."\n";
					break;
				case 'date':
					$this->_jQuery .="				".$this->columnTypeDate($columnName)."\n";
					break;
			}
		}

		$this->_jQuery.="			{\n";
		$this->_jQuery.="				data:\"aksi\",\n";
		$this->_jQuery.="				render:function(data,type,row){\n";
		$this->_jQuery.="					txt = '';\n";
		$this->_jQuery.="					txt += '<button class=\"btn btn-primary btn-xs edit\"';\n";
		$this->_jQuery.="					txt += '	".$params['primary_key']." = \"'+row.".$params['primary_key']."+'\";'\n";

		foreach ($params['columnName'] as $key => $columnName) {
			$this->_jQuery.="					txt += '	$columnName = \"'+row.$columnName+'\"';\n";	
		}

		$this->_jQuery.="					txt += '>';\n";
		$this->_jQuery.="					txt += '	<span class=\"fa fa-pencil-alt\"></span> Ubah';\n";
		$this->_jQuery.="					txt += '</button> ';\n\n";
		$this->_jQuery.="					txt += '<button class=\"btn btn-danger btn-xs delete\"';\n";
		$this->_jQuery.="					txt += '	".$params['primary_key']." = \"'+row.".$params['primary_key']."+'\";'\n";
		
		foreach ($params['columnName'] as $key => $columnName) {
			$this->_jQuery.="					txt += '	$columnName = \"'+row.$columnName+'\"';\n";	
		}

		$this->_jQuery.="					txt += '>';\n";	
		$this->_jQuery.="					txt += '	<span class=\"fa fa-trash\"></span> Hapus';\n";	
		$this->_jQuery.="					txt += '</button>';\n";	
		$this->_jQuery.="					return txt;\n";	
		$this->_jQuery.="				}\n";	
		$this->_jQuery.="			},\n";	

		$this->_jQuery .="			],\n";

		$this->_jQuery .= "		});\n\n";
	}
	public function columnTypeText($columnName){
		return "{data:\"$columnName\"},";
	}
	public function columnTypeMoney($columnName){
		return "{
					data:\"$columnName\",
					render: function(data,type,row,meta){
						return format_rp(data);
					}
				},";
	}
	public function columnTypeDate($columnName){
		return "{
					data:\"$columnName\",
					render: function(data,type,row,meta){
						return moment(data).format('DD MMMM YYYY');
					}
				},";
	}
	public function handlerEdit($params){
		$this->_jQuery.="		\$(document).on('click','.edit',function(){\n";

		$this->_jQuery .= "			".$params['primary_key']." 			= \$(this).attr('".$params['primary_key']."');\n";

		foreach ($params['columnName'] as $key => $columnName) {
			$this->_jQuery .= "			$columnName 			= \$(this).attr('$columnName');\n";	
		}

			/*attribute
				$("#id").val(id);
			*/
		$this->_jQuery .= "			$(\"#".$params['primary_key']."\").val(".$params['primary_key'].");\n";

		foreach ($params['columnName'] as $key => $columnName) {
			$this->_jQuery .= "			$(\"#$columnName\").val($columnName);\n";
		}

		$this->_jQuery.="			\$(\"#modal-addTitle\").text('Ubah data #'+".$params['primary_key'].");\n";
		$this->_jQuery.="			\$(\"#modal-add\").modal('show');\n";
		$this->_jQuery.="		});\n\n";
	}
	public function saveHandler($params){

		$this->_jQuery 	.= "		$(\"#formAdd\").validate({\n";
		$this->_jQuery 	.= "			rules : {\n";

		$this->_jQuery 	.= "				".$params['primary_key'].": {\n";
		$this->_jQuery 	.= "					required : true,\n";
		$this->_jQuery 	.= "				},\n"; 

		/* generating validation */
		foreach ($params['columnName'] as $key => $columnName) {
			if(isset($params['validation-'.$key]) && !empty($params['validation-'.$key])){
				$this->_jQuery 	.= "				".$columnName.": {\n";

				// ini untuk yang validasi ceklis dari formnya
				foreach ($params['validation-'.$key] as  $rules) {
					if($rules != 'required'){
						// belum tau gimana caranya alpha dash space
					}else{
						$this->_jQuery .= "					".$rules.":"."true,\n";
					}


				}

				if($params['min-length-'.$key]){
					$this->_jQuery .= "					minlength:".$params['min-length-'.$key].",\n";
				}
				if($params['max-length-'.$key]){
					$this->_jQuery .= "					maxlength:".$params['max-length-'.$key].",\n";
				}

				// ini untuk max & min length

				$this->_jQuery 	.= "				},\n"; 
			}
		}

		$this->_jQuery 	.= "			},\n";
		$this->_jQuery 	.= "			submitHandler : function(){\n";
		$this->_jQuery 	.= "				$.ajax({\n";
		$this->_jQuery 	.= "					url:\"<?php echo site_url('".$params['crud_name']."/save') ?>\",\n";
		$this->_jQuery 	.= "					type : \"POST\",\n";
		$this->_jQuery 	.= "					data : $(\"#formAdd\").serialize(),\n";
		$this->_jQuery 	.= "					beforeSend:function(){\n";
		$this->_jQuery 	.= "						buttonSpinner('btnAddSave');\n";
		$this->_jQuery 	.= "					},\n";
		$this->_jQuery 	.= "					success:function(res){\n";
		$this->_jQuery 	.= "						res = JSON.parse(res);\n";
		$this->_jQuery 	.= "						if(res.code == 200){\n";
		$this->_jQuery 	.= "							custom_notification('success',res.info);\n";
		$this->_jQuery 	.= "							refreshTable();\n";
		$this->_jQuery 	.= "						}else{\n";
		$this->_jQuery 	.= "							custom_notification('danger',res.info);\n";
		$this->_jQuery 	.= "						}\n";
		$this->_jQuery 	.= "					},\n";
		$this->_jQuery 	.= "					complete:function(){\n";
		$this->_jQuery 	.= "						$(\".modal\").modal('hide');\n";
		$this->_jQuery 	.= "						removeButtonSpinner('btnAddSave');\n";
		$this->_jQuery 	.= "					}\n";
		$this->_jQuery 	.= "				});\n";
		$this->_jQuery 	.= "			}\n";
		$this->_jQuery 	.= "		});\n";
	}
	public function handlerDelete($params){
		$this->_jQuery .= "		$(document).on('click','.delete',function(){\n";

		$this->_jQuery .= "			".$params['primary_key']." 			= \$(this).attr('".$params['primary_key']."');\n";

		foreach ($params['columnName'] as $key => $columnName) {
			$this->_jQuery .= "			$columnName 			= \$(this).attr('$columnName');\n";	
		}

		$this->_jQuery .="\n			conf = confirm(\"Apakah anda yakin ingin menghapus data #\"+".$params['primary_key']."+\" ?\");\n";
		$this->_jQuery .= "			if(conf){\n";
		$this->_jQuery .= "				\$.ajax({\n";
		$this->_jQuery .= "					url:\"<?php echo site_url('".$params['crud_name']."/deleteData') ?>\",\n";
		$this->_jQuery .= "					type :\"POST\",\n";
		$this->_jQuery .= "					data : {\n";
		$this->_jQuery .= "						".$params['primary_key']." : ".$params['primary_key']."\n";
		$this->_jQuery .= "					},\n";
		$this->_jQuery .= "					success:function(res){\n";
		$this->_jQuery .= "						res = JSON.parse(res);\n";
		$this->_jQuery .= "						if(res.code == 200){\n";
		$this->_jQuery .= "							custom_notification('success',res.info);\n";
		$this->_jQuery .= "							refreshTable();\n";
		$this->_jQuery .= "						}else{\n";
		$this->_jQuery .= "							custom_notification('danger',res.info);\n";
		$this->_jQuery .= "						}\n";
		$this->_jQuery .= "					}\n";
		$this->_jQuery .= "				});\n";
		$this->_jQuery .= "			}\n";
		$this->_jQuery .= "		});\n\n";
		$this->_jQuery .= " 		\$(\"#table-".$params['crud_name']."_filter\").append(\$(\"#action_button\"));\n\n";
	}
	public function addDataHandler($params){
		$this->_script .= 	"	function refreshTable(){\n";
		$this->_script .= 	"		$(\"#table-".$params['crud_name']."\").DataTable().ajax.reload();\n";
		$this->_script .= 	"	}\n";
		$this->_script .= 	"	function addData(){\n";
		$this->_script .= 	"		\$(\"#modal-addTitle\").text('Tambah data baru');\n";
		$this->_script .= 	"		$(\"#".$params['primary_key']."\").val(0);\n";
		$this->_script .= 	"		$(\"#formAdd\")[0].reset();\n";
		$this->_script .= 	"		$(\"#modal-add\").modal('show');\n";
		$this->_script .= 	"	}\n";
	}

	// basic frame
		public function jQueryOpen(){
			$this->_jQuery.= "	\$(document).ready(function(){\n";
		}

		public function jQueryClose(){
			$this->_jQuery.= "	});\n";
		}

		public function openScript(){
			$this->_script	.= "<script type=\"text/javascript\">\n";
		}

		public function closeScript(){
			$this->_script 	.= "</script>\n\n";
		}

}

/* End of file Javascript_model.php */
/* Location: ./application/modules/crudGenerator/models/Javascript_model.php */