<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Generator_model extends CI_Model {

	const MODULE_DIR = APPPATH.'modules';

	public function prepareDir($crudName){
		$folderName 		= strtolower($crudName);
		$crudName 			= ucfirst($crudName);
		
		$CI =& get_instance();
		$CI->load->model('general_model');
		$result = $this->general_model->result();
		if(!is_dir(self::MODULE_DIR."/$folderName")){
			mkdir(self::MODULE_DIR."/$folderName");
			mkdir(self::MODULE_DIR."/$folderName/controllers");
			mkdir(self::MODULE_DIR."/$folderName/models");
			mkdir(self::MODULE_DIR."/$folderName/views");

			// create file
			fopen(self::MODULE_DIR."/$folderName/controllers/".$crudName.'.php','w');
			fopen(self::MODULE_DIR."/$folderName/models/".$crudName.'_model.php','w');
			fopen(self::MODULE_DIR."/$folderName/views/".$crudName.'.php','w');
		}else{
			$result->code = 401;
			$result->info = 'Modules with the same name already exist';
		}
		return $result;
	}	

}

/* End of file generatorModel.php */
/* Location: ./application/modules/crudGenerator/models/generatorModel.php */