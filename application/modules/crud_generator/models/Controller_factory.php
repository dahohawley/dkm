<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_factory extends CI_Model {
	private $_controller = '';
	private $_crudName = '';

	public function createFile($params){
		$this->_crudName = $params['crud_name'];
		$this->initController($params);
		$this->createLoadList();
		$this->createSave($params);
		$this->createDelete();
		$this->additionalValidation();
		$this->_controller .= '}';

		$controllerFile = fopen(APPPATH."modules/".$params['crud_name']."/controllers/".ucfirst($params['crud_name']).".php", "w") or die("Unable to open file!");

		fwrite($controllerFile, $this->_controller);
	}
	public function initController($params){
		$this->_controller .= "<?php\n";
		$this->_controller .= "	defined('BASEPATH') OR exit('No direct script access allowed');\n\n";
		$this->_controller .= "	include_once APPPATH . '/modules/core/controllers/Controller.php';\n\n";
		$this->_controller .= "	class ".$params['crud_name']." extends Controller{\n\n";
		$this->_controller .= "		public function __construct(){\n";
		$this->_controller .= "			parent::__construct();\n";
		$this->_controller .= "		}\n\n";
		$this->_controller .= "		public function index(){\n";
		$this->_controller .= "			// dipake untuk namain card\n";
		$this->_controller .= "			\$this->using('datatable');\n";
		$this->_controller .= "			\$this->using('jquery.validate');\n";
		$this->_controller .= "			\$this->using('moment');\n";
		$this->_controller .= "			\$this->template->load(DEF_TEMPLATE_INSIDE,'".ucfirst($params['crud_name'])."',get_defined_vars());	\n";
		$this->_controller .= "		}\n\n";
	}
	public function createLoadList(){
		$this->_controller .= "		public function loadList(){\n";
		$this->_controller .= "			\$this->load->model('core/general_model');\n";
		$this->_controller .= "			\$result = \$this->general_model->result();\n";
		$this->_controller .= "			\$params = \$this->input->post();\n";
		$this->_controller .= "			\$dataTable = false;\n";
		$this->_controller .= "			if(isset(\$params['draw'])){\n";
		$this->_controller .= "				\$dataTable = true;\n";
		$this->_controller .= "				\$search = \$params['search']['value'];\n";
		$this->_controller .= "				\$pagination = array(\n";
		$this->_controller .= "					'limit' => \$params['length'],\n";
		$this->_controller .= "					'offset' => \$params['start']\n";
		$this->_controller .= "				);\n\n";

		$this->_controller .= "				\$ordering = array(\n";
		$this->_controller .= "					'column' => \$params['columns'][ \$params['order'][0]['column'] ]['data'],\n";
		$this->_controller .= "					'dir' => \$params['order'][0]['dir']\n";
		$this->_controller .= "				);\n\n";
				
		$this->_controller .= "				\$loadParams = array(\n";
		$this->_controller .= "					'search' => \$search,\n";
		$this->_controller .= "					'pagination' => \$pagination,\n";
		$this->_controller .= "					'order' => \$ordering\n";
		$this->_controller .= "				);\n\n";

		$this->_controller .="			}else{\n";
		$this->_controller .="				\$loadParams = array();\n";
		$this->_controller .="			}\n";
		$this->_controller .="			\$this->load->model('".$this->_crudName."_model','model');\n";
		$this->_controller .="			\$data = \$this->model->loadList('LOAD_ALL',\$loadParams);\n";

		$this->_controller .=" 	 		if(\$dataTable){\n";
		$this->_controller .=" 	 			\$result = array(\n";
		$this->_controller .=" 	 				'draw' => \$params['draw'],\n";
		$this->_controller .=" 	 				'recordsTotal' => \$data->recordsTotal,\n";
		$this->_controller .=" 	 				'recordsFiltered' => \$data->recordsFiltered,\n";
		$this->_controller .=" 	 				'data' => \$data->data\n";
		$this->_controller .=" 	 			);\n";
		$this->_controller .=" 	 		}else{\n";
		$this->_controller .=" 	 			\$result->data = \$data;\n";
		$this->_controller .=" 	 		}\n";
		$this->_controller .=" 	 		echo json_encode(\$result);\n";
		$this->_controller .=" 	 	}\n";
	}
	public function createSave($params){
		$this->_controller .= "		public function save(){\n";
		$this->_controller .= "			\$params = \$this->input->post();\n\n";

		/*
			sanitizing rupiah dbawah ini
			$params['xn1'] = format_angka($params['xn1']); 
		*/

		foreach ($params['column-type'] as $key => $columnType) {
			if($columnType === 'int_rp'){
				$this->_controller .= "			\$params['".$params['columnName'][$key]."'] = format_angka(\$params['".$params['columnName'][$key]."']);\n";
			}
		}
		$this->_controller .= "\n";

		$this->_controller .= "			\$this->form_validation->set_data(\$params);\n";

		foreach ($params['columnName'] as $key => $columnName) {
			$validation = array();
			if(isset($params['validation-'.$key])){
				foreach ($params['validation-'.$key] as $choosenValidation) {
					$validation[] = $choosenValidation;
				}
			}
			if(!empty($params['min-length-'.$key])){
				$validation[] = "min_length[".$params['min-length-'.$key]."]";
			}
			if(!empty($params['max-length-'.$key])){
				$validation[] = "max_length[".$params['max-length-'.$key]."]";
			}
			$validation = implode('|', $validation);
			if($validation){
				$parsedColumnName = $this->trueColumnName($columnName);

				$this->_controller .= "			\$this->form_validation->set_rules('$columnName','$parsedColumnName','$validation');\n";
			}
		}

		$this->_controller .="\n\n";

		$this->_controller .= "			if (\$this->form_validation->run() == TRUE) {\n";
		$this->_controller .= "				\$this->load->model('".$this->_crudName."_model');\n";
		$this->_controller .= "				\$result = \$this->".$this->_crudName."_model->save(\$params);\n\n";
			
		$this->_controller .= "			} else {\n";

		$this->_controller .= "				\$this->load->model('general_model');\n";
		$this->_controller .= "				\$result = \$this->general_model->result(401,validation_errors());\n";

		$this->_controller .= "			}\n";
		$this->_controller .= "			echo json_encode(\$result);\n";
		$this->_controller .= "		}\n";
	}
	public function createDelete(){
		$this->_controller .= "		public function deleteData(){\n";
		$this->_controller .= " 			\$params = \$this->input->post();\n";
		$this->_controller .= "				\$this->load->model('".$this->_crudName."_model');\n";
		$this->_controller .= " 			\$result = \$this->".$this->_crudName."_model->delete(\$params);\n";
		$this->_controller .= " 			echo json_encode(\$result);\n";
		$this->_controller .= " 		}\n\n";
	}
	public function additionalValidation(){
		$this->_controller .= "	 	function alpha_dash_space(\$fullname){\n";
		$this->_controller .= "	 	    if (! preg_match('/^[a-zA-Z\s]+$/', \$fullname)) {\n";
		$this->_controller .= "	 	        \$this->form_validation->set_message('alpha_dash_space', '%s Hanya bisa diisi oleh huruf dan spasi.');\n";
		$this->_controller .= "	 	        return FALSE;\n";
		$this->_controller .= "	 	    } else {\n";
		$this->_controller .= "	 	        return TRUE;\n";
		$this->_controller .= "	 	    }\n";
		$this->_controller .= "	 	}\n\n";
	}


	

	// tools
		public function trueColumnName($columnName){
			return ucwords(str_replace('_', ' ', $columnName));
		}
		public function columnName($columnName){
			return strtolower(str_replace(' ', '_', $columnName));
		}

}

/* End of file controllerFactory.php */
/* Location: ./application/modules/crudGenerator/models/controllerFactory.php */