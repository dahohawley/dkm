<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class View_factory extends CI_Model {

	protected $_view;
	protected $_crudName = '';


	public function createFile($params){
		$this->_crudName = $params['crud_name'];

		// generating table
		$this->generateTable($params);

		// creating javascript
		$CI 		=& get_instance();
		$CI->load->model('javascript_model');
		$script 	= $this->javascript_model->getScript($params);
		$this->_view .= $script;

		// creating modal
		$this->generateModal($params);



		$viewFile = fopen(APPPATH."modules/".$params['crud_name']."/views/".ucfirst($params['crud_name']).".php", "w") or die("Unable to open file!");
		fwrite($viewFile, $this->_view);
	}	

	public function generateTable($params){
		$rowName	 	= ucwords(str_replace('_', ' ', $this->_crudName));
		$this->_view.="<?php echo \$this->template->cardOpen('$rowName');?>\n\n";

		$this->_view.="<?php echo \$this->template->cardBodyOpen();?>\n";
		$this->_view.="	<div class=\"btn-group\" id=\"action_button\">\n";
		$this->_view.="		<button class=\"btn btn-primary\" id=\"buttonAdd\" onclick=\"addData()\">\n";
		$this->_view.="			<span class=\"fa fa-plus\"></span> Tambah\n";
		$this->_view.="		</button>\n";
		$this->_view.="	</div>\n\n";

		$this->_view.="	<table class=\"table table-hover table-bordered\" id=\"table-".$params['crud_name']."\" style=\"width: 100%\">\n";
		$this->_view.="		<thead>\n";
		$this->_view.="			<tr>\n";
		$this->_view	.= "				<th class=\"text-center\">".ucwords(str_replace('_', ' ', $params['primary_key']))."</th>\n";

		foreach ($params['columnName'] as $columnName) {
			$tableHeader 	= ucwords(str_replace('_', ' ', $columnName));
			$this->_view	.= "				<th class=\"text-center\">$tableHeader</th>\n";
		}

		$this->_view	.= "				<th class=\"text-center\">Aksi</th>\n";

		$this->_view.="			</tr>\n";
		$this->_view.="		</thead>\n";
		$this->_view.="	</table>\n";
		$this->_view.="<?php echo \$this->template->cardBodyClose();?>\n";
	}

	public function generateModal($params){
		$this->_view		.= "<div id=\"modal-add\" class=\"modal fade\" role=\"dialog\">\n";
		$this->_view		.= "    <div class=\"modal-dialog\">\n";
		$this->_view		.= "        <!-- Modal content-->\n";
		$this->_view		.= "        <div class=\"modal-content\">\n";
		$this->_view		.= "            <div class=\"modal-header\">\n";
		$this->_view		.= "                <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n";
		$this->_view		.= "                <h4 class=\"modal-title\" id=\"modal-addTitle\">Tambah Crud name</h4>\n";
		$this->_view		.= "            </div>\n";
		$this->_view		.= "            <div class=\"modal-body\">\n";
		$this->_view		.= "            	<form method=\"POST\" action=\"#\" class=\"form-horizontal\" id=\"formAdd\">\n";
		$this->_view 		.= " 					<input type=\"hidden\" name=\"".$params['primary_key']."\" id=\"".$params['primary_key']."\" >\n";
		foreach ($params['columnName'] as $key => $columnName) {

			$this->_view 	.=	"				<div class=\"form-group row\">\n";
			$this->_view 	.=	"					<label class=\"control-label col-md-3\">".ucwords(str_replace('_', ' ', $columnName))."</label>\n";
			$this->_view 	.=	"					<div class=\"col-md-9\">\n";

			switch ($params['column-type'][$key]) {
				case 'text':
					$this->_view 	.=	"						<input type=\"text\" class='form-control' name=\"$columnName\" id=\"$columnName\">\n";
					break;
				case 'int':
					$this->_view 	.=	"						<input type=\"number\" class='form-control' name=\"$columnName\" id=\"$columnName\">\n";
					break;
				case 'int_rp':
					$this->_view 	.=	"						<input type=\"text\" class='form-control rupiah' name=\"$columnName\" id=\"$columnName\">\n";
					break;
				case 'date':
					$this->_view 	.=	"						<input type=\"date\" class='form-control rupiah' name=\"$columnName\" id=\"$columnName\">\n";
					break;
				default:
					$this->_view 	.=	"						<input type=\"text\" class='form-control' name=\"$columnName\" id=\"$columnName\">\n";
					break;
			}


			$this->_view 	.=	"					</div>\n";
			$this->_view 	.=	"				</div>\n";
		}


		$this->_view		.= "            	</form>\n";
		$this->_view		.= "            </div>\n";
		$this->_view		.= "            <div class=\"modal-footer\">\n";
		$this->_view		.= "            	<button class=\"btn btn-primary\" onclick='$(\"#formAdd\").submit()' id=\"btnAddSave\">\n";
		$this->_view		.= "            		<span class=\"fa fa-check\"></span> Simpan\n";
		$this->_view		.= "            	</button>\n";
		$this->_view		.= "                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>\n";
		$this->_view		.= "            </div>\n";
		$this->_view		.= "        </div>\n";
		$this->_view		.= "    </div>\n";
		$this->_view		.= "</div>\n";
	}

}

/* End of file View_factory.php */
/* Location: ./application/modules/crudGenerator/models/View_factory.php */