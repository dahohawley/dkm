<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Table_factory extends CI_Model {
	const PK_TYPE_AI = 'auto_increment';
	const PKTYPE_GENERIC = 'generic';

	protected $_tableName;
	protected $_crudName;
	protected $_pkType;
	protected $_column;
	protected $_pkName;
	protected $_fields = array();
	protected $_columnType;

	protected $_is_testing = FALSE;

	public function initTableFactory($params,$is_testing = FALSE){
		$this->_pkName = str_replace(' ','_', strtolower($params['primary_key']));
		$this->_tableName = str_replace(' ','_',$params['crud_name']);
		$this->_crudName = $params['crud_name'];
		$this->_pkType = $params['pk_type'];
		$this->_column = $params['columnName'];
		$this->_columnType = $params['column-type'];

		$this->_is_testing = $is_testing;
	}

	/**
	* Create Table
	* @param
	* @var general_model::result $result, bool $check, bool $createTable
	* @return general_model::result object
	*/
	public function createTable(){
		$this->load->dbforge();
		$CI =& get_instance();
		$CI->load->model('general_model');
		$result = $this->general_model->result();

		$check = $this->db->table_exists($this->_tableName);
		if($check){
			if(!$this->_is_testing){
				$result->code = 401;
				$result->info = 'Database with the same name already exists.';
			}
		}else{
			if($this->renderColumn()){
				$createTable = $this->dbforge->create_table($this->_tableName);
			}else{
				$result->code = 500;
				$result->info = "Failed rendering Column";
			}
		}
		return $result;
	}

	private function renderColumn(){
		try{
			if($this->_pkType == self::PK_TYPE_AI){
				$this->_fields[$this->_pkName] = array(
					'type' => 'INT',
					'auto_increment' => true
				);
			}else{
				$this->_fields[$this->_pkName] = array(
					'type' => 'VARCHAR',
					'constraint' => '50',
				);
			}

			foreach ($this->_column as $key => $columnName) {
				$columnName = $this->spaceRemove($columnName);
				$columnType = strtoupper($this->_columnType[$key]);
				if($columnType === 'INT_RP'){
					$columnType = 'INT';
				}
				switch ($columnType) {
					case 'INT':
						$constraint = 11;
						break;
					case 'TEXT':
						$constraint = 100;
						break;
					case 'CHAR':
						$constraint = 5;
						break;
					case 'DATE':
						$constraint = FALSE;
						break;
					default:
						$constraint = 100;
						break;
				}
				if($constraint){
					$this->_fields[$columnName] = array(
						'type' => $columnType,
						'constraint' => $constraint,
					);
				}else{
					$this->_fields[$columnName] = array(
						'type' => $columnType,
					);
				}
			}
			// add key
			$this->dbforge->add_field($this->_fields);
			$this->dbforge->add_key($this->_pkName,TRUE);
			return true;
		}catch(Exception $ex){
			return false;
		}
	}

	private function spaceRemove($string){
		return strtolower(str_replace(" ", '_', $string));
	}
}

/* End of file tableFactory.php */
/* Location: ./application/modules/crudGenerator/models/tableFactory.php */