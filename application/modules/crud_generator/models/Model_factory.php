<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_factory extends CI_Model {

	protected $_model;
	CONST PK_TYPE_AI 		= 'auto_increment';
	CONST PK_TYPE_GENERIC	= 'generic';

	public function __construct(){
		$this->load->model('general_model');
	}
	public function createModel($params){
		$result 	= $this->general_model->result();
		$this->_crudName = $params['crud_name'];

		$this->initModel();
		$this->initLoadList($params);
		$this->initSave($params);
		$this->initDelete($params);
		$this->initCodeGenerator($params);
		$this->_model.= "}\n";
		$modelFile = fopen(APPPATH."modules/".$params['crud_name']."/models/".ucfirst($params['crud_name'])."_model.php", "w") or die("Unable to open file!");

		fwrite($modelFile, $this->_model);
	}
	public function initModel(){
		$this->_model 	.=	"<?php\n";
		$this->_model 	.=	"defined('BASEPATH') OR exit('No direct script access allowed');\n\n";
		$this->_model 	.=	"class ".ucfirst($this->_crudName)."_model extends CI_Model {\n";
		$this->_model 	.=	"	protected \$_table = '".$this->_crudName."';\n";
		$this->_model 	.=	"	protected \$_key;\n\n";
	}
	public function initLoadList($params){
		$this->_model .="	public function loadList(\$mode = 'LOAD_ALL',\$params = array()){\n";
		// $this->_model .="		// total rows\n";
		$this->_model .="		\$recordsTotal 		= \$this->db->count_all_results(\$this->_table);\n\n";

		// $this->_model .="		// search query\n";
		$this->_model .="		if(isset(\$params['search']) && \$params['search']){\n";
		$this->_model .="				\$this->db->start_cache();\n\n";

		foreach ($params['columnName'] as $columnName) {
			$this->_model .="				\$this->db->or_like('lower($columnName)',strtolower(\$params['search']),'both');\n";
		}
		$this->_model .="\n";
		$this->_model .="				\$this->db->stop_cache();\n";
		$this->_model .="		}\n\n";

		// $this->_model .="		// pagination query\n";
		$this->_model .="		if(isset(\$params['pagination'])){\n";
		$this->_model .="			\$limit 			= \$params['pagination']['limit'];\n";
		$this->_model .="			\$offset 		= \$params['pagination']['offset'];\n";
		$this->_model .="		}else{\n";
		$this->_model .="			\$limit 			= 0;\n";
		$this->_model .="			\$offset 		= 0;\n";
		$this->_model .="		}\n\n";

		// $this->_model .="		// ordering query\n";
		$this->_model .="		if(isset(\$params['order'])){\n";
		$this->_model .="			\$this->db->order_by(\$params['order']['column'],\$params['order']['dir']);\n";
		$this->_model .="		}\n\n";

		// $this->_model .="		// base query\n";
		$this->_model .="		\$data 						= \$this->db->get(\$this->_table,\$limit,\$offset);\n";

		// $this->_model .="		// getTotalFiltered\n";
		$this->_model .="		\$recordsFiltered 			= \$this->db->count_all_results(\$this->_table);\n\n";

		$this->_model .="		\$result 					= new stdClass;\n";
		$this->_model .="		\$result->data 				= \$data->result();\n";
		$this->_model .="		\$result->recordsTotal 		= \$recordsTotal;\n";
		$this->_model .="		\$result->recordsFiltered 	= \$recordsFiltered;\n\n";

		$this->_model .="		return \$result;\n";
		$this->_model .= "	}\n";
	}
	public function initSave($params){
		$id 			= $params['primary_key'];

		$this->_model.="	public function save( \$params = array() ){\n";
		$this->_model.="		\$CI =& get_instance();\n";
		$this->_model.="		\$CI->load->model('core/general_model');\n\n";
		$this->_model.="		if(isset(\$params['$id']) && !empty(\$params['$id'])){\n";
		$this->_model.="			\$$id 				= \$params['$id'];\n\n";
		$this->_model.="			\$this->db->where('$id',\$$id);\n";
		$this->_model.="			\$this->db->set(\$params);\n";
		$this->_model.="			\$this->db->update(\$this->_table);\n\n";
		$this->_model.="			\$resultData = array(\n";
		$this->_model.="				'$id' => \$$id,\n";
		$this->_model.="			);\n";
		$this->_model.="			\$result = \$CI->general_model->result(200,'Berhasil Merubah data.',\$resultData);\n";
		$this->_model.="		}else{\n";
		$this->_model.="			unset(\$params['$id']);\n";

		if($params['pk_type'] == self:: PK_TYPE_GENERIC){
			$this->_model.="			\$params['$id'] = \$this->getNewCode();\n\n";
		}
		
		$this->_model.="			\$this->db->insert(\$this->_table,\$params);\n\n";
		$this->_model.="			\$resultData = array(\n";
		$this->_model.="				'insert_id' => \$this->db->insert_id(),\n";
		$this->_model.="			);\n";
		$this->_model.="			\$result = \$CI->general_model->result(200,'Berhasil Menambah Data',\$resultData);\n";
		$this->_model.="		}\n";
		$this->_model.="		return \$result;\n";
		$this->_model.="	}\n";
	}
	public function initDelete($params){
		$id 			= $params['primary_key'];

		$this->_model.="	public function delete( \$params = array() ){\n";
		$this->_model.="		\$this->db->where('$id',\$params['$id']);\n";
		$this->_model.="		\$this->db->delete(\$this->_table);\n\n";

		$this->_model.="		\$CI =& get_instance();\n";
		$this->_model.="		\$CI->load->model('general_model');\n";
		$this->_model.="		\$result = \$this->general_model->result(200,'Sukses Menghapus Data #'.\$params['$id']);\n";
		$this->_model.="		return \$result;\n";
		$this->_model.="	}\n";
	}
	public function initCodeGenerator($params){
		$prefix 		= strtoupper(substr($this->_crudName, 0,3));
		$this->_model 	.= 	"	protected function getNewCode(){\n";
		$this->_model 	.=	"		\$q 						= \$this->db->query(\"SELECT max(".$params['primary_key'].") as lastCode from \$this->_table\")->row();\n";
		$this->_model 	.=	"		\$lastCode 				= \$q->lastCode;\n";
		$this->_model 	.=	"		\$nextValue	 			= explode('-', \$lastCode)[1]+1;\n";
		$this->_model 	.=	"		\$nextValue				= str_pad(\$nextValue, 6,'0',STR_PAD_LEFT);\n";
		$this->_model 	.=	"		\$prefix 				= '$prefix-';\n";
		$this->_model 	.=	"		\$newCode 				= \$prefix.\$nextValue;\n";
		$this->_model 	.=	"		return \$newCode;\n";
		$this->_model 	.= 	"	}\n";
	}

}

/* End of file Model_factory.php */
/* Location: ./application/modules/crudGenerator/models/Model_factory.php */