<style type="text/css">
	.req{
		color: red;
	}
	.subLabel {
		color: grey;
	}
</style>
<?php echo $this->template->cardOpen('Crud Generator');?>

<?php echo $this->template->cardBodyOpen();?>
	<form action="#" id="formCrud" method="POST">
		<div class="form-group row">
			<label class="control-label col-md-3 text-right">
				Crud Name <span class="req">*</span><br>
				<small class="subLabel">This Column is used as Table name</small>
			</label>
			<div class="col-md-9">
				<input type="text" class='form-control' value="" name="crud_name" id="crud_name">
			</div>
		</div>
		<div class="form-group row">
			<label class="control-label col-md-3 text-right">
				Primary Key <span class="req">*</span> <br>
				<small class="subLabel">Used as primary key of the table</small>
			</label>
			<div class="col-md-9">
				<input type="text" class='form-control' value="" name="primary_key" id="primary_key">
			</div>
		</div>
		<div class="form-group row">
			<label class="control-label col-md-3 text-right">
				Primary Key Type <span class="req">*</span>
			</label>
			<div class="col-md-9">
				<label>
					<input type="radio" name="pk_type" checked value="generic"> Generic
				</label>
				<label>
					<input type="radio" name="pk_type" value="auto_increment"> Auto Increment
				</label>
			</div>
		</div>
		<div class="form-group row">
			<label class="control-label col-md-3 text-right"></label>
			<div class="col-md-9">
				<button class="btn btn-primary" type="button" onclick="addColumn()">
					<span class="fa fa-plus"></span> Add Column
				</button>
				<button class="btn btn-success">
					<span class="fa fa-save"></span> Save
				</button>
			</div>
		</div>

		<hr>
		<div id="columnRow">
			
		</div>

	</form>
<?php echo $this->template->cardBodyClose();?>

<script type="text/javascript">
	groupNumber = 0;
	// Column Manipulation
		addColumn();
		function addColumn(){
			groupNumber += 1;
			txt = '';
			txt += '<div id="colGroup-'+groupNumber+'" class="colGroup">';

				txt += drawColumnName(groupNumber);
				txt += drawDataType(groupNumber);
				txt += drawValidation(groupNumber);

			txt += '</div>';

			$("#columnRow").append(txt);

			addRules();
		}
		function removeColumn(groupNumber){
			$("#colGroup-"+groupNumber).remove();
		}
		function drawColumnName(groupNumber){
			txt = '';
			txt += '<div class="form-group row">';
			txt += '	<label class="control-label col-md-3 text-right">Column Name</label>';
			txt += '	<div class="col-md-9">';
			txt += '		<div class="row">';
			txt += '			<div class="col-md-9">';
			txt += '				<input type="text" class="form-control columnName" name="columnName['+groupNumber+']" id="columnName-'+groupNumber+'">';
			txt += '			</div>';
			txt += '			<div class="col-md-3">';
			txt += '				<button class="btn btn-danger" type="button" onclick="removeColumn('+groupNumber+')">';
			txt += '					<span class="fa fa-times"></span> Remove';
			txt += '				</button>';
			txt += '			</div>';
			txt += '		</div>';
			txt += '	</div>';
			txt += '</div>';

			return txt;
		}

		function drawDataType(groupNumber){
			txt = '';
			txt += '<div class="form-group row">';
			txt += '	<label class="control-label col-md-3 text-right">Data Type</label>';
			txt += '	<div class="col-md-9">';
			txt += '		<select name="column-type['+groupNumber+']" class="form-control">';
			txt += '			<option value="text">Text</option>';
			txt += '			<option value="varchar">Varchar</option>';
			txt += '			<option value="char">Char</option>';
			txt += '			<option value="int">Integer</option>';
			txt += '			<option value="int_rp">Integer with Money Class</option>';
			txt += '			<option value="date">Date</option>';
			txt += '		</select>';
			txt += '	</div>';
			txt += '</div>';

			return txt;
		}

		function drawValidation(groupNumber){
			txt = '';
			txt += '<div class="form-group row">';
			txt += '	<label class="control-label col-md-3 text-right">Validation</label>';
			txt += '	<div class="col-md-9">';
			txt += '		<label>';
			txt += '			<input type="checkbox" name="validation-'+groupNumber+'[]" value="required"> Required';
			txt += '		</label><br>';
			txt += '		<label>';
			txt += '			<input type="checkbox" name="validation-'+groupNumber+'[]" value="callback_alpha_dash_space"> Alpha Dash Space';
			txt += '		</label><br><br>';
			txt += '		<div class="form-group row">';
			txt += '			<label class="control-label col-md-2">Min Length</label>';
			txt += '			<div class="col-md-4">';
			txt += '				<input type="number" class="form-control" value="" name="min-length-'+groupNumber+'" id="min-length">';
			txt += '			</div>';
			txt += '		</div>';
			txt += '		<div class="form-group row">';
			txt += '			<label class="control-label col-md-2">Max Length</label>';
			txt += '			<div class="col-md-4">';
			txt += '				<input type="number" class="form-control" value="" name="max-length-'+groupNumber+'" id="max-length">';
			txt += '			</div>';
			txt += '		</div>';
			txt += '	</div>';
			txt += '</div>';
			return txt;
		}
		function addRules(){
			$('.columnName').each(function() {
				id = $(this).attr('id');
                setTimeout(function() {
	                $("#"+id).rules("add", {required: true});
				}, 300);
            });  
		}

	// form submitter
		function submitForm(){
			$.ajax({
				url:"<?php echo site_url('crud_generator/saveCrud') ?>",
				type:"POST",
				data : $("#formCrud").serialize(),
				beforeSend:function(){
					$("button").attr('disabled','true');
				},
				success:function(response){
					response = JSON.parse(response);
					if(response.code == 200){
						crudName 	= $("#crud_name").val();
						console.log(crudName);
						window.open("<?php echo site_url() ?>/"+crudName);
					}else{
						custom_notification('danger',response.info);
					}
				},
				complete:function(){
					$("button").removeAttr('disabled');				
				}
			});
		}
	
	$(document).ready(function(){
		$("#formCrud").validate({
			rules:{
				crud_name : "required",
				pk_type : "required",
				primary_key : "required",
			},
			submitHandler:function(){
				/**
				* Menghitung jumlah column yang ditambahkan.
				* jika column kurang dari 1 maka tidak akan disubmit.
				*/
				var countColumn = 0;
				$(".colGroup").each(function(){
					countColumn += 1;
				});
				if(countColumn < 1){
					custom_notification('danger','Please add column first!');
					return false;
				}

				/**
				* Submit Form jika benar;
				*/
				submitForm();

			}
		});
	});
</script>