<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Infaq_model extends CI_Model {
	protected $_table = 'infaq';
	protected $_key;
	const ID_TIPE_TRANSAKSI 	= 2;


	public function loadList($mode = 'LOAD_ALL',$params = array()){
		$recordsTotal 		= $this->db->count_all_results($this->_table);

		if(isset($params['search']) && $params['search']){
				$this->db->start_cache();

				$this->db->or_like('lower(nama_donatur)',strtolower($params['search']),'both');
				$this->db->or_like('lower(tanggal_penerimaan)',strtolower($params['search']),'both');
				$this->db->or_like('lower(alamat)',strtolower($params['search']),'both');
				$this->db->or_like('lower(jumlah_penerimaan)',strtolower($params['search']),'both');

				$this->db->stop_cache();
		}

		if(!empty($params['start_date'])){
			$this->db->where("tanggal_penerimaan >= ",$params['start_date']);
		}
		
		if(!empty($params['end_date'])){
			$this->db->where("tanggal_penerimaan <= ",$params['end_date']);
		}

		if(isset($params['pagination'])){
			$limit 			= $params['pagination']['limit'];
			$offset 		= $params['pagination']['offset'];
		}else{
			$limit 			= 0;
			$offset 		= 0;
		}

		if(isset($params['order'])){
			$this->db->order_by($params['order']['column'],$params['order']['dir']);
		}

		$data 						= $this->db->get($this->_table,$limit,$offset);
		$recordsFiltered 			= $this->db->count_all_results($this->_table);

		$result 					= new stdClass;
		$result->data 				= $data->result();
		$result->recordsTotal 		= $recordsTotal;
		$result->recordsFiltered 	= $recordsFiltered;

		return $result;
	}
	public function save( $params = array() ){
		$CI =& get_instance();
		$CI->load->model('core/general_model');

		if(isset($params['id_infaq']) && !empty($params['id_infaq'])){
			$id_infaq 				= $params['id_infaq'];

			$this->db->where('id_infaq',$id_infaq);
			$this->db->set($params);
			$this->db->update($this->_table);

			$resultData = array(
				'id_infaq' => $id_infaq,
			);
			$result = $CI->general_model->result(200,'Berhasil Merubah data.',$resultData);
		}else{
			unset($params['id_infaq']);
			$params['id_infaq'] = $this->getNewCode();

			$this->db->insert($this->_table,$params);

			$insertID 		= $params['id_infaq'];

			$resultData = array(
				'insert_id' => $insertID,
			);

			/* Insert Jurnal */
			$dataJurnal 			= array();
			$this->load->model('accounting/jurnal_model');
			$dataJurnal['transaksi'] 	= array(
				'id_tipe' 			=> self::ID_TIPE_TRANSAKSI,
				'id_transaksi'		=> $insertID,
				'tanggal_transaksi' => date('Y-m-d',strtotime($params['tanggal_penerimaan'])),
				'jumlah_transaksi' 	=> format_angka($params['jumlah_penerimaan']),
			);
			$dataJurnal['jurnal'][] = array(
				'kode_akun' 		=> '111',
				'posisi_dr_cr' 		=> 'D',
				'nominal' 			=> format_angka($params['jumlah_penerimaan']),
			);
			$dataJurnal['jurnal'][] = array(
				'kode_akun' 		=> '101',
				'posisi_dr_cr' 		=> 'k',
				'nominal' 			=> format_angka($params['jumlah_penerimaan']),
			);
			$this->jurnal_model->createJurnal($dataJurnal);


			$result = $CI->general_model->result(200,'Berhasil Menambah Data',$resultData);
		}
		return $result;
	}
	public function delete( $params = array() ){
		$this->db->where('id_infaq',$params['id_infaq']);
		$this->db->delete($this->_table);

		$CI =& get_instance();
		$CI->load->model('general_model');
		$result = $this->general_model->result(200,'Sukses Menghapus Data #'.$params['id_infaq']);
		return $result;
	}
	protected function getNewCode(){
		$q 						= $this->db->query("SELECT max(id_infaq) as lastCode from $this->_table")->row();
		if ($q->lastCode) {
			$lastCode 				= $q->lastCode;
			$nextValue	 			= explode('-', $lastCode)[1] + 1;
			$nextValue				= str_pad($nextValue, 6, '0', STR_PAD_LEFT);
			$prefix 				= 'INF-';
			$newCode 				= $prefix . $nextValue;
		} else {
			$newCode 				= 'INF-' . str_pad(1, 6, '0', STR_PAD_LEFT);
		}
		return $newCode;
	}
}
