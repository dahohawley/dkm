<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js" integrity="sha256-xKeoJ50pzbUGkpQxDYHD7o7hxe0LaOGeguUidbq6vis=" crossorigin="anonymous"></script>
<style type="text/css">
	.dropdown-menu {
	    width: 500px !important;
	}
</style>
<div class="row">
	<div class="col-md-12">
		<?php echo $this->template->cardOpen('Infaq & Sodaqoh');?>
		<?php echo $this->template->cardBodyOpen();?>
		<div class="btn-group" id="action_button">
		    <a href="<?php echo site_url('infaq/createNew') ?>" class="btn btn-primary">
			    <span class="fa fa-plus"></span> Tambah
		    </a>
		    <div class="dropdown">
		        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		        	<span class="fa fa-filter"></span>
		        </button>
		        <div class="dropdown-menu">
		            <form class="px-4 py-3" id="form-filter">
		            	<div class="form-group row">
		            		<div class="col-md-6">
		            			<label>Tanggal Awal</label>
			                	<input type="date" name="start_date" class="form-control" id="start_date">
		            		</div>
		            		<div class="col-md-6">
		            			<label>Tanggal Akhir</label>
			                	<input type="date" name="end_date" class="form-control" id="end_date">
		            		</div>
		            	</div>

		                <div class="float-right">
			                <button type="submit" class="btn btn-primary">Filter</button>
		                </div>
		                <br><br>
		            </form>
		        </div>
		    </div>
		</div>

		<table class="table table-hover table-bordered" id="table-infaq" style="width: 100%">
		    <thead>
		        <tr>
		            <th class="text-center">Id Infaq</th>
		            <th class="text-center">Nama Donatur</th>
		            <th class="text-center">Tanggal Penerimaan</th>
		            <th class="text-center">Alamat</th>
		            <th class="text-center">Jumlah Penerimaan</th>
		        </tr>
		    </thead>
		</table>

		<?php echo $this->template->cardBodyClose();?>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
    	$("#table-infaq").DataTable({
    		processing:true,
    		serverSide:true,
    		ajax : {
    			url:"<?php echo site_url('infaq/loadList') ?>",
    			type:"POST",
    			data : function(e){
    				e.start_date 	= $("#start_date").val();
    				e.end_date 		= $("#end_date").val();
    			}
    		},
    		columns :[
    			{data:"id_infaq"},
    			{data:"nama_donatur"},
    			{
    				data:"tanggal_penerimaan",
    				render: function(data,type,row,meta){
    					return moment(data).format('DD MMMM YYYY');
    				}
    			},
    			{data:"alamat"},
    			{
    				data:"jumlah_penerimaan",
    				render: function(data,type,row,meta){
    					return format_rp(data);
    				}
    			},
    		],
    	});

    	$("#form-filter").submit(function(e){
    		e.preventDefault();
    		$("#table-infaq").DataTable().ajax.reload();
    	});

		$("#table-infaq_filter").append($("#action_button"));
    });
    function refreshTable(){
    	$("#table-infaq").DataTable().ajax.reload();
    }
    function addData(){
    	$("#modal-addTitle").text('Tambah data baru');
    	$("#id_infaq").val(0);
    	$("#formAdd")[0].reset();
    	$("#modal-add").modal('show');
    }
</script>