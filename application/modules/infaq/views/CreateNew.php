<style type="text/css">
	.readonly { 
		background-color: white !important;
	}
</style>
<div class="card">
	<div class="card-header">
		Penerimaan Infaq dan Sodaqoh
	</div>
	<div class="card-body">
		<form method="POST" action="#" class="form-horizontal" id="formAdd">
			<input type="hidden" name="id_infaq" id="id_infaq" >
			<div class="form-group row">
				<label class="control-label col-md-3">Nama Donatur</label>
				<div class="col-md-9">
					<input type="text" class='form-control' name="nama_donatur" id="nama_donatur">
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-3">Tanggal Penerimaan</label>
				<div class="col-md-9">
					<input type="text" class='form-control readonly' readonly name="tanggal_penerimaan" id="tanggal_penerimaan" value="<?php echo date('d F Y') ?>">
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-3">Alamat</label>
				<div class="col-md-9">
					<input type="text" class='form-control' name="alamat" id="alamat">
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-3">Jumlah Penerimaan</label>
				<div class="col-md-9">
					<input type="text" class='form-control rupiah' name="jumlah_penerimaan" id="jumlah_penerimaan">
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-3"></label>
				<div class="col-9">
					<div class="float-right">
						<a href="<?php echo site_url('infaq') ?>" class="btn btn-secondary">
							Back
						</a>
						<button class="btn btn-primary">
							Simpan
						</button>
					</div>
				</div>
			</div>	
		</form>
	</div>
</div>

<script type="text/javascript">
	$.validator.addMethod(
		'moneyGreaterThanZero',
		function (value, element) {
			value = format_angka(value);
			if(value <= 0){
				return false;
			}else{
				return true;
			}
		},
		'Please insert correct values'
	);

	$("#formAdd").validate({
		rules : {
			id_infaq: {
				required : true,
			},
			nama_donatur: {
				required:true,
			},
			jumlah_penerimaan: {
				required:true,
				moneyGreaterThanZero : true,
			},
		},
		submitHandler : function(){
			$.ajax({
				url:"<?php echo site_url('infaq/save') ?>",
				type : "POST",
				data : $("#formAdd").serialize(),
				beforeSend:function(){
					buttonSpinner('btnAddSave');
				},
				success:function(res){
					res = JSON.parse(res);
					if(res.code == 200){
						custom_notification('success',res.info);
						window.location.replace("<?php echo site_url('infaq') ?>");
					}else{
						custom_notification('danger',res.info);
					}
				},
				complete:function(){
					$(".modal").modal('hide');
					removeButtonSpinner('btnAddSave');
				}
			});
		},
		errorClass : 'text-danger',
		errorElement : 'div'
	});
</script>
