<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	include_once APPPATH . '/modules/core/controllers/Controller.php';

	class infaq extends Controller{

		public function __construct(){
			parent::__construct();
		}

		public function index(){
			// dipake untuk namain card
			$this->using('datatable');
			$this->using('jquery.validate');
			$this->using('moment');
			$this->dispatch(DEF_TEMPLATE_INSIDE,'Infaq',get_defined_vars());	
		}

		public function createNew(){
			$this->using('jquery.validate');
			$this->using('moment');
			$this->dispatch(DEF_TEMPLATE_INSIDE,'CreateNew',get_defined_vars());
		}

		public function loadList(){
			$this->load->model('core/general_model');
			$result = $this->general_model->result();
			$params = $this->input->post();

			$dataTable = false;
			if(isset($params['draw'])){
				$dataTable = true;
				$search = $params['search']['value'];
				$pagination = array(
					'limit' => $params['length'],
					'offset' => $params['start']
				);

				$ordering = array(
					'column' => $params['columns'][ $params['order'][0]['column'] ]['data'],
					'dir' => $params['order'][0]['dir']
				);

				$loadParams = array(
					'search' => $search,
					'pagination' => $pagination,
					'order' => $ordering,
					'start_date' => $params['start_date'],
					'end_date' => $params['end_date']
				);
			}else{
				$loadParams = array();
			}
			$this->load->model('infaq_model','model');
			$data = $this->model->loadList('LOAD_ALL',$loadParams);
 	 		if($dataTable){
 	 			$result = array(
 	 				'draw' => $params['draw'],
 	 				'recordsTotal' => $data->recordsTotal,
 	 				'recordsFiltered' => $data->recordsFiltered,
 	 				'data' => $data->data
 	 			);
 	 		}else{
 	 			$result->data = $data;
 	 		}
 	 		echo json_encode($result);
 	 	}
		public function save(){
			$params = $this->input->post();

			$params['jumlah_penerimaan'] = format_angka($params['jumlah_penerimaan']);

			$this->form_validation->set_data($params);
			$this->form_validation->set_rules('nama_donatur','Nama Donatur','required|callback_alpha_dash_space');
			$this->form_validation->set_rules('jumlah_penerimaan','Jumlah Penerimaan','required');

			$params['tanggal_penerimaan'] 	= date('Y-m-d');

			if ($this->form_validation->run() == TRUE) {
				$this->load->model('infaq_model');
				$result = $this->infaq_model->save($params);

			} else {
				$this->load->model('general_model');
				$result = $this->general_model->result(401,validation_errors());
			}
			echo json_encode($result);
		}
		public function deleteData(){
 			$params = $this->input->post();
				$this->load->model('infaq_model');
 			$result = $this->infaq_model->delete($params);
 			echo json_encode($result);
 		}

	 	function alpha_dash_space($fullname){
	 	    if (! preg_match('/^[a-zA-Z\s]+$/', $fullname)) {
	 	        $this->form_validation->set_message('alpha_dash_space', '%s Hanya bisa diisi oleh huruf dan spasi.');
	 	        return FALSE;
	 	    } else {
	 	        return TRUE;
	 	    }
	 	}

}