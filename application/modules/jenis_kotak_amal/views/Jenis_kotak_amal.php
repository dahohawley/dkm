<?php echo $this->template->cardOpen('Jenis Kotak Amal');?>

<?php echo $this->template->cardBodyOpen();?>
	<div class="btn-group" id="action_button">
		<button class="btn btn-primary" id="buttonAdd" onclick="addData()">
			<span class="fa fa-plus"></span> Tambah
		</button>
	</div>

	<table class="table table-hover table-bordered" id="table-jenis_kotak_amal" style="width: 100%">
		<thead>
			<tr>
				<th class="text-center">Id Jenis Kotak Amal</th>
				<th class="text-center">Nama Jenis</th>
				<th class="text-center">Aksi</th>
			</tr>
		</thead>
	</table>
<?php echo $this->template->cardBodyClose();?>
<script type="text/javascript">
	$(document).ready(function(){
		$("#table-jenis_kotak_amal").DataTable({
			processing:true,
			serverSide:true,
			ajax : {
				url:"<?php echo site_url('jenis_kotak_amal/loadList') ?>",
				type:"POST"
			},
			columns :[
				{data:"id_jenis_kotak_amal"},
				{data:"nama_jenis"},
			{
				data:"aksi",
				render:function(data,type,row){
					txt = '';
					txt += '<button class="btn btn-primary btn-xs edit"';
					txt += '	id_jenis_kotak_amal = "'+row.id_jenis_kotak_amal+'";'
					txt += '	nama_jenis = "'+row.nama_jenis+'"';
					txt += '>';
					txt += '	<span class="fa fa-pencil-alt"></span> Ubah';
					txt += '</button> ';

					txt += '<button class="btn btn-danger btn-xs delete"';
					txt += '	id_jenis_kotak_amal = "'+row.id_jenis_kotak_amal+'";'
					txt += '	nama_jenis = "'+row.nama_jenis+'"';
					txt += '>';
					txt += '	<span class="fa fa-trash"></span> Hapus';
					txt += '</button>';
					return txt;
				}
			},
			],
		});

		$(document).on('click','.edit',function(){
			id_jenis_kotak_amal 			= $(this).attr('id_jenis_kotak_amal');
			nama_jenis 			= $(this).attr('nama_jenis');
			$("#id_jenis_kotak_amal").val(id_jenis_kotak_amal);
			$("#nama_jenis").val(nama_jenis);
			$("#modal-addTitle").text('Ubah data #'+id_jenis_kotak_amal);
			$("#modal-add").modal('show');
		});

		$(document).on('click','.delete',function(){
			id_jenis_kotak_amal 			= $(this).attr('id_jenis_kotak_amal');
			nama_jenis 			= $(this).attr('nama_jenis');

			conf = confirm("Apakah anda yakin ingin menghapus data #"+id_jenis_kotak_amal+" ?");
			if(conf){
				$.ajax({
					url:"<?php echo site_url('jenis_kotak_amal/deleteData') ?>",
					type :"POST",
					data : {
						id_jenis_kotak_amal : id_jenis_kotak_amal
					},
					success:function(res){
						res = JSON.parse(res);
						if(res.code == 200){
							custom_notification('success',res.info);
							refreshTable();
						}else{
							custom_notification('danger',res.info);
						}
					}
				});
			}
		});

 		$("#table-jenis_kotak_amal_filter").append($("#action_button"));

		$("#formAdd").validate({
			rules : {
				id_jenis_kotak_amal: {
					required : true,
				},
				nama_jenis: {
					required:true,
				},
			},
			submitHandler : function(){
				$.ajax({
					url:"<?php echo site_url('jenis_kotak_amal/save') ?>",
					type : "POST",
					data : $("#formAdd").serialize(),
					beforeSend:function(){
						buttonSpinner('btnAddSave');
					},
					success:function(res){
						res = JSON.parse(res);
						if(res.code == 200){
							custom_notification('success',res.info);
							refreshTable();
						}else{
							custom_notification('danger',res.info);
						}
					},
					complete:function(){
						$(".modal").modal('hide');
						removeButtonSpinner('btnAddSave');
					}
				});
			}
		});
	});
	function refreshTable(){
		$("#table-jenis_kotak_amal").DataTable().ajax.reload();
	}
	function addData(){
		$("#modal-addTitle").text('Tambah data baru');
		$("#id_jenis_kotak_amal").val(0);
		$("#formAdd")[0].reset();
		$("#modal-add").modal('show');
	}
</script>

<div id="modal-add" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal-addTitle">Tambah Crud name</h4>
            </div>
            <div class="modal-body">
            	<form method="POST" action="#" class="form-horizontal" id="formAdd">
 					<input type="hidden" name="id_jenis_kotak_amal" id="id_jenis_kotak_amal" >
				<div class="form-group">
					<label class="control-label col-md-3 text-right">Nama Jenis</label>
					<div class="col-md-9">
						<input type="text" class='form-control' name="nama_jenis" id="nama_jenis">
					</div>
				</div>
            	</form>
            </div>
            <div class="modal-footer">
            	<button class="btn btn-primary" onclick='$("#formAdd").submit()' id="btnAddSave">
            		<span class="fa fa-check"></span> Simpan
            	</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
