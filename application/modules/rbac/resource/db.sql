/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 10.1.37-MariaDB : Database - itfw
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `rbac_account` */

DROP TABLE IF EXISTS `rbac_account`;

CREATE TABLE `rbac_account` (
  `id_account` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(40) NOT NULL,
  `password` text NOT NULL,
  `email` text,
  `id_role` int(11) NOT NULL,
  `account_status` char(1) NOT NULL COMMENT '0 = Pending, 1 = Active, 2 = Banned',
  PRIMARY KEY (`id_account`),
  KEY `id_role` (`id_role`),
  CONSTRAINT `rbac_account_ibfk_1` FOREIGN KEY (`id_role`) REFERENCES `rbac_role` (`id_role`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Table structure for table `rbac_auth_log` */

DROP TABLE IF EXISTS `rbac_auth_log`;

CREATE TABLE `rbac_auth_log` (
  `id_auth_log` int(11) NOT NULL AUTO_INCREMENT,
  `id_account` int(11) DEFAULT NULL,
  `auth_type` char(1) DEFAULT NULL COMMENT '0 = Login, 1 = Logout',
  `ip_address` text,
  `auth_dtm` datetime DEFAULT NULL,
  PRIMARY KEY (`id_auth_log`),
  KEY `id_account` (`id_account`),
  CONSTRAINT `rbac_auth_log_ibfk_1` FOREIGN KEY (`id_account`) REFERENCES `rbac_account` (`id_account`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Table structure for table `rbac_menu` */

DROP TABLE IF EXISTS `rbac_menu`;

CREATE TABLE `rbac_menu` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `label` text,
  `parent` int(11) DEFAULT NULL,
  `references` text,
  `type` char(1) DEFAULT NULL COMMENT '0 = Menu , 1 = Post Menu',
  `icon` text,
  PRIMARY KEY (`id_menu`),
  KEY `parent` (`parent`),
  CONSTRAINT `rbac_menu_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `rbac_menu` (`id_menu`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Table structure for table `rbac_privileges` */

DROP TABLE IF EXISTS `rbac_privileges`;

CREATE TABLE `rbac_privileges` (
  `id_privileges` int(11) NOT NULL AUTO_INCREMENT,
  `id_role` int(11) DEFAULT NULL,
  `id_menu` int(11) DEFAULT NULL,
  `update_dtm` datetime DEFAULT NULL,
  PRIMARY KEY (`id_privileges`),
  KEY `id_role` (`id_role`),
  KEY `id_menu` (`id_menu`),
  CONSTRAINT `rbac_privileges_ibfk_1` FOREIGN KEY (`id_role`) REFERENCES `rbac_role` (`id_role`),
  CONSTRAINT `rbac_privileges_ibfk_2` FOREIGN KEY (`id_menu`) REFERENCES `rbac_menu` (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `rbac_role`;

CREATE TABLE `rbac_role` (
  `id_role` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  PRIMARY KEY (`id_role`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Data for the table `rbac_role` */

insert  into `rbac_role`(`id_role`,`name`) values 
(0,'All Roles'),
(1,'Superadmin');

insert  into `rbac_menu`(`id_menu`,`label`,`parent`,`references`,`type`,`icon`) values 
(1,'Access Control',NULL,'#','0','fa fa-gear'),
(2,'Role',1,'rbac/form/menu','0','fa fa-gear'),
(3,'Account',1,'rbac/form/account','0','fa fa-user'),
(4,'Menu',1,'rbac/form/menu','0','fa fa-flag'),
(5,'Privileges',1,'rbac/form/privileges','0','fa fa-address-card');

/*Data for the table `rbac_privileges` */

insert  into `rbac_privileges`(`id_privileges`,`id_role`,`id_menu`,`update_dtm`) values 
(1,1,1,'2019-05-19 02:24:49'),
(2,1,2,'2019-05-19 02:25:00'),
(3,1,3,'2019-05-19 02:25:05'),
(4,1,4,'2019-05-19 02:25:09'),
(5,1,5,'2019-05-19 02:25:17');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
