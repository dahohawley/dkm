/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 10.1.37-MariaDB : Database - test_
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Data for the table `rbac_menu` */

insert  into `rbac_menu`(`id_menu`,`label`,`parent`,`references`,`type`,`icon`) values 
(1,'Access Control',NULL,'#','0','fa fa-gear'),
(2,'Role',1,'rbac/form/menu','0','fa fa-gear'),
(3,'Account',1,'rbac/form/account','0','fa fa-user'),
(4,'Menu',1,'rbac/form/menu','0','fa fa-flag'),
(5,'Privileges',1,'rbac/form/privileges','0','fa fa-address-card');

/*Data for the table `rbac_privileges` */

insert  into `rbac_privileges`(`id_privileges`,`id_role`,`id_menu`,`update_dtm`) values 
(1,1,1,'2019-05-19 02:24:49'),
(2,1,2,'2019-05-19 02:25:00'),
(3,1,3,'2019-05-19 02:25:05'),
(4,1,4,'2019-05-19 02:25:09'),
(5,1,5,'2019-05-19 02:25:17');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
