/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 10.1.37-MariaDB : Database - school
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`school` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `school`;

/*Table structure for table `rbac_account` */

DROP TABLE IF EXISTS `rbac_account`;

CREATE TABLE `rbac_account` (
  `id_account` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(40) NOT NULL,
  `password` text NOT NULL,
  `id_role` int(11) NOT NULL,
  `account_status` char(1) NOT NULL,
  PRIMARY KEY (`id_account`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `rbac_account` */

insert  into `rbac_account`(`id_account`,`username`,`password`,`id_role`,`account_status`) values 
(2,'superadmin','$2y$10$b8awtKLxZU.qI1mQ83mQh.5ey4uoJZBLqewZnabf7NXRDf6eb.2Ly',2,'1'),
(16,'teacher','$2y$10$2Hom2Tf3KICArx0/8RdG3.WzTWLHcQ2VEM00o7SBjXKKCczY96Nmm',2,'1');

/*Table structure for table `rbac_menu` */

DROP TABLE IF EXISTS `rbac_menu`;

CREATE TABLE `rbac_menu` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `label` text NOT NULL,
  `icon` text NOT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `rbac_menu` */

insert  into `rbac_menu`(`id_menu`,`label`,`icon`) values 
(1,'RBAC','fa fa-gears'),
(7,'User','fa fa-user');

/*Table structure for table `rbac_privileges` */

DROP TABLE IF EXISTS `rbac_privileges`;

CREATE TABLE `rbac_privileges` (
  `id_privileges` int(11) NOT NULL AUTO_INCREMENT,
  `id_role` text NOT NULL,
  `id_menu` int(11) NOT NULL,
  `id_submenu` int(11) NOT NULL,
  PRIMARY KEY (`id_privileges`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

/*Data for the table `rbac_privileges` */

insert  into `rbac_privileges`(`id_privileges`,`id_role`,`id_menu`,`id_submenu`) values 
(1,'1',1,16),
(2,'1',1,17),
(4,'1',1,21),
(5,'1',1,22),
(6,'1',2,20),
(7,'1',2,23),
(8,'1',2,24),
(9,'1',2,25),
(10,'1',2,26),
(11,'1',2,27),
(12,'1',4,28),
(13,'2',1,16),
(14,'2',1,17),
(15,'2',1,21),
(16,'2',1,22),
(17,'2',7,30),
(18,'2',7,31),
(19,'2',7,32),
(20,'2',7,33);

/*Table structure for table `rbac_role` */

DROP TABLE IF EXISTS `rbac_role`;

CREATE TABLE `rbac_role` (
  `id_role` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  PRIMARY KEY (`id_role`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `rbac_role` */

insert  into `rbac_role`(`id_role`,`name`) values 
(2,'superadmin');

/*Table structure for table `rbac_submenu` */

DROP TABLE IF EXISTS `rbac_submenu`;

CREATE TABLE `rbac_submenu` (
  `id_submenu` int(11) NOT NULL AUTO_INCREMENT,
  `id_menu` int(11) NOT NULL,
  `label` text NOT NULL,
  `url` text NOT NULL,
  `icon` text NOT NULL,
  PRIMARY KEY (`id_submenu`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

/*Data for the table `rbac_submenu` */

insert  into `rbac_submenu`(`id_submenu`,`id_menu`,`label`,`url`,`icon`) values 
(16,1,'Menu','rbac/form/menu',''),
(17,1,'Privileges','rbac/form/privileges',''),
(19,3,'Penjualan','jualbeli/penjualan','fd'),
(20,2,'Chart Of Accounts','core/coa','d'),
(21,1,'Account','rbac/form/account',''),
(22,1,'Role','rbac/form/role',''),
(25,2,'Pasien','pasien/',''),
(26,2,'Dokter','dokter/',''),
(27,2,'Jasa Tindakan','jasaTindakan/',''),
(28,4,'Persediaan','persediaan/kartu_stok',''),
(30,7,'Data User','user/form/user',''),
(31,7,'User Type','user/form/userType',''),
(32,7,'User Group','user/form/userGroup',''),
(33,7,'User Maping','user/form/userMap','');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
