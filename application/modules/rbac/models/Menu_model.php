<?php
	class Menu_model extends CI_MODEL{
		public function loadList($mode = 'LOAD_ALL',$params = array()){
			$totalData = $this->db->count_all_results('rbac_menu');
			if($mode != 'LOADBY_PAGE'){
				switch ($mode) {
					case 'LOADBY_ID':
						$this->db->where('id_menu',$params['id_menu']);
						break;
					case 'LOADBY_REFERENCES':
						$this->db->where('references',$params['references']);
						break;
					case 'LOAD_PARENT':
						$this->db->where('parent is NULL');
						break;
					case 'LOAD_CHILD':
						$this->db->where('parent',$params['parent']);
						break;
					case 'LOAD_PARENTBYROLE':
						$this->db->where('parent is NULL');
						$this->db->where('id_role = '.$params['id_role']);
						break;
				}
			}else{
				$this->db->limit($params['limit'],$params['offset']);
			}
			if(isset($params['search']) && !empty($params['search']) ){
				$this->db->where('lower(id_menu) LIKE "%'.strtolower($params['search']).'%"');
				$this->db->or_where('lower(name) LIKE "%'.strtolower($params['search']).'%" ');
			}

			$q = $this->db->get('rbac_menu');
			
			// echo $this->db->last_query();die();
			$data = new stdClass;
			$data->data = $q;
			$data->total = $totalData;
			return $data;
		}
		public function save($data = array()){
			$CI =& get_instance();
			$CI->load->model('core/general_model');
			
			$result = $this->general_model->result();
			if(!empty($data['id_menu'])){
				if(!$data['parent']){
					$data['parent'] = null;
				}
				// Update
				$id = $data['id_menu'];
				unset($data['id_menu']);
				$this->db->where('id_menu',$id);
				$this->db->set($data);
				if($this->db->update('rbac_menu')){
					return $id;
				}else{
					return false;
				}
			}else{
				if(!$data['parent']){
					unset($data['parent']);
				}
				if($this->db->insert('rbac_menu',$data)){
					return $this->db->insert_id();
				}else{
					return false;
				}
			}
			return false;
		}
		public function delete($delete_id = null){
			if(!$delete_id){
				return false;
			}else{
				$this->db->where('id_menu',$delete_id);
				$this->db->delete('rbac_menu');
				return $delete_id;
			}
		}
	}