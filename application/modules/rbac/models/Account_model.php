<?php
	class Account_model extends CI_MODEL{
		public function __construct(){
			$this->load->database();
		}
		public function loadList($mode = 'LOAD_ALL',$params = array()){
			$totalData = $this->db->count_all_results('rbac_account');
			$this->db->select('id_account,username,email,rbac_account.id_role,rbac_role.name as role_name,account_status');
			if($mode != 'LOADBY_PAGE'){
				switch ($mode) {
					case 'LOADBY_ID':
						$this->db->where('id_account',$params['id_role']);
						break;
					case 'AUTHENTICATION':
						$this->db->where('username',$params['username']);
						$this->db->select('password');
						break;
				}
			}else{
				$this->db->limit($params['limit'],$params['offset']);
			}
			if(isset($params['search']) && !empty($params['search']) ){
				$this->db->where('lower(id_account) LIKE "%'.strtolower($params['search']).'%"');
				$this->db->or_where('lower(username) LIKE "%'.strtolower($params['search']).'%" ');
				$this->db->or_where('lower(email) LIKE "%'.strtolower($params['search']).'%" ');
			}
			$this->db->join('rbac_role', 'rbac_role.id_role = rbac_account.id_role');
			$q = $this->db->get('rbac_account');

			$data = new stdClass;
			$data->data = $q;
			$data->total = $totalData;
			return $data;
		}
		public function save($data = array()){
			$CI =& get_instance();
			$CI->load->model('core/general_model');
			
			$result = $this->general_model->result();
			$password = $data['password'];
			unset($data['password']);
			unset($data['confPass']);
			unset($data['conf_password']);

			if(!empty($data['id_account'])){
				// Update
				$id = $data['id_account'];
				unset($data['id_account']);
				

				if(!empty($password)){
					$data['password'] = password_hash($password,PASSWORD_DEFAULT);
				}

				$this->db->where('id_account',$id);
				$this->db->set($data);
				if($this->db->update('rbac_account')){
					return $id;
				}else{
					return false;
				}
			}else{
				$data['password'] = password_hash($password,PASSWORD_DEFAULT);
				if($this->db->insert('rbac_account',$data)){
					return $this->db->insert_id();
				}else{
					return false;
				}
			}
			return false;
		}
		public function delete($delete_id = null){
			if(!$delete_id){
				return false;
			}else{
				$this->db->where('id_account',$delete_id);
				$this->db->delete('rbac_account');
				return $delete_id;
			}
		}
		public function load($mode = 'LOADBY_ID',$params = array()){
			switch ($mode) {
				case 'LOADBY_ID':
					$this->db->where('id_account',$params['id_account']);
					break;
				case 'LOADBY_USERNAME':
					$this->db->where('username',$params['username']);
					break;
				default:
					$this->db->where('username',$this->session->userdata('username'));
					break;
			}
			$data = $this->db->get('rbac_account')->row();
			
			$CI =& get_instance();
			$CI->load->model('core/general_model');
			$result = $this->general_model->result();

			if($data){
				unset($data->password);
				$result->data = $data;
			}else{
				$result->code = 401;
				$result->info = 'No Data loaded';
			}
			return $result;
		}
	}