<?php
class Privileges_model extends CI_MODEL{
	public function load($mode = 'LOAD_ALL',$params = array(),$isMenu = true){
		if($isMenu){
			$this->db->where('type',0);
		}
		
		if($mode != 'LOAD_ALL'){
			switch ($mode) {
				case 'LOAD_PARENTBYROLE':
					if(!isset($params['id_role'])){
						return false;
					}
					$this->db->where('rbac_menu.parent is NULL');
					$this->db->where('rbac_privileges.id_role', $params['id_role']);
					break;
				case 'CHECK_DUPLICATE':
					$this->db->where('rbac_privileges.id_role',$params['id_role']);
					$this->db->where('rbac_privileges.id_menu', $params['id_menu']);
					break;
				case 'LOAD_CHILDBYROLE':
					$this->db->where('rbac_privileges.id_role',$params['id_role']);
					$this->db->where('rbac_menu.parent',$params['parent']);
					break;
				case 'LOADBY_IDMENUROLE':
					if(!isset($params['id_menu']) || !isset($params['id_role'])){
						return false;
					}
					$this->db->where('rbac_menu.id_menu',$params['id_menu']);
					$this->db->where('rbac_privileges.id_role',$params['id_role']);
					break;
			}
		}
		if(isset($params['limit'])){
			$this->db->limit(intval($params['limit']),intval($params['offset']));
		}

		$this->db->join('rbac_menu', 'rbac_menu.id_menu = rbac_privileges.id_menu');
		$q = $this->db->get('rbac_privileges');
		
		if($q){
			$result = new stdClass;
			$result->total = count($q->result());
			$result->data = $q->result();
			return $result;
		}else{
			return false;
		}
	}
	public function save($params = array()){
		if($this->db->insert('rbac_privileges',$params)){
			return true;
		}else{
			return false;
		}
	}
	public function delete($params = array()){
		$CI =& get_instance();
		$CI->load->model('core/general_model');
		
		$result = $this->general_model->result();
		// Delete selected menu
		$this->db->where('id_menu',$params['id_menu']);
		$this->db->where('id_role',$params['id_role']);
		if($this->db->delete('rbac_privileges')){
			$CI =& get_instance();
			$CI->load->model('menu_model');
			$child = $this->menu_model->loadList('LOAD_CHILD',array('parent'=>$params['id_menu']));
			$child = $child->data;
			foreach ($child->result() as $data) {
				$this->db->where('id_role',$params['id_role']);
				$this->db->where('id_menu',$data->id_menu);
				$this->db->delete('rbac_privileges');
			}
		}else{
			$result->code = 501;
			$result->info = 'Failed to deleting data';
		}
		return $result;
	}
	public function getChild($id_menu,$id_role){
		$child = $this->load('LOAD_CHILDBYROLE',array('parent' => $id_menu,'id_role' =>$id_role));
		$list = array();
		foreach ($child->data as $data) {
			$data->child = $this->getChild($data->id_menu,$id_role);
			$list[] = $data;
		}
		return $list;
	}
}