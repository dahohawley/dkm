<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authentication_model extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	public function saveLog($params){
		try {
			$this->db->insert('rbac_auth_log',$params);
			return $this->db->insert_id();
		} catch (Exception $e) {
			error_log('Error at : '.$ex->getFile().' Line : '.$ex->getLine(). ' : '. $ex->getMessage());
		}
	}
	public function checkPrivileges($url,$id_role){
		$CI =& get_instance();
		$CI->load->model('rbac/Menu_model');
		$CI->load->model('core/general_model');

		$menu = $this->Menu_model->loadList('LOADBY_REFERENCES',array('references' => $url))->data->row();
		$result = $this->general_model->result();
		if($menu){
			$id_menu = $menu->id_menu;
			$CI->load->model('rbac/Privileges_model');
			$privileges = $this->Privileges_model->load('LOADBY_IDMENUROLE',array('id_menu' => $id_menu, 'id_role' => $id_role ));
			
			if(!$privileges->total){
				$result->code = 502;
				$result->info = 'You have no right to access this page';
			}
		}else{
			$result->code = 501;
			$result->info = "Invalid References";
		}
		return $result;
	}

}

/* End of file Authentication_model.php */
/* Location: ./application/modules/rbac/models/Authentication_model.php */