<div class="btn-group" id="btnGroup-menu">
	<button class="btn btn-success" onclick="refreshTableMenu()">
		<span class="fa fa-refresh"></span>
	</button>
	<button class="btn btn-primary" id="button-add">
		<span class="fa fa-plus"></span> Add new menu
	</button>
</div>
<?php echo $this->template->cardOpen('Data Menu');?>

<?php echo $this->template->cardBodyOpen();?>
	<table class="table table-hover table-condensed" id="table-menu">
		<thead>
			<tr>
				<th>ID Menu</th>
				<th>Label</th>
				<th>Parent</th>
				<th>References</th>
				<th>Type</th>
				<th>Icon</th>
				<th>Action</th>
			</tr>
		</thead>
	</table>
<?php echo $this->template->cardBodyClose();?>

<script type="text/javascript">
	$(document).ready(function(){
		$("#button-add").click(function(){
			initParent();
			$("#form-menu")[0].reset();
			$("#id_menu").val(0);
			$("#modal-addMenu-title").text('Add new menu');
			$("#modal-addMenu").modal('show');
		});
		$("#table-menu").DataTable({
			processing:true,
			serverSide:true,
			ordering:false,
			ajax : {
				url:"<?php echo site_url('rbac/menu/loadList') ?>",
				type:"POST"
			},
			columns:[
				{data:"id_menu"},
				{data:"label"},
				{data:"parent"},
				{data:"references"},
				{
					data:"type",
					render:function(data){
						if(data == 0){
							return 'Menu';
						}else if(data == 1){
							return 'Post Menu';
						}
					}
				},
				{
					data:"icon",
					render:function(data){
						return '<span class="'+data+'"></span>';
					}
				},
				{
					data:"action",
					render:function(data,type,row,meta){
						txt = '';
						txt += '<button ';
						txt += '	class="btn btn-primary btn-xs command-edit"';
						txt += '	id_menu="'+row.id_menu+'"';
						txt += '	label="'+row.label+'"';
						txt += '	parent="'+row.parent+'"';
						txt += '	type="'+row.type+'"';
						txt += '	icon="'+row.icon+'"';
						txt += '	references="'+row.references+'"';
						txt += '>';
						txt += '	<span class="fa fa-pencil-alt"></span>';
						txt += '</button>';

						txt += '<button ';
						txt += '	class="btn btn-danger btn-xs command-delete"';
						txt += '	id_menu="'+row.id_menu+'"';
						txt += '	label="'+row.label+'"';
						txt += '	parent="'+row.parent+'"';
						txt += '	type="'+row.type+'"';
						txt += '	icon="'+row.icon+'"';
						txt += '	references="'+row.references+'"';
						txt += '>';
						txt += '	<span class="fa fa-trash"></span>';
						txt += '</button>';

						return txt;
					}
				}
			],
			columnDefs:[
				{
					targets:-1,
					orderable:false,
				}
			]
		});
		$("#table-menu_filter").append($("#btnGroup-menu"));
		$("#table-menu tbody").on('click','.command-edit',function(){
			initParent();
			that = $(this);
			id_menu = that.attr('id_menu');
			label = that.attr('label');
			parent = that.attr('parent');
			references = that.attr('references');
			type = that.attr('type');
			icon = that.attr('icon');


			$("#modal-addMenu-title").text('Add Menu #'+id_menu);
			$("#id_menu").val(id_menu);
			$("#label").val(label);
			$("#parent").val(parent);
			$("#references").val(references);
			$("#type").val(type);
			$("#icon").val(icon);

			$("#modal-addMenu").modal('show');
		});
		$("#table-menu tbody").on('click','.command-delete',function(){
			id = $(this).attr('id_menu');

			$("#modal-deleteMenu-title").text('Delete this Menu ? #'+id);
			$("#delete_id").val(id);
			$("#modal-deleteMenu").modal('show');
		});
		$("#buttonDelete").click(function(){
			$.ajax({
				url:"<?php echo site_url('rbac/Menu/delete') ?>",
				type:"POST",
				data : {
					menu_id : $("#delete_id").val(),
				},
				beforeSend:function(){
					buttonSpinner('buttonDelete');
				},
				success:function(res){
					res = JSON.parse(res);
					if(res.code == 200){
						custom_notification('success','Success deleting data');
					}else{
						custom_notification('danger','Failed deleting data');
					}
				},
				complete:function(){
					removeButtonSpinner('buttonDelete');
					refreshTableMenu();
					$(".modal").modal('hide');
				}
			});
		});
		$("#button-saveForm").click(function(){
			$.ajax({
				url:"<?php echo site_url('rbac/Menu/save') ?>",
				type:"POST",
				data : $("#form-menu").serialize(),
				beforeSend:function(){
					buttonSpinner("button-saveForm");
				},
				success:function(res){
					res = JSON.parse(res);
					if(res.code == 200){
						custom_notification('success','Success');
						refreshTableMenu();
					}else{
						custom_notification('danger','Failed adding new Data');
					}
				},
				complete:function(){
					removeButtonSpinner("button-saveForm");
					$("#modal-addMenu").modal('hide');
				}
			});
		});
	});
	function initParent(){
		$("#parent").empty();
		$.ajax({
			url:"<?php echo site_url('rbac/menu/loadList') ?>",
			success:function(res){
				res = JSON.parse(res);
				data = res.data.rows;
				$("#parent").append("<option value>Choose Parent</option>");
				$.each(data,function(column,row){
					txt = '';
					txt += '<option value="'+row.id_menu+'">'+row.label+'</option>';
					$("#parent").append(txt);
				});
			}
		});
	}
	function refreshTableMenu(){
		$("#table-menu").DataTable().ajax.reload();
	}
</script>

<div id="modal-addMenu" class="modal fade" menu="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal-addMenu-title"></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-menu">
            		<input type="hidden" name="id_menu" id="id_menu">
                	<div class="form-group">
                		<label class="control-label col-md-3">Label</label>
                		<div class="col-md-9">
                			<input type="text" class='form-control' name="label" id="label">
                		</div>
                	</div>
                	<div class="form-group">
                		<label class="control-label col-md-3">Parent</label>
                		<div class="col-md-9">
	                		<select name="parent" class="form-control" id="parent">
	                		</select>
                		</div>
                			
                	</div>
                	<div class="form-group">
                		<label class="control-label col-md-3">References</label>
                		<div class="col-md-9">
                			<input type="text" class='form-control' name="references" id="references">
                		</div>
                	</div>
                	<div class="form-group">
                		<label class="control-label col-md-3">Type</label>
                		<div class="col-md-9">
                			<select name="type" class="form-control" id="type">
                				<option value="0">Menu</option>
                				<option value="1">Post Menu</option>
                			</select>
                		</div>
                	</div>
                	<div class="form-group">
                		<label class="control-label col-md-3">Icon</label>
                		<div class="col-md-9">
                			<input type="text" class='form-control' name="icon" id="icon">
                		</div>
                	</div>
                </form>
            </div>
            <div class="modal-footer">
            	<button class="btn btn-primary" id="button-saveForm">
            		<span class="fa fa-save"></span> Save
            	</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="modal-deleteMenu" class="modal fade" menu="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal-deleteMenu-title">Delete</h4>
            </div>
            <div class="modal-body">
            	Are you sure want to delete this data ? 
				<input type="hidden" name="delete_id" id="delete_id">
            </div>
            <div class="modal-footer">
            	<button id="buttonDelete" class="btn btn-danger">
            		<span class="fa fa-trash"></span> Delete
            	</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
