<div class="btn-group" id="buttonGroup-account">
	<button class="btn btn-success" id="button-refresh" onclick="refreshTableAccount()">
		<span class="fa fa-refresh"></span>
	</button>
	<button class="btn btn-primary" id="button-createNew">
		<span class="fa fa-plus"></span> Create New Account
	</button>
</div>
<?php echo $this->template->cardOpen('Account');?>

<?php echo $this->template->cardBodyOpen();?>
	<table class="talbe table-hover table-condensed" id="table-account">
		<thead>
			<tr>
				<th>ID Account</th>
				<th>Username</th>
				<th>Email</th>
				<th>Role</th>
				<th>Account Status</th>
				<th>Action</th>
			</tr>
		</thead>
	</table>
<?php echo $this->template->cardBodyClose();?>

<script type="text/javascript">
	$(document).ready(function(){
		$("#table-account").DataTable({
			processing:true,
			serverSide:true,
			ordering:false,
			ajax:{
				url:"<?php echo site_url('rbac/Account/loadList') ?>",
				type:"POST"
			},
			columns:[
				{data:"id_account"},
				{data:"username"},
				{data:"email"},
				{data:"role_name"},
				{
					data:"account_status",
					render:function(data){
						if(data == 0){
							return '<span class="label label-default">Pending</span>';
						}else if(data == 1){
							return '<span class="label label-primary">Active</span>'
						}else if(data == 2){
							return '<span class="label label-danger">Banned</span>';
						}
					}
				},
				{
					data :"action",
					render:function(data,type,row,meta){
						txt = '';
						txt += '<button';
						txt += '	class="btn btn-primary btn-xs command-edit"';
						txt += '	id_account="'+row.id_account+'"';
						txt += '	username="'+row.username+'"';
						txt += '	email="'+row.email+'"';
						txt += '	id_role="'+row.id_role+'"';
						txt += '	account_status="'+row.account_status+'"';
						txt += '>';
						txt += '	<span class="fa fa-pencil-alt"></span>';
						txt += '</button> ';
						txt += '<button';
						txt += '	class="btn btn-danger btn-xs command-delete"';
						txt += '	id_account="'+row.id_account+'"';
						txt += '	username="'+row.username+'"';
						txt += '	email="'+row.email+'"';
						txt += '	id_role="'+row.id_role+'"';
						txt += '	account_status="'+row.account_status+'"';
						txt += '>';
						txt += '	<span class="fa fa-trash"></span>';
						txt += '</button>';
						return txt;
					}
				}	
			]
		});
		$("#table-account_filter").append($("#buttonGroup-account"));
		$("#table-account tbody").on('click','.command-edit',function(){
			$("#form-addAccount")[0].reset();
			that = $(this);
			$("#username").attr('readonly','true');
			$("#id_account").val(that.attr('id_account'));
			$("#username").val(that.attr('username'));
			$("#email").val(that.attr('email'));
			$("#id_role").val(that.attr('id_role'));
			$("#account_status").val(that.attr('account_status'));

			$("#modalTitle-addAcount").text('Edit Account #'+that.attr('id_account'));
			$("#modal-addAccount").modal('show');
		});
		$("#table-account tbody").on('click','.command-delete',function(){
			id = $(this).attr('id_account');
			$("#modal-deleteAccount-title").text('Delete this data ? #'+id);
			$("#delete_id").val(id);
			$("#modal-deleteAccount").modal('show');
		});
		$("#button-createNew").click(function(){
			$("#modalTitle-addAcount").text('Create new Account');
			$("#username").removeAttr('readonly');
			$("#modal-addAccount").modal('show');
			$("#id_account").val(0);
			$("#form-addAccount")[0].reset();
		});
		$("#button-saveAccount").click(function(){
			$.ajax({
				url:"<?php echo site_url('rbac/Account/save') ?>",
				type:"POST",
				data : $("#form-addAccount").serialize(),
				beforeSend:function(){
					// buttonSpinner('button-saveAccount');
				},
				success:function(res){
					res = JSON.parse(res);
					if(res.code == 200){
						custom_notification('success','Success creating new account');
					}else{
						custom_notification('danger','<b>Failed Creating new Account </b>: <br />'+res.info );
					}
				},
				complete:function(){
					$(".modal").modal('hide');
					removeButtonSpinner('button-saveAccount');
					refreshTableAccount();
				},
				error:function(){
					custom_notification('warning','Error, Please contact administrator.');
					$(".modal").modal('hide');
					removeButtonSpinner('button-saveAccount');
				}
			});
		});

		// Init role
		$.ajax({
			url:"<?php echo site_url('rbac/Roles/loadList') ?>",
			success:function(res){
				res = JSON.parse(res);
				if(res.code == 200){
					data = res.data.rows;
					$.each(data,function(column,row){
						txt = '';
						txt += '<option value="'+row.id_role+'">'+row.name+'</option>';
						$("#id_role").append(txt);
					});
				}else{
					custom_notification('Failed loading parameters. Please try to reload. or contact administrator');
				}
			}
		});

		$("#buttonDelete").click(function(){
			$.ajax({
				url:"<?php echo site_url('rbac/account/delete') ?>",
				type:"POST",
				data : {
					role_id : $("#delete_id").val(),
				},
				beforeSend:function(){
					buttonSpinner('buttonDelete');
				},
				success:function(res){
					res = JSON.parse(res);
					if(res.code == 200){
						custom_notification('success','Success deleting data');
					}else{
						custom_notification('danger','Failed deleting data');
					}
				},
				complete:function(){
					removeButtonSpinner('buttonDelete');
					refreshTableAccount();
					$(".modal").modal('hide');
				}
			});
		});
	});
	function refreshTableAccount(){
		$("#table-account").DataTable().ajax.reload();	
	}
</script>

<div id="modal-addAccount" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modalTitle-addAcount">Create new Account</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-addAccount">
                	<input type="hidden" name="id_account" value="0" id="id_account">
                	<div class="form-group">
                		<label class="control-label col-md-3">Username</label>
                		<div class="col-md-9">
                			<input type="text" class='form-control' name="username" id="username">
                		</div>
                	</div>
                	<div class="form-group">
                		<label class="control-label col-md-3">Email</label>
                		<div class="col-md-9">
                			<input type="email" class='form-control' name="email" id="email">
                		</div>
                	</div>
                	<div class="form-group">
                		<label class="control-label col-md-3">Password</label>
                		<div class="col-md-9">
                			<input type="password" class='form-control' name="password" id="password">
                		</div>
                	</div>
                	<div class="form-group">
                		<label class="control-label col-md-3">Confirmation Password</label>
                		<div class="col-md-9">
                			<input type="password" class='form-control' name="conf_password" id="conf_password">
                		</div>
                	</div>
                	<div class="form-group">
                		<label class="control-label col-md-3">Role</label>
                		<div class="col-md-9">
                			<select name="id_role" class="form-control" id="id_role">
                				
                			</select>
                		</div>			
                	</div>
                	<div class="form-group">
                		<label class="control-label col-md-3">Account Status</label>
                		<div class="col-md-9">
                			<select name="account_status" id="account_status" class="form-control"> 
                				<option value="0">Pending</option>
                				<option value="1">Active</option>
                				<option value="2">Banned</option>
                			</select>
                		</div>
                	</div>
                </form>
            </div>
            <div class="modal-footer">
            	<button class="btn btn-primary" id="button-saveAccount">
            		<span class="fa fa-save"></span> Save
            	</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="modal-deleteAccount" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal-deleteAccount-title">Delete</h4>
            </div>
            <div class="modal-body">
            	Are you sure want to delete this data ? 
				<input type="hidden" name="delete_id" id="delete_id">
            </div>
            <div class="modal-footer">
            	<button id="buttonDelete" class="btn btn-danger">
            		<span class="fa fa-trash"></span> Delete
            	</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
