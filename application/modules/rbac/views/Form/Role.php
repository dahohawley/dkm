<div class="btn-group" id="btnGroup-role">
	<button class="btn btn-success" onclick="refreshTableRole()">
		<span class="fa fa-refresh"></span>
	</button>
	<button class="btn btn-primary" id="button-add">
		<span class="fa fa-plus"></span> Add new role
	</button>
</div>
<?php echo $this->template->cardOpen('Data Role');?>

<?php echo $this->template->cardBodyOpen();?>
	<table class="table table-hover table-condensed" id="table-role">
		<thead>
			<tr>
				<th>ID Role</th>
				<th>Role Name</th>
				<th>Action</th>
			</tr>
		</thead>
	</table>
<?php echo $this->template->cardBodyClose();?>

<script type="text/javascript">
	$(document).ready(function(){
		$("#button-add").click(function(){
			$("#form-role")[0].reset();
			$("#id_role").val(0);
			$("#modal-addRole").modal('show');
		});
		$("#table-role").DataTable({
			processing:true,
			serverSide:true,
			ordering:false,
			ajax : {
				url:"<?php echo site_url('rbac/roles/loadList') ?>",
				type:"POST"
			},
			columns:[
				{data:"id_role"},
				{data:"name"},
				{
					data:"action",
					render:function(data,type,row,meta){
						txt = '';
						txt += '<button ';
						txt += '	class="btn btn-primary btn-xs command-edit"';
						txt += '	id_role="'+row.id_role+'"';
						txt += '	name="'+row.name+'"';
						txt += '>';
						txt += '	<span class="fa fa-pencil-alt"></span>';
						txt += '</button>';

						txt += '<button ';
						txt += '	class="btn btn-danger btn-xs command-delete"';
						txt += '	id_role="'+row.id_role+'"';
						txt += '	name="'+row.name+'"';
						txt += '>';
						txt += '	<span class="fa fa-trash"></span>';
						txt += '</button>';

						return txt;
					}
				}
			],
			columnDefs:[
				{
					targets:-1,
					orderable:false,
					clasName:'align-right'
				}
			]
		});
		$("#table-role_filter").append($("#btnGroup-role"));

		$("#table-role tbody").on('click','.command-edit',function(){
			that = $(this);
			id_role = that.attr('id_role');
			name = that.attr('name');

			$("#id_role").val(id_role);
			$("#name").val(name);

			$("#modal-addRole").modal('show');
		});

		$("#table-role tbody").on('click','.command-delete',function(){
			id = $(this).attr('id_role');
			$("#modal-deleteRole-title").text('Delete this data ? #'+id);
			$("#delete_id").val(id);
			$("#modal-deleteRole").modal('show');
		});

		$("#buttonDelete").click(function(){
			$.ajax({
				url:"<?php echo site_url('rbac/Roles/delete') ?>",
				type:"POST",
				data : {
					role_id : $("#delete_id").val(),
				},
				beforeSend:function(){
					buttonSpinner('buttonDelete');
				},
				success:function(res){
					res = JSON.parse(res);
					if(res.code == 200){
						custom_notification('success','Success deleting data');
					}else{
						custom_notification('danger','Failed deleting data');
					}
				},
				complete:function(){
					removeButtonSpinner('buttonDelete');
					refreshTableRole();
					$(".modal").modal('hide');
				}
			});
		});

		$("#button-saveForm").click(function(){
			$.ajax({
				url:"<?php echo site_url('rbac/Roles/save') ?>",
				type:"POST",
				data : $("#form-role").serialize(),
				beforeSend:function(){
					buttonSpinner("button-saveForm");
				},
				success:function(res){
					res = JSON.parse(res);
					if(res.code == 200){
						custom_notification('success','Success');
						refreshTableRole();
					}else{
						custom_notification('danger','Failed adding new Data');
					}
				},
				complete:function(){
					removeButtonSpinner("button-saveForm");
					$("#modal-addRole").modal('hide');
				}
			});
		});
	});

	function refreshTableRole(){
		$("#table-role").DataTable().ajax.reload();
	}
</script>

<div id="modal-addRole" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal-addRole-title"></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-role">
                	<div class="form-group">
                		<input type="hidden" name="id_role" id="id_role">
                	</div>
                	<div class="form-group">
                		<label class="control-label col-md-3">Role Name</label>
                		<div class="col-md-9">
                			<input type="text" class='form-control' name="name" id="name">
                		</div>
                	</div>
                </form>
            </div>
            <div class="modal-footer">
            	<button class="btn btn-primary" id="button-saveForm">
            		<span class="fa fa-refresh"></span> Save
            	</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="modal-deleteRole" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal-deleteRole-title">Delete</h4>
            </div>
            <div class="modal-body">
            	Are you sure want to delete this data ? 
				<input type="hidden" name="delete_id" id="delete_id">
            </div>
            <div class="modal-footer">
            	<button id="buttonDelete" class="btn btn-danger">
            		<span class="fa fa-trash"></span> Delete
            	</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
