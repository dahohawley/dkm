<style type="text/css">
	.card-default{
		cursor: pointer;
	}
</style>
<div class="row">
	<div class="col-md-12" id="col-roles">
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		getRole();
		$("#form-addMenu").submit(function(e){
			e.preventDefault();
			id_role = $("#addMenu-id_role").val();
			$.ajax({
				url:"<?php echo site_url('rbac/privileges/save') ?>",
				type:"POST",
				data : $("#form-addMenu").serialize(),
				beforeSend:function(){
					buttonSpinner("buttonSave-addMenu");
				},
				success:function(res){
					res = JSON.parse(res);
					if(res.code == 200){
						$("#header-"+id_role).trigger('click');
						$("#header-"+id_role).attr('loaded','false');
						$("#body-"+id_role).empty();
						custom_notification('success','Success adding new menu to this role');
					}else{
						custom_notification('danger',res.info);
					}
				},
				complete:function(){
					removeButtonSpinner('buttonSave-addMenu');
					$(".modal").modal('hide');
					$("card-header")
				},
				error:function(){
					custom_notification('danger','Error! Please try again.');
					removeButtonSpinner();
					$(".modal").modal('hide');
				}
			});
		});

		$("#buttonDelete-menu").click(function(){
			id_menu = $("#delete-id_menu").val();
			id_role = $("#delete-id_role").val();
			$.ajax({
				url:"<?php echo site_url('rbac/privileges/delete') ?>",
				type:"POST",
				data : {
					id_menu : id_menu,
					id_role : id_role,
				},
				beforeSend:function(){
					buttonSpinner('buttonDelete-menu');
				},
				success:function(res){
					res = JSON.parse(res);
					if(res.code == 200){
						custom_notification('success','Success deleting menu.');
					}else{
						custom_notification('danger','Failed deleting menu. :'+res.info);
					}
					$("#header-"+id_role).trigger('click');
					$("#header-"+id_role).attr('loaded','false');
					$("#body-"+id_role).empty();
				},
				complete:function(){
					removeButtonSpinner("buttonDelete-menu");
					$(".modal").modal('hide');
				},
				error:function(){
					custom_notification('danger','Failed deleting menu. :'+res.info);
					removeButtonSpinner("buttonDelete-menu");
					$(".modal").modal('hide');
				}
			});
		});

		$("#form-addParent").submit(function(e){
			e.preventDefault();
			id_role = $("#formAddParent-id_role").val();
			id_menu = $("#formAddParent-id_menu").val();
			$.ajax({
				url:"<?php echo site_url('rbac/privileges/save') ?>",
				type:"POST",
				data : {
					id_role : $("#formAddParent-id_role").val(),
					id_menu : $("#formAddParent-id_menu").val()
				},
				beforeSend:function(){
					buttonSpinner("buttonSave-addParente");
				},
				success:function(res){
					res = JSON.parse(res);
					if(res.code == 200){
						custom_notification('success','Success deleting menu.');
					}else{
						custom_notification('danger','Failed deleting menu. :'+res.info);
					}
					$("#header-"+id_role).trigger('click');
					$("#header-"+id_role).attr('loaded','false');
					$("#body-"+id_role).empty();

				},
				complete:function(){
					$(".modal").modal('hide');
					removeButtonSpinner("buttonSave-addParent");
				}
			});	
		});
	});
	function getRole(){
		$.ajax({
			url:"<?php echo site_url('rbac/roles/loadList') ?>",
			success:function(res){
				res = JSON.parse(res);
				if(res.code == 200){
					data = res.data.rows;
					$.each(data,function(column,rows){
						txt = '';
						txt += '<div class="card card-default">';
						txt += '	<div class="card-header" data-toggle="collapse" id="header-'+rows.id_role+'" id_role="'+rows.id_role+'" data-target="#body-'+rows.id_role+'" loaded="false">';
						txt += 			'<span class="fa fa-caret-right"></span> '+rows.name;
						txt += '		<button class="btn btn-primary btn-xs pull-right button-addParent"><span class="fa fa-plus"></span></button>'
						txt += '	</div>';
						txt += '	<div class="card-body collapse" id="body-'+rows.id_role+'">';
						txt += '	</div>';
						txt += '</div>';
						$("#col-roles").append(txt);
					});

				}else{
					custom_notification('danger','Failed to fetch roles. please try again ');
				}
			},
			complete:function(){
				getParent();
			}
		});
	}
	$(document).on('click','.card-header',function(){
		toggleCaret($(this));
		// Cek udah di load atau belum
		if($(this).attr('loaded') == 'false'){
			// $(this).attr('loaded','true');	
			id_role = $(this).attr('id_role');
			$.ajax({
				url:"<?php echo site_url('rbac/privileges/loadList') ?>",
				type:"POST",
				data : {
					mode : "LOAD_PARENTBYROLE",
					id_role : id_role,
				},
				success:function(res){
					res = JSON.parse(res);
					if(res.code == 200){
						data = res.data.data;
						total = res.data.total;
						if(total > 0){
							txt = '<div class="list-group">';
							$.each(data,function(column,rows){
								txt += '<a href="#" loaded="false" class="list-group-item" level="0" id_menu="'+rows.id_menu+'" id_role="'+id_role+'">'
								txt += '	<span class="fa fa-caret-right"></span> '+rows.label;
								txt += '	<div class="btn-group pull-right">';
								txt += '		<button';
								txt += '			class="btn btn-success btn-xs command-addMenu"';
								txt += '			id_role="'+id_role+'"';
								txt += '			id_menu="'+rows.id_menu+'"';
								txt += '		>';
								txt += '			<span class="fa fa-plus"></span>';
								txt += '		</button>';
								txt += '		<button';
								txt += '			class="btn btn-danger btn-xs command-deleteMenu"';
								txt += '			id_role="'+id_role+'"';
								txt += '			id_menu="'+rows.id_menu+'"';
								txt += '		>';
								txt += '			<span class="fa fa-trash"></span>';
								txt += '		</button>';
								txt += '	</div>';
								txt += '</a>';
							});

							txt += '</div>';
							$("#header-"+id_role).attr('loaded','true');
							$("#body-"+id_role).append(txt);
						}else{
							$("#header-"+id_role).attr('loaded','true');
							$("#body-"+id_role).append('<center>No data</center>');
						}
					}else{
						custom_notification('danger','Failed to fetch roles. please try again ');
					}
				}
			});
		}
	});
	$(document).on('click','.list-group-item',function(){
		parent = $(this);
		toggleCaret(parent);
		parentLevel = parseInt($(this).attr('level'));
		newLevel = parentLevel + 1;
		space = '&nbsp;&nbsp;';
		id_role = $(parent).attr('id_role');
		loaded = $(parent).attr('loaded');
		if(loaded == 'false'){
			$.ajax({
				url:"<?php echo site_url('rbac/privileges/loadList') ?>",
				type:"POST",
				data : {
					mode : 'LOAD_CHILDBYROLE',
					id_role : id_role,
					parent : parent.attr('id_menu')
				},
				success:function(res){
					res = JSON.parse(res);
					if(res.code == 200){
						var newSpace = '';
						for(i=0;i<=newLevel;i++){
							newSpace += space;
						}
						$.each(res.data.data,function(column,rows){
							txt = '';
							txt += '<a href="#" class="list-group-item" loaded="false" id_role="'+id_role+'" level="'+newLevel+'" id_menu="'+rows.id_menu+'">'
							txt += newSpace+'<span class="fa fa-caret-right"></span> '+rows.label;
							txt += '	<div class="btn-group pull-right">';
							txt += '		<button';
							txt += '			class="btn btn-success btn-xs command-addMenu"';
							txt += '			id_role="'+id_role+'"';
							txt += '			id_menu="'+rows.id_menu+'"';
							txt += '		>';
							txt += '			<span class="fa fa-plus"></span>';
							txt += '		</button>';
							txt += '		<button';
							txt += '			class="btn btn-danger btn-xs command-deleteMenu"';
							txt += '			id_role="'+id_role+'"';
							txt += '			id_menu="'+rows.id_menu+'"';
							txt += '		>';
							txt += '			<span class="fa fa-trash"></span>';
							txt += '		</button>';
							txt += '	</div>';
							txt += '</a>';
							$(parent).after(txt);
						});
						$(parent).attr('loaded','true');
					}
				}
			});
		}
	});
	$(document).on('click','.command-addMenu',function(){
		parent = $(this).parent();
		id_role = $(this).attr('id_role');
		id_menu = $(this).attr('id_menu');

		$("#addMenu-id_role").val(id_role);
		$("#modal-addMenu").modal('show');
		getMenu(id_menu);
	}); 
	$(document).on('click','.command-deleteMenu',function(){
		$("#modal-deleteMenu").modal('show');
		id_role = $(this).attr('id_role');
		id_menu = $(this).attr('id_menu');
		$("#delete-id_role").val(id_role);
		$("#delete-id_menu").val(id_menu);
	});
	$(document).on('click','.button-addParent',function(){
		id_role = $(this).parent().attr('id_role');
		$("#modal-addParent").find('.modal-title').text('Add Parent to Role #'+id_role);
		$("#modal-addParent").modal('show');
		$("#formAddParent-id_role").val(id_role);
	});
	function toggleCaret(element){
		caret = $(element).find('span').attr('class');
		if(caret == 'fa fa-caret-right'){
			newCaret = 'fa fa-caret-down';
		}else if(caret == 'fa fa-caret-down'){
			newCaret = 'fa fa-caret-right';
		}
		$(element).find('span:first').attr('class',newCaret);
	}
	function getMenu(id_menu = null,mode = 'LOAD_CHILD'){
		$("#id_menu").empty();
		$.ajax({
			url:"<?php echo site_url('rbac/menu/loadList') ?>",
			type:"POST",
			data : {
				parent : id_menu,
				mode : mode
			},
			success:function(res){
				$("#buttonSave-addMenu").removeAttr('disabled');
				res = JSON.parse(res);
				if(res.data.total > 0){
					$.each(res.data.rows,function(column,rows){
						opt = new Option(rows.label,rows.id_menu);
						$("#id_menu").append(opt);
					});
				}else{
					opt = new Option('No Children','0');
					$("#id_menu").append(opt);
					$("#buttonSave-addMenu").prop('disabled','true');
				}
			}
		});
	}
	function getParent(){
		$.ajax({
			url:"<?php echo site_url('rbac/menu/loadList') ?>",
			type:"POST",
			data : {
				mode : "LOAD_PARENT"
			},
			success:function(res){
				res = JSON.parse(res);
				if(res.code == 200){
					data = res.data.rows;
					$.each(data,function(rows,column){
						opt = new Option(column.label,column.id_menu);
						$("#formAddParent-id_menu").append(opt);
					});
				}else{
					custom_notification('danger','Failed fetching parent.');
				}
			}
		});
	}

</script>

<div id="modal-addMenu" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Menu</h4>
            </div>
            <div class="modal-body">
                <form action="#" id="form-addMenu" class="form-horizontal">
                	<input type="hidden" name="id_role" id="addMenu-id_role">
                	<div class="form-group">
                		<label class="control-label col-md-3">Menu</label>
                		<div class="col-md-9">
                			<select name="id_menu" id="id_menu" class="form-control">
                				
                			</select>
                		</div>
                	</div>
            </div>
            <div class="modal-footer">
            	<button class="btn btn-primary" id="buttonSave-addMenu">
            		<span class="fa fa-save"></span> Save
            	</button>
                </form>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="modal-deleteMenu" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete Menu</h4>
            </div>
            <div class="modal-body">
				<form class="form-horizontal">
					<div class="form-group">
						<label class="control-label col-md-3">ID Menu</label>
						<div class="col-md-9">
							<input type="text" class='form-control' value="" readonly name="deleteID" id="delete-id_menu">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Role</label>
						<div class="col-md-9">
							<input type="text" class='form-control' value="" readonly name="delete-id_role" id="delete-id_role">
						</div>
					</div>
				</form>
            </div>
            <div class="modal-footer">
            	<button class="btn btn-danger" id="buttonDelete-menu">
            		<span class="fa fa-trash"></span> Delete
            	</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="modal-addParent" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
            	<form class="form-horizontal" id="form-addParent">
            		<input type="hidden" name="id_role" required id="formAddParent-id_role">
            		<div class="form-group">
            			<label class="control-label col-md-3">Choose Parent</label>
            			<div class="col-md-9">
            				<select name="id_menu" class="form-control" id="formAddParent-id_menu">
            					
            				</select>
            			</div>
            		</div>
            </div>
            <div class="modal-footer">
            	<button class="btn btn-primary" id="buttonSave-addParent">
            		<span class="fa fa-save"></span> Save
            	</button>
            	</form>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
