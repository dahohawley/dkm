<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . '/modules/core/controllers/Controller.php';

class Authentication extends Controller{
	public function __construct(){
		parent::__construct();
	}
	public function login(){
		$post = $this->input->post();
		$this->load->model('core/general_model');
		$result = $this->general_model->result();
		if(!isset($post['username'])){
			$result->code = 401;
			$result->info = 'Please insert username.';
			echo json_encode($result);
			return false;
		}else if(!isset($post['password'])){
			$result->code = 402;
			$result->info = 'Please insert Password.';
			echo json_encode($result);
			return false;
		}
		$params['username'] = $post['username'];

		$this->load->model('Account_model');
		$fetch = $this->Account_model->loadList('AUTHENTICATION',$params)->data;
		if($fetch->row()){
			// username Exists
			$account_status = $fetch->row()->account_status;
			$storedPassword = $fetch->row()->password;

			if($account_status == 0){

				// Pending Block
				$result->code = 403;
				$result->info = 'This account is not actived yet.';

			}else if($account_status == 2){
				
				// Banned Block
				$result->code = 405;
				$result->info = 'This account is banned.';

			}else{

				// Active Account
				if(password_verify($post['password'], $storedPassword)){
					$sessData = $fetch->row_array();
					unset($sessData['password']);
					$sessData['logged_in'] = true;
					$this->session->set_userdata($sessData);
					// Save to log
					$params = array(
						'id_account' => $sessData['id_account'],
						'auth_type' => 0,
						'ip_address' => $this->input->ip_address(),
						'auth_dtm' => date('Y-m-d H:i:s')
					);
					$this->load->model('Authentication_model');
					$this->Authentication_model->saveLog($params);
					$result->data = $sessData;
				}else{
					// Password didnt match.
					$result->code = 406;
					$result->info = 'Password did not match';
				}
			}
		}else{
			// Username doesnt exists.
			$result->code = 407;
			$result->info = 'Username doesnt exist';
		}
		echo json_encode($result);
		return false;
	}
	public function logout(){
		$this->load->model('core/general_model');
		$result = $this->general_model->result();
		$sessData = $this->session->userdata();
		$this->session->sess_destroy();
		$params = array(
			'id_account' => @$sessData['id_account'],
			'auth_type' => 1,
			'ip_address' => $this->input->ip_address(),
			'auth_dtm' => date('Y-m-d H:i:s')
		);
		$this->load->model('Authentication_model');
		$this->Authentication_model->saveLog($params);
		echo json_encode($result);
	}
}	