<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . '/modules/core/controllers/Controller.php';

class Form extends Controller{
	public function __construct(){
		parent::__construct();
		// $this->grant();
	}
	public function Roles(){
		$this->using('datatable');
		$this->dispatch(DEF_TEMPLATE_INSIDE,'Form/Role');
	}
	public function Account(){
		$this->using('datatable');
		$this->dispatch(DEF_TEMPLATE_INSIDE,'Form/Account');
	}
	public function Menu(){
		$this->using('datatable');
		$this->dispatch(DEF_TEMPLATE_INSIDE,'Form/Menu'); 
	}
	public function Privileges(){
		$this->dispatch(DEF_TEMPLATE_INSIDE,'Form/Privileges');
	}
}	