<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . '/modules/core/controllers/Controller.php';

class Privileges extends Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('Privileges_model','model');
	}
	
	public function save(){
		$params = $this->input->post();

		// CHECK_DUPLICATE
		$cekDuplicate = $this->model->load('CHECK_DUPLICATE',$params);

		$this->load->model('core/general_model');
		$result = $this->general_model->result();
		
		if($cekDuplicate->total){
			$result->code = 401;
			$result->info = 'This role already has menu you submited.';
		}else{
			// get detail menu
			$this->load->model('menu_model');
			$menu = $this->menu_model->loadList('LOADBY_ID',array('id_menu' => $params['id_menu']))->data->row();
			$parentExistence = false;
			if($menu->parent){
				$cekParentExistence = $this->model->load('CHECK_DUPLICATE',array('id_menu' => $menu->parent,'id_role' => $params['id_role']));
				if($cekParentExistence->total){
					$parentExistence = true;
				}
			}else{
				$parentExistence = true;
			}
			if(!$parentExistence){
				$result->code = 402;
				$result->info = "Please add parent of this menu first!";
			}else{
				$params['update_dtm'] = date('Y-m-d H:i:s');
				if(!$this->model->save($params)){
					$result->code = 501;
					$result->info = 'Failed Saving data';
				}
			}
		}
		echo json_encode($result);
	}	public function delete(){
		$post = $this->input->post();
		$delete = $this->model->delete($post);
		echo json_encode($delete);
	}
	public function loadList(){
		$post = $this->input->post();
		$mode = isset($post['mode']) ? $post['mode'] : 'LOAD_ALL'; 
		$data = $this->model->load($mode,$post);

		$this->load->model('core/general_model');
		$result = $this->general_model->result();
		if($data){
			$result->data = $data;	
		}else{
			$result->code = 501;
			$result->info = 'Failed to load data';
		}
		echo json_encode($result);
	}
	public function loadMenu(){
		$post = $this->input->post();
		
		$this->load->model('core/general_model');
		$result = $this->general_model->result();

		if(isset($post['id_role'])){
			$id_role = $post['id_role'];
		}else{
			$sess = $this->session->userdata();	
			$id_role = $sess['id_role'];
		}

		if(!$id_role){
			$result->code = 401;
			$result->info= 'Mandatory parameters required! Please see documentation';
		}else{
			$this->load->model('Privileges_model');
			$parent = $this->Privileges_model->load('LOAD_PARENTBYROLE',array('id_role' => $id_role));
			$list = array();
			if($parent->total > 0){
				foreach ($parent->data as $data) {
					$child = $this->Privileges_model->getChild($data->id_menu,$id_role);
					$data->child = $child;
					$list[] = $data;
				}
			}
		}
		$result->data = $list;
		echo json_encode($result);
		return true;
	}
}