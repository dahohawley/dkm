<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Layout_model extends CI_Model {

	protected $_header = array();
	protected $_footer = array();
	public function __construct(){
		$this->load->helper('file');
		// Get Default Configuration
			$defaultConfig = read_file("assets/library/config.json");
			$defaultJson = json_decode($defaultConfig);
			$this->append('assets/library/',$defaultJson);
		// Get Theme Configuration
			$this->load->config('layout');
			$theme = $this->config->item('theme');
			$location = "assets/theme/".$theme.'/';
			$string = read_file($location."/config.json");
			$json = json_decode($string);
			$this->append($location,$json);

		
	}
	public function append($name,$array = null){
		$css = $array->css;
		$js = $array->js;
		foreach ($css as $data) {
			$this->_header[] = $name.$data;
		}
		foreach ($js as $data) {
			$this->_footer[] = $name.$data;
		}
	}
	public function drawHeader(){
		$header = '';
		if(is_array($this->_header)){
			foreach ($this->_header as $data) {
				$header .= "<link href='".base_url($data)."' rel='stylesheet'>\r\n";
			}
		}
		echo $header;
	}
	public function drawFooter(){
		$footer = '';
		if(is_array($this->_footer)){
			foreach ($this->_footer as $data) {
				$footer .= "<script src='".base_url($data)."'></script>\r\n";
			}
		}
		echo $footer;
	}
	public function resetTemplate(){
		$this->_header = array();
		$this->_footer = array();	
		$this->setMandatory();
	}
	public function setTemplate($templateName = null){
		$this->resetTemplate();
		
		$location = "assets/theme/".$templateName.'/';
		$string = read_file($location."/config.json");
		// var_dump($string);die();
		$json = json_decode($string);
		$this->append($location,$json);
	}
	public function setMandatory(){
		$defaultConfig = read_file("assets/library/config.json");
		$defaultJson = json_decode($defaultConfig);
		$this->append('assets/library/',$defaultJson);
	}
}

/* End of file Layout_model.php */
/* Location: ./application/modules/core/models/Layout_model.php */