# What is This?

Proyek ini dibuat sebagai dasar pembuatan aplikasi akuntansi. terdapat banyak library yang dapat digunakan untuk memudahkan developer dalam membangun aplikasi akuntansi berbasis web.

Proyek ini dibuat dengan menggunakan Framework Codeigniter 3.1.9 dengan DBMS MySQL

# Siklus Akuntansi

- [x] Jurnal
- [x] Buku Besar
- [x] Neraca Saldo
- [ ] Laporan Laba Rugi

# To Do :
- [x] Notif row di template
- [x] Buat notif rownya auto close

# Dokumentasi sementara 
- setiap model harus mengembalikan response seperti berikut : 
` $response = array(
	'status' => true,
	'info' => 'Informasi');`


# LOAD ANOTHER MODEL INSIDE A MODEL
```php

$CI =& get_instance();
$CI->load->model('nama_model');
$CI->nama_model->nama_fungsi();

```

Hello
