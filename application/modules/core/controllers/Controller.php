<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Controller extends CI_Controller {
	
	/**
	* @param $_priv
	* is used to check if the user has privileges on the page or not
	*/

	protected $_priv;

	/**
	* @param $_grant 
	* is used to check if the user has logged in or not
	*/

	protected $_grant;

	/**
	* @param $__freeme;
	* is used to bypass $_priv and $_grant; 
	*/

	protected $__freeMe;

	public function __construct(){
		parent::__construct();
		$this->checkPrivileges();
	}	
	public function grant(){
		$this->__freeMe = true;
	}
	public function checkPrivileges(){
		// Check Login
		$session = $this->session->userdata();

		if(array_key_exists('logged_in', $session) && $session['logged_in'] == true){
			$this->_grant = true;

			$id_role = $session['id_role'];
			
			$url = $this->uri->uri_string();
			
			if($url){
				$this->load->model('rbac/Authentication_model');
				$privileges = $this->Authentication_model->checkPrivileges($url,$id_role);
				if($privileges->code == 200){
					$this->_priv = true;
				}else{
					$this->_priv = false;
				}
			}else{
				$this->_priv = true;
			}
		}
	}
	public function dispatch($templateName = '' ,$viewName = '' , $variable = array() ){
		if(!$this->__freeMe){
			if(!$this->_grant){
				$this->session->set_flashdata('notif', '<div class="alert alert-danger">You need to logged in first.</div>');
				redirect( site_url($this->routes->default_controller) );
			}
			if(!$this->_priv){
				$this->session->set_flashdata('notif', '<div class="alert alert-danger">You have no right to access this page.</div>');
				redirect( site_url( $this->routes->default_controller ) );
				
			}
		}

		$this->template->load($templateName,$viewName,$variable);
	}
	public function alpha_dash_space($fullname){
	    if (! preg_match('/^[a-zA-Z\s]+$/', $fullname)) {
	        $this->form_validation->set_message('alpha_dash_space', 'The %s field may only contain alpha characters & White spaces');
	        return FALSE;
	    } else {
	        return TRUE;
	    }
	}
	public function map($columnName = '',$alias = null,$inputType = 'text' , $inputClass ='',$dataFormatting = '',$validation = null){
		if(is_array($inputType)){
			$type = $inputType['type'];
			$id = $inputType['id'];
			$name = $inputType['name'];
			// Load data into selected model
				if(isset($inputType['model'])){
					$aliasModel = str_replace('/','',strstr($inputType['model'],'/'));
					$this->load->model($inputType['model'],$aliasModel);
					$data = $this->$aliasModel->load()['data']['rows']->result();
				}else{
					$data = $this->convertArrayToObject($inputType['data']);
				}
			$inputType['data'] = $data;
		}
		return array(
			'columnName' => $columnName,
			'alias' => $alias,
			'inputType' => $inputType,
			'inputClass' => $inputClass,
			'dataFormatting' => '',
			'validation' => $validation
		);
	}

	public function using($name = null){
		// Loading Config
		if($name){
			$location = "assets/library/".$name.'/';
			// check file exists or not. if not exists then install first from repo.
			if(!file_exists($location)){
				$this->load->model('installation_model');
				$installLib = $this->installation_model->extractLibrary(null,$name);
				if(!$installLib){
					$this->session->set_flashdata('notif', '<div class="alert alert-danger">LIBRARY ERROR : '.$name.' not exists in local files and in repository.</div>');
					return false;
				}
			}
			$this->load->helper('file');
			$string = read_file($location."/config.json");
			$array = json_decode($string);
			$this->Layout_model->append($location,$array);
		}
	}
	
}

/* End of file Controller.php */
/* Location: ./application/modules/core/controllers/Controller.php */