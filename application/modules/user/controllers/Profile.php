<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . '/modules/core/controllers/Controller.php';

class Profile extends Controller{
	public function __construct(){
		parent::__construct();
		$this->grant();
		$this->load->model('UserMap_model');
	}
	public function index(){
		$this->dispatch(DEF_TEMPLATE_INSIDE,'Profile');
	}
	public function loadProfile(){
		$this->load->model('core/general_model');
		$result = $this->general_model->getResult();
		$params = $this->input->post();
		$id_account = isset($params['id_account']) ? $params['id_account'] : $this->session->userdata('id_account');
		$map = $this->UserMap_model->load(null,'LOADBY_IDACCOUNT',array('id_account' => $id_account))['data']['rows']->row();
		if($map){
			$result['data'] = $map;
		}else{
			$result['info'] = 'No Profile Loaded';
			$result['status'] = false;
		}
		echo json_encode($result);
	}
}