<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . '/modules/core/controllers/Controller.php';

class UserDetail extends Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('userDetail_model','model');
	}

	public function save(){
		$params = $this->input->post();
		$this->load->model('core/general_model');
		$result = $this->general_model->getResult();
		$picture = $_FILES['picture'];

		$dataDetailUser = $this->model->load(null,'LOADBY_IDUSER',array('id_user' => $params['id_user']))['data']['rows']->row_array();
		
		if($dataDetailUser){
			$dataDetailUser = array_merge($dataDetailUser,$params);
		}else{
			$dataDetailUser = $params;
		}

		// Upload
			if(!$picture['error']){
				$config['upload_path']          = './assets/user_upload/profile_picture/';
		        $config['allowed_types']        = 'gif|jpg|png';
		        $config['file_name']			= $params['id_user'].'-'.date('Y_m_d__H_i_s');
		        $this->load->library('upload', $config);

		        if ( ! $this->upload->do_upload('picture')){
		            $result['info'] = $this->upload->display_errors();
		            echo json_encode($result);
		            exit();
		        }else{
		            $uploadData = $this->upload->data();
		            $dataDetailUser['picture'] = $uploadData['file_name'];
		        }
			}


		$this->load->model('user_model');
		$dataDetailUser['id_user'] = $this->user_model->save($dataDetailUser)['data'];

		if($result['status']){
	        $result = $this->model->save($dataDetailUser);
		}else{
			$result['info'] = 'Failed saving user detail';
			$result['status'] = false;
		}
		echo json_encode($result);
	}
	public function delete(){
		$params = $this->input->post();
		$id = @$params['id'];
		$result = $this->model->delete($id);
		echo json_encode($result);
	}
	public function loadList(){
		$this->load->model('core/general_model');
		$result = $this->general_model->getResult();
		$post = $this->input->post();
		$data = $this->model->load(null,'LOAD_ALL',$post);
		
		if(isset($post['current'])){
			$result = array(
				'current' => $post['current'],
				'rowCount' => $post['rowCount'],
				'rows' => $data['data']['rows']->result(),
				'total' => $data['data']['totalRows']);
		}else{
			$result['data'] = $data['data']['rows']->result();
		}
		echo json_encode($result);
	}
	public function load(){
		$params = $this->input->post();
		$mode = (isset($params['mode']) && $params['mode'] != '') ? $params['mode'] : 'LOAD_ALL';
		$data = $this->model->load(null,$mode,$params);
		$data['data'] = $data['data']['rows']->row();
		echo json_encode($data);
	}

}	