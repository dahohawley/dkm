<table class="table table-hover table-stripped" id="manageUserGroup_user" style="width: 100%">
	<thead>
		<tr>
			<th data-column-id="id_user">User ID</th>
			<th data-column-id="full-name" data-formatter="full-name" >Full Name</th>
			<th data-column-id="action" data-formatter="action">Action</th>
		</tr>
	</thead>
</table>
<?php
	$mode = isset($mode) ? $mode : null;
?>
<script type="text/javascript">
	function initUserChooser(){
		// $("#manageUserGroup_user").bootgrid({
		// 	ajax : true,
		// 	url:"<?php echo site_url('user/user/loadDatatable') ?>",
		// 	post: function(){
		// 		return {
		// 			mode : "<?php echo $mode?>",
		// 			<?php
		// 				if(count(@$params) > 0){
		// 					foreach ($params as $key => $value) {
		// 						echo $key.':'.$value.',';
		// 					}
		// 				}
		// 			?>

		// 		}

		// 	},
		// 	formatters : {
		// 		"full-name" : function(column,row){
		// 			return row.first_name + ' ' + row.last_name;
		// 		},
		// 		"action" : function(column,row){
					// txt = '';
					// txt += '<button class="btn btn-success btn-xs chooseUser"';
					// txt += 'id_user="'+row.id_user+'"';
					// txt += 'first_name="'+row.first_name+'"';
					// txt += 'last_name="'+row.last_name+'"'; 
					// txt += '>';
					// txt += '	choose';
					// txt += '</button>';
					// return txt;
		// 		}
		// 	}
		// }).on('loaded.rs.jquery.bootgrid',function(e){
		// 	$(".chooseUser").click(function(){
				// data = {
				// 	"id_user" : $(this).attr('id_user'),
				// 	"first_name" : $(this).attr('first_name'),
				// 	"last_name" : $(this).attr('last_name')
				// };
				// <?php echo $callBackEvent?>(data);
		// 	});
		// });

		$("#manageUserGroup_user").DataTable({
			serverSide:true,
			processing:true,
			ajax: {
				url:"<?php echo site_url('user/user/loadDatatable') ?>",
				type:"POST",
			},
			columns : [
				{data:"id_user"},
				{
					data:"fullname",
					render:function(data,type,row){
						return row.first_name + ' '+row.last_name
					}
				},
				{
					data:"action",
					render:function(data,type,row,meta){
						txt = '';
						txt += '<button class="btn btn-success btn-xs chooseUser"';
						txt += 'id_user="'+row.id_user+'"';
						txt += 'first_name="'+row.first_name+'"';
						txt += 'last_name="'+row.last_name+'"';
						txt += '>';
						txt += '	choose';
						txt += '</button>';
						return txt;
					}
				},
			]
		});

		$("#manageUserGroup_user tbody").on('click','.chooseUser',function(){
			data = {
				"id_user" : $(this).attr('id_user'),
				"first_name" : $(this).attr('first_name'),
				"last_name" : $(this).attr('last_name')
			};
			<?php echo $callBackEvent?>(data);
		});
	}
</script>	