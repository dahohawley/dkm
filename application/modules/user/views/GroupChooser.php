<table class="table table-hover table-stripped" id="grid-groupChooser">
	<thead>
		<tr>
			<th data-column-id="user_group_id">User Group ID</th>
			<th data-column-id="user_group_name">User Group Name</th>
			<th data-column-id="action" data-formatter="action">Action</th>
		</tr>
	</thead>
</table>

<script type="text/javascript">
	function initGroupChooser(){
		$("#grid-groupChooser").bootgrid({
			ajax : true,
			url:"<?php echo site_url('user/UserGroup/loadList')?>",
			formatters:{
				"action" : function(column,row){
					txt = '';
					txt += '<button class="btn btn-success btn-xs chooseGroup"';
					txt += 'user_group_id="'+row.user_group_id+'"';
					txt += 'user_group_name="'+row.user_group_name+'"';
					txt += '>	';
					txt += '	Choose';
					txt += '</button>';
					return txt;
				}
			}
		}).on('loaded.rs.jquery.bootgrid',function(){
			$(".chooseGroup").click(function(){
				data = { 
					"user_group_name" : $(this).attr('user_group_name'),
					"user_group_id" : $(this).attr('user_group_id')
				}

				<?php
					if(isset($callBackEvent)){
						echo $callBackEvent.'(data);';
					}else{?>
						localStorage.setItem('user_group_name',$(this).attr('user_group_name'));
						localStorage.setItem('user_group_id',$(this).attr('user_group_id'));

				<?php
					}
				?>
			});
		});
	}
</script>