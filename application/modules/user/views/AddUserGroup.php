<?php
	$this->load->model('usergroup_model');
?>
<form action="#" method="POST" id="form-user-group" class="form-horizontal">
	<input type="hidden" name="<?php echo $this->usergroup_model->getKey()?>" id="<?php echo $this->usergroup_model->getKey()?>" >
	<?php

		foreach ($this->usergroup_model->_column as $data) {

			if($data['columnName'] != $this->usergroup_model->getKey()){
				// Kalo array input type nya (select)
					if(is_array($data['inputType'])){ ?>
						<div class="form-group">
							<label class="col-sm-3 control-label"><?php echo $data['alias']?></label>
							<div class="col-sm-9">
								<select name="<?php echo $data['columnName']?>" id="<?php echo $data['columnName']?>" class="form-control">
									<option value selected>
										Select 
										<?php
											if($data['alias'] != null){
												echo $data['alias'];
											}else{
												echo ucwords(str_replace('_',' ',$data['columnName']));
											}
										?>
									</option>
									<?php
										foreach ($data['inputType']['data'] as $selectData) {?>
											<option value="<?php echo $selectData->$data['inputType']['id']?>">
												<?php echo $selectData->$data['inputType']['name']?>
											</option>
									<?php
										}
									?>
								</select>
							</div>
						</div>
					<?php
				// kalo array input typenya (select)
				}else{?>
					<div class="form-group">
						<label class="col-sm-3 control-label">
							<?php
								if(!empty($data['alias'])){
									echo $data['alias'];
								}else{
									echo ucwords(str_replace('_',' ',$data['columnName']));
								}
							?>
						</label>
						<div class="col-sm-9">
							<input type="<?php echo $data['inputType']?>" class='form-control <?php echo $data['inputClass']?>' placeholder="Please Insert <?php echo ucwords(str_replace('_',' ',$data['columnName'])); ?>" name="<?php echo $data['columnName']?>" id="<?php echo $data['columnName']?>">
						</div>
					</div>
				<?php
				}


			}
		}

	?>
</form>

<script type="text/javascript">
	function saveUserGroup(){
		$.ajax({
			url:"<?php echo site_url('user/userGroup/save')?>",
			data : $("#form-user-group").serialize(),
			type:"POST",
			success:function(res){
				obj = JSON.parse(res);
				if(obj.status){
					custom_notification('success','Success');
				}else{
					custom_notification('success',obj.info);
				}
				$("#grid-user-group").bootgrid('reload');
				$(".modal").modal('hide');
			}
		});
	}
</script>