<form action="#" method="POST" id="form-user-type" class="form-horizontal">
	<input type="hidden" name="user_type_id" id="user_type_id">
	<div class="form-group">
		<label class="col-sm-3 control-label">User Type Name</label>
		<div class="col-sm-9">
			<input type="text" class='form-control' name="user_type_name" id="user_type_name">
		</div>
	</div>
</form>

<script type="text/javascript">
	function saveUserType(){
		$.ajax({
			url:"<?php echo site_url('user/userType/save')?>",
			data : $("#form-user-type").serialize(),
			type:"POST",
			success:function(res){
				obj = JSON.parse(res);
				if(obj.status){
					custom_notification('success','Success');
				}else{
					custom_notification('success',obj.info);
				}
				// $("#grid-user-type").bootgrid('reload');
				$("#grid-user-type").DataTable().ajax.reload();
				$(".modal").modal('hide');
			}
		});
	}
</script>