<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penerimaan_kotak_amal_model extends CI_Model {
	protected $_table = 'penerimaan_kotak_amal';
	protected $_key;

	const 	ID_TIPE_TRANSAKSI = 1;

	public function loadList($mode = 'LOAD_ALL',$params = array()){
		$recordsTotal 		= $this->db->count_all_results($this->_table);

		if(isset($params['search']) && $params['search']){
				$this->db->start_cache();

				$this->db->or_like('lower(id_jenis_kotak_amal)',strtolower($params['search']),'both');
				$this->db->or_like('lower(tahun_pelaksanaan)',strtolower($params['search']),'both');
				$this->db->or_like('lower(tanggal_pelaksanaan)',strtolower($params['search']),'both');
				$this->db->or_like('lower(create_dtm)',strtolower($params['search']),'both');
				$this->db->or_like('lower(total)',strtolower($params['search']),'both');

				$this->db->stop_cache();
		}
		
		if($mode != 'LOAD_ALL'){
			switch ($mode) {
				case 'GROUPBY_JENIS':
					$this->db->group_by('penerimaan_kotak_amal.id_jenis_kotak_amal');
					$this->db->select('jenis_kotak_amal.nama_jenis,SUM(total) as total');
					break;
			}
		}
		
		$this->db->join('jenis_kotak_amal','penerimaan_kotak_amal.id_jenis_kotak_amal = jenis_kotak_amal.id_jenis_kotak_amal');
		
		if(isset($params['pagination'])){
			$limit 			= $params['pagination']['limit'];
			$offset 		= $params['pagination']['offset'];
		}else{
			$limit 			= 0;
			$offset 		= 0;
		}

		if(isset($params['order'])){
			$this->db->order_by($this->_table.'.'.$params['order']['column'],$params['order']['dir']);
		}

		$data 						= $this->db->get($this->_table,$limit,$offset);
		$recordsFiltered 			= $this->db->count_all_results($this->_table);

		$result 					= new stdClass;
		$result->data 				= $data->result();
		$result->recordsTotal 		= $recordsTotal;
		$result->recordsFiltered 	= $recordsFiltered;

		return $result;
	}
	public function save( $params = array() ){
		$CI =& get_instance();
		$CI->load->model('core/general_model');

		if(isset($params['id_penerimaan_kotak_amal']) && !empty($params['id_penerimaan_kotak_amal'])){
			$id_penerimaan_kotak_amal 				= $params['id_penerimaan_kotak_amal'];

			$this->db->where('id_penerimaan_kotak_amal',$id_penerimaan_kotak_amal);
			$this->db->set($params);
			$this->db->update($this->_table);

			$resultData = array(
				'id_penerimaan_kotak_amal' => $id_penerimaan_kotak_amal,
			);
			$result = $CI->general_model->result(200,'Berhasil Merubah data.',$resultData);
		}else{
			unset($params['id_penerimaan_kotak_amal']);
			$this->db->insert($this->_table,$params);

			$insertID 			= $this->db->insert_id(); 

			$resultData = array(
				'insert_id' => $insertID,
			);

			$dataJurnal 			= array();
			$this->load->model('accounting/jurnal_model');
			$dataJurnal['transaksi'] 	= array(
				'id_tipe' 			=> self::ID_TIPE_TRANSAKSI,
				'id_transaksi'		=> $insertID,
				'tanggal_transaksi' => date('Y-m-d', strtotime($params['tanggal_pelaksanaan'])),
				'jumlah_transaksi' 	=> format_angka($params['total']),
			);
			$dataJurnal['jurnal'][] = array(
				'kode_akun' 		=> '111',
				'posisi_dr_cr' 		=> 'D',
				'nominal' 			=> format_angka($params['total']),
			);
			$dataJurnal['jurnal'][] = array(
				'kode_akun' 		=> '101',
				'posisi_dr_cr' 		=> 'K',
				'nominal' 			=> format_angka($params['total']),
			);
			$this->jurnal_model->createJurnal($dataJurnal);



			$result = $CI->general_model->result(200,'Berhasil Menambah Data',$resultData);
		}
		return $result;
	}
	public function delete( $params = array() ){
		$this->db->where('id_penerimaan_kotak_amal',$params['id_penerimaan_kotak_amal']);
		$this->db->delete($this->_table);

		$CI =& get_instance();
		$CI->load->model('general_model');
		$result = $this->general_model->result(200,'Sukses Menghapus Data #'.$params['id_penerimaan_kotak_amal']);
		return $result;
	}
	protected function getNewCode(){
		$q 						= $this->db->query("SELECT max(id_penerimaan_kotak_amal) as lastCode from $this->_table")->row();
		$lastCode 				= $q->lastCode;
		$nextValue	 			= explode('-', $lastCode)[1]+1;
		$nextValue				= str_pad($nextValue, 6,'0',STR_PAD_LEFT);
		$prefix 				= 'PEN-';
		$newCode 				= $prefix.$nextValue;
		return $newCode;
	}
}
