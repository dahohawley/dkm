<div class="row" id="row-summary">
</div>

<?php echo $this->template->cardOpen('Penerimaan Kotak Amal');?>

<?php echo $this->template->cardBodyOpen();?>
	<div class="btn-group" id="action_button">
		<a href="<?php echo site_url('penerimaan_kotak_amal/createNew') ?>" class="btn btn-primary"><span class="fa fa-plus"></span> Tambah</a>
	</div>

	<table class="table table-hover table-bordered" id="table-penerimaan_kotak_amal" style="width: 100%">
		<thead>
			<tr>
				<!-- <th class="text-center">Id Penerimaan Kotak Amal</th> -->
				<th class="text-center">Jenis Kotak Amal</th>
				<th class="text-center">Tahun Pelaksanaan</th>
				<th class="text-center">Tanggal Pelaksanaan</th>
				<!-- <th class="text-center">Create Dtm</th> -->
				<th class="text-center">Total</th>
				<!-- <th class="text-center">Aksi</th>/ -->
			</tr>
		</thead>
	</table>
<?php echo $this->template->cardBodyClose();?>
<script type="text/javascript">
	let getSummary = () => {
		return new Promise((resolve,reject)=>{
			$.ajax({
				url 			: "<?php echo site_url('penerimaan_kotak_amal/loadList')?>",
				type 			: "POST",
				data 			: {
					mode : "GROUPBY_JENIS"
				},
				success			: function(res){
					resolve(res);
				},	
			});
		});
	}
	$(document).ready(function(){
		getSummary().then((res)=>{
			res = JSON.parse(res);

			$.each(res.data.data,(col,row)=>{
				txt = '';
				txt += '<div class="col-xl-3 col-md-6 mb-4">';
				txt += '    <div class="card border-left-success shadow h-100 py-2">';
				txt += '        <div class="card-body">';
				txt += '            <div class="row no-gutters align-items-center">';
				txt += '                <div class="col mr-2">';
				txt += '                    <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Kotak Amal '+row.nama_jenis+'</div>';
				txt += '                    <div class="h5 mb-0 font-weight-bold text-gray-800">'+format_rp(row.total)+'</div>';
				txt += '                </div>';
				txt += '                <div class="col-auto">';
				txt += '                    <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>';
				txt += '                </div>';
				txt += '            </div>';
				txt += '        </div>';
				txt += '    </div>';
				txt += '</div>';

				$("#row-summary").append(txt);
			});
		}).catch();
		$("#table-penerimaan_kotak_amal").DataTable({
			processing:true,
			serverSide:true,
			ajax : {
				url:"<?php echo site_url('penerimaan_kotak_amal/loadList') ?>",
				type:"POST"
			},
			columns :[
				{
					data:"id_jenis_kotak_amal",
					render:function(data,type,row){
						return row.nama_jenis
					}
				},
				{
					data : "tahun_pelaksanaan",
					render : function(data){
						return data + " Hijriyah";
					},
					className : "text-center"
				},
				{
					data:"tanggal_pelaksanaan",
					render: function(data,type,row,meta){
						return moment(data).format('DD MMMM YYYY');
					},
					className : "text-center"
				},
				{
					data:"total",
					render: function(data,type,row,meta){
						return format_rp(data);
					},
					className : "text-right"
				},
			],
		});

		$(document).on('click','.edit',function(){
			id_penerimaan_kotak_amal 			= $(this).attr('id_penerimaan_kotak_amal');
			id_jenis_kotak_amal 			= $(this).attr('id_jenis_kotak_amal');
			tahun_pelaksanaan 			= $(this).attr('tahun_pelaksanaan');
			tanggal_pelaksanaan 			= $(this).attr('tanggal_pelaksanaan');
			create_dtm 			= $(this).attr('create_dtm');
			total 			= $(this).attr('total');
			$("#id_penerimaan_kotak_amal").val(id_penerimaan_kotak_amal);
			$("#id_jenis_kotak_amal").val(id_jenis_kotak_amal);
			$("#tahun_pelaksanaan").val(tahun_pelaksanaan);
			$("#tanggal_pelaksanaan").val(tanggal_pelaksanaan);
			$("#create_dtm").val(create_dtm);
			$("#total").val(total);
			$("#modal-addTitle").text('Ubah data #'+id_penerimaan_kotak_amal);
			$("#modal-add").modal('show');
		});

		$(document).on('click','.delete',function(){
			id_penerimaan_kotak_amal 			= $(this).attr('id_penerimaan_kotak_amal');
			id_jenis_kotak_amal 			= $(this).attr('id_jenis_kotak_amal');
			tahun_pelaksanaan 			= $(this).attr('tahun_pelaksanaan');
			tanggal_pelaksanaan 			= $(this).attr('tanggal_pelaksanaan');
			create_dtm 			= $(this).attr('create_dtm');
			total 			= $(this).attr('total');

			conf = confirm("Apakah anda yakin ingin menghapus data #"+id_penerimaan_kotak_amal+" ?");
			if(conf){
				$.ajax({
					url:"<?php echo site_url('penerimaan_kotak_amal/deleteData') ?>",
					type :"POST",
					data : {
						id_penerimaan_kotak_amal : id_penerimaan_kotak_amal
					},
					success:function(res){
						res = JSON.parse(res);
						if(res.code == 200){
							custom_notification('success',res.info);
							refreshTable();
						}else{
							custom_notification('danger',res.info);
						}
					}
				});
			}
		});

 		$("#table-penerimaan_kotak_amal_filter").append($("#action_button"));

		$("#formAdd").validate({
			rules : {
				id_penerimaan_kotak_amal: {
					required : true,
				},
				id_jenis_kotak_amal: {
					required:true,
				},
				tahun_pelaksanaan: {
					required:true,
				},
				tanggal_pelaksanaan: {
					required:true,
				},
				total: {
					required:true,
				},
			},
			submitHandler : function(){
				$.ajax({
					url:"<?php echo site_url('penerimaan_kotak_amal/save') ?>",
					type : "POST",
					data : $("#formAdd").serialize(),
					beforeSend:function(){
						buttonSpinner('btnAddSave');
					},
					success:function(res){
						res = JSON.parse(res);
						if(res.code == 200){
							custom_notification('success',res.info);
							refreshTable();
						}else{
							custom_notification('danger',res.info);
						}
					},
					complete:function(){
						$(".modal").modal('hide');
						removeButtonSpinner('btnAddSave');
					}
				});
			}
		});
	});
	function refreshTable(){
		$("#table-penerimaan_kotak_amal").DataTable().ajax.reload();
	}
	function addData(){
		$("#modal-addTitle").text('Tambah data baru');
		$("#id_penerimaan_kotak_amal").val(0);
		$("#formAdd")[0].reset();
		$("#modal-add").modal('show');
	}
</script>

<div id="modal-add" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal-addTitle">Tambah Crud name</h4>
            </div>
            <div class="modal-body">
            	<form method="POST" action="#" class="form-horizontal" id="formAdd">
 					<input type="hidden" name="id_penerimaan_kotak_amal" id="id_penerimaan_kotak_amal" >
				<div class="form-group">
					<label class="control-label col-md-3 text-right">Id Jenis Kotak Amal</label>
					<div class="col-md-9">
						<input type="text" class='form-control rupiah' name="id_jenis_kotak_amal" id="id_jenis_kotak_amal">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3 text-right">Tahun Pelaksanaan</label>
					<div class="col-md-9">
						<input type="text" class='form-control' name="tahun_pelaksanaan" id="tahun_pelaksanaan">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3 text-right">Tanggal Pelaksanaan</label>
					<div class="col-md-9">
						<input type="date" class='form-control rupiah' name="tanggal_pelaksanaan" id="tanggal_pelaksanaan">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3 text-right">Create Dtm</label>
					<div class="col-md-9">
						<input type="date" class='form-control rupiah' name="create_dtm" id="create_dtm">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3 text-right">Total</label>
					<div class="col-md-9">
						<input type="text" class='form-control rupiah' name="total" id="total">
					</div>
				</div>
            	</form>
            </div>
            <div class="modal-footer">
            	<button class="btn btn-primary" onclick='$("#formAdd").submit()' id="btnAddSave">
            		<span class="fa fa-check"></span> Simpan
            	</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
