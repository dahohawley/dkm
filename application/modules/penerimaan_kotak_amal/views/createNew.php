<script src="https://cdnjs.cloudflare.com/ajax/libs/air-datepicker/2.2.3/js/datepicker.min.js" integrity="sha256-0prQxFtdWQNa3vW5JDMbvbBp6ehKzf9UIWqFGZIWPPE=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/air-datepicker/2.2.3/js/i18n/datepicker.en.min.js" integrity="sha256-RbE9ux8UbaKexk/+aT9XkZ8tcUWg3yw/3XE/S8iW7eI=" crossorigin="anonymous"></script>

<div class="card">
	<div class="card-header">
		Penerimaan Kotak Amal
	</div>
	<div class="card-body">
		<form action="#" id="form-penerimaan-kotak-amal">
			<div class="form-group row">
				<label class="control-label col-3">Jenis Kotak Amal</label>
				<div class="col-9">
					<select name="id_jenis_kotak_amal" class="form-control" id="id_jenis_kotak_amal">
						<option value disabled selected>-- Pilih Jenis Kotak Amal -- </option>
						<?php foreach ($jenisKotakAmal->data as $data): ?>
							<option value="<?php echo $data->id_jenis_kotak_amal ?>"><?php echo $data->nama_jenis ?></option>
						<?php endforeach ?>
					</select>
				</div>
			</div>

			<div class="form-group row">
				<label class="control-label col-3">Tahun Penerimaan</label>
				<div class="col-9">
					<div class="input-group mb-3">
					    <input type="text" class="form-control" placeholder="Tahun Penerimaan (Hijriyah)" name="tahun_pelaksanaan" id="tahun_pelaksanaan">
					    <div class="input-group-prepend">
					        <span class="input-group-text" id="basic-addon1">Hijriyah</span>
					    </div>
					</div>
				</div>
			</div>

			<div class="form-group row">
				<label class="control-label col-3">Tanggal Pelaksanaan</label>
				<div class="col-9">
					<input type="date" class='form-control'  name="tanggal_pelaksanaan" id="tanggal_pelaksanaan">
				</div>
			</div>

			<div class="form-group row">
				<label class="control-label col-3">Jumlah Penerimaan</label>
				<div class="col-9">
					<input type="text" class='form-control rupiah' name="total" id="total">
				</div>
			</div>
			
			<div class="float-right">
				<a href="<?php echo site_url('penerimaan_kotak_amal') ?>" class="btn btn-secondary">Back</a>
				<button class="btn btn-primary" id="btn-save">
					Simpan
				</button>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">
	$.validator.addMethod(
		'moneyGreaterThanZero',
		function (value, element) {
			value = format_angka(value);
			if(value <= 0){
				return false;
			}else{
				return true;
			}
		},
		'Please insert correct values'
	);

	$("#form-penerimaan-kotak-amal").validate({
		rules : {
			id_jenis_kotak_amal : "required",
			tahun_pelaksanaan : {
				required : true,
				number : true,
			},
			tanggal_pelaksanaan : "required",
			total : {
				required : true,
				moneyGreaterThanZero : true,
			}
		},
		errorClass : "text-danger",
		errorElement : 'div',
		submitHandler : function(){
			$.ajax({
				url 			: "<?php echo site_url('penerimaan_kotak_amal/save') ?>",
				type 			: "POST",
				data 			: $("#form-penerimaan-kotak-amal").serialize(),
				beforeSend 		: function(){
					$("#btn-save").attr('disabled','true');
				},
				success			: function(res){
					// window.location.replace("<?php echo site_url('penerimaan_kotak_amal') ?>");
				},	
				complete 		: function(){
					$("#btn-save").removeAttr('disabled','true');
				},
				error 			: function(){
					$.toast({
						title 		: "Error",
						content 	: "Test",
						type 		: "danger",
						delay 		: 5000
					});
				}
			});
		}	
	});
</script>