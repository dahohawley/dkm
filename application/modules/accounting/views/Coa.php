<?php echo $this->template->cardOpen('Coa');?>

<?php echo $this->template->cardBodyOpen();?>
	<div class="btn-group" id="action_button">
		<button class="btn btn-primary" id="buttonAdd" onclick="addData()">
			<span class="fa fa-plus"></span> Tambah
		</button>
	</div>

	<table class="table table-hover table-bordered" id="table-coa" style="width: 100%">
		<thead>
			<tr>
				<th class="text-center">Id Coa</th>
				<th class="text-center">Kode Akun</th>
				<th class="text-center">Nama Akun</th>
				<th class="text-center">Aksi</th>
			</tr>
		</thead>
	</table>
<?php echo $this->template->cardBodyClose();?>
<script type="text/javascript">
	$(document).ready(function(){
		$("#table-coa").DataTable({
			processing:true,
			serverSide:true,
			ajax : {
				url:"<?php echo site_url('accounting/coa/loadList') ?>",
				type:"POST"
			},
			columns :[
				{
					data:"id_coa",
					visible : false,
				},
				{
					data : "kode_akun"
				},
				{
					data:"nama_akun"
				},
				{
					data:"aksi",
					render:function(data,type,row){
						txt = '';
						txt += '<button class="btn btn-primary btn-xs edit"';
						txt += '	id_coa = "'+row.id_coa+'";'
						txt += '	kode_akun = "'+row.kode_akun+'"';
						txt += '	nama_akun = "'+row.nama_akun+'"';
						txt += '>';
						txt += '	<span class="fa fa-pencil-alt"></span> Ubah';
						txt += '</button> ';

						txt += '<button class="btn btn-danger btn-xs delete"';
						txt += '	id_coa = "'+row.id_coa+'";'
						txt += '	kode_akun = "'+row.kode_akun+'"';
						txt += '	nama_akun = "'+row.nama_akun+'"';
						txt += '>';
						txt += '	<span class="fa fa-trash"></span> Hapus';
						txt += '</button>';
						return txt;
					},
					className : "text-center",
					visible : false,
				},
			],
		});

		$(document).on('click','.edit',function(){
			id_coa 			= $(this).attr('id_coa');
			kode_akun 			= $(this).attr('kode_akun');
			nama_akun 			= $(this).attr('nama_akun');
			$("#id_coa").val(id_coa);
			$("#kode_akun").val(kode_akun);
			$("#nama_akun").val(nama_akun);
			$("#modal-addTitle").text('Ubah data #'+id_coa);
			$("#modal-add").modal('show');
		});

		$(document).on('click','.delete',function(){
			id_coa 			= $(this).attr('id_coa');
			kode_akun 			= $(this).attr('kode_akun');
			nama_akun 			= $(this).attr('nama_akun');

			conf = confirm("Apakah anda yakin ingin menghapus data #"+id_coa+" ?");
			if(conf){
				$.ajax({
					url:"<?php echo site_url('accounting/coa/deleteData') ?>",
					type :"POST",
					data : {
						id_coa : id_coa
					},
					success:function(res){
						res = JSON.parse(res);
						if(res.code == 200){
							custom_notification('success',res.info);
							refreshTable();
						}else{
							custom_notification('danger',res.info);
						}
					}
				});
			}
		});

 		$("#table-coa_filter").append($("#action_button"));

		$("#formAdd").validate({
			rules : {
				id_coa: {
					required : true,
				},
				kode_akun: {
					required:true,
					number : true,
				},
				nama_akun: {
					required:true,
				},
			},
			submitHandler : function(){
				$.ajax({
					url:"<?php echo site_url('accounting/coa/save') ?>",
					type : "POST",
					data : $("#formAdd").serialize(),
					beforeSend:function(){
						buttonSpinner('btnAddSave');
					},
					success:function(res){
						res = JSON.parse(res);
						if(res.code == 200){
							custom_notification('success',res.info);
							refreshTable();
						}else{
							custom_notification('danger',res.info);
						}
					},
					complete:function(){
						$(".modal").modal('hide');
						removeButtonSpinner('btnAddSave');
					}
				});
			}
		});
	});
	function refreshTable(){
		$("#table-coa").DataTable().ajax.reload();
	}
	function addData(){
		$("#modal-addTitle").text('Tambah data baru');
		$("#id_coa").val(0);
		$("#formAdd")[0].reset();
		$("#modal-add").modal('show');
	}
</script>

<div id="modal-add" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal-addTitle">Tambah Crud name</h4>
            </div>
            <div class="modal-body">
            	<form method="POST" action="#" class="form-horizontal" id="formAdd">
 					<input type="hidden" name="id_coa" id="id_coa" >
				<div class="form-group row">
					<label class="control-label col-md-3">Kode Akun</label>
					<div class="col-md-9">
						<input type="text" class='form-control' name="kode_akun" id="kode_akun">
					</div>
				</div>
				<div class="form-group row">
					<label class="control-label col-md-3">Nama Akun</label>
					<div class="col-md-9">
						<input type="text" class='form-control' name="nama_akun" id="nama_akun">
					</div>
				</div>
            	</form>
            </div>
            <div class="modal-footer">
            	<button class="btn btn-primary" onclick='$("#formAdd").submit()' id="btnAddSave">
            		<span class="fa fa-check"></span> Simpan
            	</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
