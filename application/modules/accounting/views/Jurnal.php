<?php echo $this->template->cardOpen('Jurnal'); ?>

<?php echo $this->template->cardBodyOpen(); ?>
<table class="table table-hover table-bordered" id="table-jurnal" style="width: 100%">
	<thead>
		<tr>
			<th colspan="4"></th>
			<th colspan="2" class="text-center">Saldo</th>
		</tr>
		<tr>
			<th class="text-center">Tanggal Jurnal</th>
			<th class="text-center">Kode Transaksi</th>
			<th class="text-center">Nama Akun</th>
			<th class="text-center">Ref</th>
			<th class="text-center">Debit</th>
			<th class="text-center">Kredit</th>
		</tr>
	</thead>
</table>
<?php echo $this->template->cardBodyClose(); ?>
<script type="text/javascript">
	$(document).ready(function() {
		$("#table-jurnal").DataTable({
			processing: true,
			serverSide: true,
			ajax: {
				url: "<?php echo site_url('accounting/jurnal/loadList') ?>",
				type: "POST"
			},
			columns: [{
					data: "tanggal_jurnal",
					render: function(data, type, row, meta) {
						return moment(data).format('DD MMMM YYYY');
					},
					orderable: false,
				},
				{
					data: "kode_transaksi",
					orderable: false,

				},
				{
					data: "nama_akun",
					render: function(data, type, row) {
						txt = '';
						if (row.posisi_dr_cr == 'K') {
							txt += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
						}
						txt += data;

						return txt;
					},
					orderable: false,
				},
				{
					data: null,
					render: function() {
						return 'JU';
					},
					orderable: false,
				},
				{
					data: "debit",
					render: function(data, type, row) {
						if (row.posisi_dr_cr == 'D') {
							return format_rp(row.nominal);
						} else {
							return '-';
						}
					},
					className: "text-right",
					orderable: false,
				},
				{
					data: "kredit",
					render: function(data, type, row) {
						if (row.posisi_dr_cr == 'K') {
							return format_rp(row.nominal);
						} else {
							return '-';
						}
					},
					className: "text-right",
					orderable: false,
				},
				
			],
		});
	});

	function refreshTable() {
		$("#table-jurnal").DataTable().ajax.reload();
	}
</script>