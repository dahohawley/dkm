<?php echo $this->template->cardOpen('Tipe Transaksi');?>

<?php echo $this->template->cardBodyOpen();?>
	<div class="btn-group" id="action_button">
		<button class="btn btn-primary" id="buttonAdd" onclick="addData()">
			<span class="fa fa-plus"></span> Tambah
		</button>
	</div>

	<table class="table table-hover table-bordered" id="table-tipe_transaksi" style="width: 100%">
		<thead>
			<tr>
				<th class="text-center">Id Tipe</th>
				<th class="text-center">Nama Tipe</th>
				<th class="text-center">Aksi</th>
			</tr>
		</thead>
	</table>
<?php echo $this->template->cardBodyClose();?>
<script type="text/javascript">
	$(document).ready(function(){
		$("#table-tipe_transaksi").DataTable({
			processing:true,
			serverSide:true,
			ajax : {
				url:"<?php echo site_url('accounting/tipe_transaksi/loadList') ?>",
				type:"POST"
			},
			columns :[
				{data:"id_tipe"},
				{data:"nama_tipe"},
			{
				data:"aksi",
				render:function(data,type,row){
					txt = '';
					txt += '<button class="btn btn-primary btn-xs edit"';
					txt += '	id_tipe = "'+row.id_tipe+'";'
					txt += '	nama_tipe = "'+row.nama_tipe+'"';
					txt += '>';
					txt += '	<span class="fa fa-pencil-alt"></span> Ubah';
					txt += '</button> ';

					txt += '<button class="btn btn-danger btn-xs delete"';
					txt += '	id_tipe = "'+row.id_tipe+'";'
					txt += '	nama_tipe = "'+row.nama_tipe+'"';
					txt += '>';
					txt += '	<span class="fa fa-trash"></span> Hapus';
					txt += '</button>';
					return txt;
				}
			},
			],
		});

		$(document).on('click','.edit',function(){
			id_tipe 			= $(this).attr('id_tipe');
			nama_tipe 			= $(this).attr('nama_tipe');
			$("#id_tipe").val(id_tipe);
			$("#nama_tipe").val(nama_tipe);
			$("#modal-addTitle").text('Ubah data #'+id_tipe);
			$("#modal-add").modal('show');
		});

		$(document).on('click','.delete',function(){
			id_tipe 			= $(this).attr('id_tipe');
			nama_tipe 			= $(this).attr('nama_tipe');

			conf = confirm("Apakah anda yakin ingin menghapus data #"+id_tipe+" ?");
			if(conf){
				$.ajax({
					url:"<?php echo site_url('accounting/tipe_transaksi/deleteData') ?>",
					type :"POST",
					data : {
						id_tipe : id_tipe
					},
					success:function(res){
						res = JSON.parse(res);
						if(res.code == 200){
							custom_notification('success',res.info);
							refreshTable();
						}else{
							custom_notification('danger',res.info);
						}
					}
				});
			}
		});

 		$("#table-tipe_transaksi_filter").append($("#action_button"));

		$("#formAdd").validate({
			rules : {
				id_tipe: {
					required : true,
				},
				nama_tipe: {
					required:true,
				},
			},
			submitHandler : function(){
				$.ajax({
					url:"<?php echo site_url('accounting/tipe_transaksi/save') ?>",
					type : "POST",
					data : $("#formAdd").serialize(),
					beforeSend:function(){
						buttonSpinner('btnAddSave');
					},
					success:function(res){
						res = JSON.parse(res);
						if(res.code == 200){
							custom_notification('success',res.info);
							refreshTable();
						}else{
							custom_notification('danger',res.info);
						}
					},
					complete:function(){
						$(".modal").modal('hide');
						removeButtonSpinner('btnAddSave');
					}
				});
			}
		});
	});
	function refreshTable(){
		$("#table-tipe_transaksi").DataTable().ajax.reload();
	}
	function addData(){
		$("#modal-addTitle").text('Tambah data baru');
		$("#id_tipe").val(0);
		$("#formAdd")[0].reset();
		$("#modal-add").modal('show');
	}
</script>

<div id="modal-add" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal-addTitle">Tambah Crud name</h4>
            </div>
            <div class="modal-body">
            	<form method="POST" action="#" class="form-horizontal" id="formAdd">
 					<input type="hidden" name="id_tipe" id="id_tipe" >
				<div class="form-group row">
					<label class="control-label col-md-3">Nama Tipe</label>
					<div class="col-md-9">
						<input type="text" class='form-control' name="nama_tipe" id="nama_tipe">
					</div>
				</div>
            	</form>
            </div>
            <div class="modal-footer">
            	<button class="btn btn-primary" onclick='$("#formAdd").submit()' id="btnAddSave">
            		<span class="fa fa-check"></span> Simpan
            	</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
