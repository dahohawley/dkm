<?php echo $this->template->cardOpen('Transaksi'); ?>

<?php echo $this->template->cardBodyOpen(); ?>
<div class="btn-group" id="action_button">
	<button class="btn btn-primary" id="buttonAdd" onclick="addData()">
		<span class="fa fa-plus"></span> Tambah
	</button>
</div>

<table class="table table-hover table-bordered" id="table-transaksi" style="width: 100%">
	<thead>
		<tr>
			<th class="text-center">Kode Transaksi</th>
			<th class="text-center">Tipe Transaksi</th>
			<th class="text-center">ID Transaksi</th>
			<th class="text-center">Tanggal Transaksi</th>
			<th class="text-center">Jumlah Transaksi</th>
			<th class="text-center">Tanggal Dibuat</th>
			<th class="text-center">Aksi</th>
		</tr>
	</thead>
</table>
<?php echo $this->template->cardBodyClose(); ?>
<script type="text/javascript">
	$(document).ready(function() {
		$("#table-transaksi").DataTable({
			processing: true,
			serverSide: true,
			ajax: {
				url: "<?php echo site_url('accounting/transaksi/loadList') ?>",
				type: "POST"
			},
			columns: [{
					data: "kode_transaksi"
				},
				{
					data: "nama_tipe"
				},
				{
					data: "id_transaksi"
				},
				{
					data: "tanggal_transaksi",
					render: function(data, type, row, meta) {
						return moment(data).format('DD MMMM YYYY');
					}
				},
				{
					data: "jumlah_transaksi",
					render: function(data, type, row, meta) {
						return format_rp(data);
					}
				},
				{
					data: "tanggal_dibuat",
					render: function(data, type, row, meta) {
						return moment(data).format('DD MMMM YYYY');
					}
				},
				{
					data: "aksi",
					render: function(data, type, row) {
						txt = '';
						txt += '<button class="btn btn-primary btn-xs edit"';
						txt += '	kode_transaksi = "' + row.kode_transaksi + '";'
						txt += '	id_tipe = "' + row.id_tipe + '"';
						txt += '	tanggal_transaksi = "' + row.tanggal_transaksi + '"';
						txt += '	jumlah_transaksi = "' + row.jumlah_transaksi + '"';
						txt += '	tanggal_dibuat = "' + row.tanggal_dibuat + '"';
						txt += '>';
						txt += '	<span class="fa fa-pencil-alt"></span> Ubah';
						txt += '</button> ';

						txt += '<button class="btn btn-danger btn-xs delete"';
						txt += '	kode_transaksi = "' + row.kode_transaksi + '";'
						txt += '	id_tipe = "' + row.id_tipe + '"';
						txt += '	tanggal_transaksi = "' + row.tanggal_transaksi + '"';
						txt += '	jumlah_transaksi = "' + row.jumlah_transaksi + '"';
						txt += '	tanggal_dibuat = "' + row.tanggal_dibuat + '"';
						txt += '>';
						txt += '	<span class="fa fa-trash"></span> Hapus';
						txt += '</button>';
						return txt;
					}
				},
			],
		});

		$(document).on('click', '.edit', function() {
			kode_transaksi = $(this).attr('kode_transaksi');
			id_tipe = $(this).attr('id_tipe');
			tanggal_transaksi = $(this).attr('tanggal_transaksi');
			jumlah_transaksi = $(this).attr('jumlah_transaksi');
			tanggal_dibuat = $(this).attr('tanggal_dibuat');
			$("#kode_transaksi").val(kode_transaksi);
			$("#id_tipe").val(id_tipe);
			$("#tanggal_transaksi").val(tanggal_transaksi);
			$("#jumlah_transaksi").val(jumlah_transaksi);
			$("#tanggal_dibuat").val(tanggal_dibuat);
			$("#modal-addTitle").text('Ubah data #' + kode_transaksi);
			$("#modal-add").modal('show');
		});

		$(document).on('click', '.delete', function() {
			kode_transaksi = $(this).attr('kode_transaksi');
			id_tipe = $(this).attr('id_tipe');
			tanggal_transaksi = $(this).attr('tanggal_transaksi');
			jumlah_transaksi = $(this).attr('jumlah_transaksi');
			tanggal_dibuat = $(this).attr('tanggal_dibuat');

			conf = confirm("Apakah anda yakin ingin menghapus data #" + kode_transaksi + " ?");
			if (conf) {
				$.ajax({
					url: "<?php echo site_url('accounting/transaksi/deleteData') ?>",
					type: "POST",
					data: {
						kode_transaksi: kode_transaksi
					},
					success: function(res) {
						res = JSON.parse(res);
						if (res.code == 200) {
							custom_notification('success', res.info);
							refreshTable();
						} else {
							custom_notification('danger', res.info);
						}
					}
				});
			}
		});

		$("#table-transaksi_filter").append($("#action_button"));

		$("#formAdd").validate({
			rules: {
				kode_transaksi: {
					required: true,
				},
				id_tipe: {
					required: true,
				},
				tanggal_transaksi: {
					required: true,
				},
				jumlah_transaksi: {
					required: true,
				},
				tanggal_dibuat: {
					required: true,
				},
			},
			submitHandler: function() {
				$.ajax({
					url: "<?php echo site_url('accounting/transaksi/save') ?>",
					type: "POST",
					data: $("#formAdd").serialize(),
					beforeSend: function() {
						buttonSpinner('btnAddSave');
					},
					success: function(res) {
						res = JSON.parse(res);
						if (res.code == 200) {
							custom_notification('success', res.info);
							refreshTable();
						} else {
							custom_notification('danger', res.info);
						}
					},
					complete: function() {
						$(".modal").modal('hide');
						removeButtonSpinner('btnAddSave');
					}
				});
			}
		});
	});

	function refreshTable() {
		$("#table-transaksi").DataTable().ajax.reload();
	}

	function addData() {
		$("#modal-addTitle").text('Tambah data baru');
		$("#kode_transaksi").val(0);
		$("#formAdd")[0].reset();
		$("#modal-add").modal('show');
	}
</script>

<div id="modal-add" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="modal-addTitle">Tambah Crud name</h4>
			</div>
			<div class="modal-body">
				<form method="POST" action="#" class="form-horizontal" id="formAdd">
					<input type="hidden" name="kode_transaksi" id="kode_transaksi">
					<div class="form-group row">
						<label class="control-label col-md-3">Id Tipe</label>
						<div class="col-md-9">
							<select name="id_tipe" class="form-control">
								<?php
								foreach ($dataType->data as $type) { ?>
									<option value="<?php echo $type->id_tipe ?>"><?php echo $type->nama_tipe ?></option>
								<?php
								}
								?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-3">Tanggal Transaksi</label>
						<div class="col-md-9">
							<input type="date" class='form-control rupiah' name="tanggal_transaksi" id="tanggal_transaksi">
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-3">Jumlah Transaksi</label>
						<div class="col-md-9">
							<input type="text" class='form-control rupiah' name="jumlah_transaksi" id="jumlah_transaksi">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" onclick='$("#formAdd").submit()' id="btnAddSave">
					<span class="fa fa-check"></span> Simpan
				</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>