<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi_model extends CI_Model {
	protected $_table = 'transaksi';
	protected $_key;

	public function loadList($mode = 'LOAD_ALL',$params = array()){
		$recordsTotal 		= $this->db->count_all_results($this->_table);

		$this->db->join(
			'tipe_transaksi',
			'tipe_transaksi.id_tipe = transaksi.id_tipe'
		);

		if(isset($params['search']) && $params['search']){
				$this->db->start_cache();

				$this->db->or_like('lower(id_tipe)',strtolower($params['search']),'both');
				$this->db->or_like('lower(tanggal_transaksi)',strtolower($params['search']),'both');
				$this->db->or_like('lower(jumlah_transaksi)',strtolower($params['search']),'both');
				$this->db->or_like('lower(tanggal_dibuat)',strtolower($params['search']),'both');

				$this->db->stop_cache();
		}

		if(isset($params['pagination'])){
			$limit 			= $params['pagination']['limit'];
			$offset 		= $params['pagination']['offset'];
		}else{
			$limit 			= 0;
			$offset 		= 0;
		}

		if(isset($params['order'])){
			$this->db->order_by($params['order']['column'],$params['order']['dir']);
		}

		$data 						= $this->db->get($this->_table,$limit,$offset);
		$recordsFiltered 			= $this->db->count_all_results($this->_table);

		$result 					= new stdClass;
		$result->data 				= $data->result();
		$result->recordsTotal 		= $recordsTotal;
		$result->recordsFiltered 	= $recordsFiltered;

		return $result;
	}
	public function save( $params = array() ){
		$CI =& get_instance();
		$CI->load->model('core/general_model');

		if(isset($params['kode_transaksi']) && !empty($params['kode_transaksi'])){
			$kode_transaksi 				= $params['kode_transaksi'];

			$this->db->where('kode_transaksi',$kode_transaksi);
			$this->db->set($params);
			$this->db->update($this->_table);

			$resultData = array(
				'kode_transaksi' => $kode_transaksi,
			);
			$result = $CI->general_model->result(200,'Berhasil Merubah data.',$resultData);
		}else{
			unset($params['kode_transaksi']);
			$params['kode_transaksi'] = $this->getNewCode();
			$params['tanggal_dibuat'] = date('Y-m-d');

			$this->db->insert($this->_table,$params);
			$resultData = array(
				'insert_id' => $params['kode_transaksi'],
			);
			$result = $CI->general_model->result(200,'Berhasil Menambah Data',$resultData);
		}
		return $result;
	}
	public function delete( $params = array() ){
		$this->db->where('kode_transaksi',$params['kode_transaksi']);
		$this->db->delete($this->_table);

		$CI =& get_instance();
		$CI->load->model('general_model');
		$result = $this->general_model->result(200,'Sukses Menghapus Data #'.$params['kode_transaksi']);
		return $result;
	}
	protected function getNewCode(){
		$q 						= $this->db->query("SELECT max(kode_transaksi) as lastCode from $this->_table")->row();
		if($q->lastCode){
			$lastCode 				= $q->lastCode;
			$nextValue	 			= explode('-', $lastCode)[1]+1;
			$nextValue				= str_pad($nextValue, 6,'0',STR_PAD_LEFT);
			$prefix 				= 'TRA-';
			$newCode 				= $prefix.$nextValue;
		}else{
			$newCode 				= 'TRA-' . str_pad(1, 6, '0', STR_PAD_LEFT);
		}
		return $newCode;
	}
}
