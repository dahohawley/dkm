<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jurnal_model extends CI_Model {
	protected $_table = 'jurnal';
	protected $_key;

	public function loadList($mode = 'LOAD_ALL',$params = array()){
		$recordsTotal 		= $this->db->count_all_results($this->_table);

		$this->db->join('coa','coa.kode_akun = jurnal.kode_akun');

		if(isset($params['search']) && $params['search']){
				$this->db->start_cache();

				$this->db->or_like('lower(kode_transaksi)',strtolower($params['search']),'both');
				$this->db->or_like('lower(kode_akun)',strtolower($params['search']),'both');
				$this->db->or_like('lower(posisi_dr_cr)',strtolower($params['search']),'both');
				$this->db->or_like('lower(nominal)',strtolower($params['search']),'both');
				$this->db->or_like('lower(tanggal_jurnal)',strtolower($params['search']),'both');

				$this->db->stop_cache();
		}

		if(isset($params['pagination'])){
			$limit 			= $params['pagination']['limit'];
			$offset 		= $params['pagination']['offset'];
		}else{
			$limit 			= 0;
			$offset 		= 0;
		}

		if(isset($params['order'])){
			$this->db->order_by($params['order']['column'],$params['order']['dir']);
		}

		$data 						= $this->db->get($this->_table,$limit,$offset);
		$recordsFiltered 			= $this->db->count_all_results($this->_table);

		$result 					= new stdClass;
		$result->data 				= $data->result();
		$result->recordsTotal 		= $recordsTotal;
		$result->recordsFiltered 	= $recordsFiltered;

		return $result;
	}
	public function save( $params = array() ){
		$CI =& get_instance();
		$CI->load->model('core/general_model');

		if(isset($params['id_jurnal']) && !empty($params['id_jurnal'])){
			$id_jurnal 				= $params['id_jurnal'];

			$this->db->where('id_jurnal',$id_jurnal);
			$this->db->set($params);
			$this->db->update($this->_table);

			$resultData = array(
				'id_jurnal' => $id_jurnal,
			);
			$result = $CI->general_model->result(200,'Berhasil Merubah data.',$resultData);
		}else{
			unset($params['id_jurnal']);
			$this->db->insert($this->_table,$params);

			$resultData = array(
				'insert_id' => $this->db->insert_id(),
			);
			$result = $CI->general_model->result(200,'Berhasil Menambah Data',$resultData);
		}
		return $result;
	}
	public function delete( $params = array() ){
		$this->db->where('id_jurnal',$params['id_jurnal']);
		$this->db->delete($this->_table);

		$CI =& get_instance();
		$CI->load->model('general_model');
		$result = $this->general_model->result(200,'Sukses Menghapus Data #'.$params['id_jurnal']);
		return $result;
	}
	protected function getNewCode(){
		$q 						= $this->db->query("SELECT max(id_jurnal) as lastCode from $this->_table")->row();
		$lastCode 				= $q->lastCode;
		$nextValue	 			= explode('-', $lastCode)[1]+1;
		$nextValue				= str_pad($nextValue, 6,'0',STR_PAD_LEFT);
		$prefix 				= 'JUR-';
		$newCode 				= $prefix.$nextValue;
		return $newCode;
	}
	public function createJurnal($params = array()){
		$result 	= $this->general_model->result();

		/* Save Transaksi */
		$this->load->model('accounting/transaksi_model');
		$dataTransaksi 	= $params['transaksi'];
		$dataTransaksi['tanggal_dibuat'] 	= date('Y-m-d');
		$saveTransaksi 	= $this->transaksi_model->save($dataTransaksi);
		
		foreach ($params['jurnal'] as $data ) {
			$data['posisi_dr_cr'] 	= strtoupper($data['posisi_dr_cr']);
			$data['kode_transaksi'] = $saveTransaksi->data['insert_id'];
			$data['tanggal_jurnal'] = date('Y-m-d');
			$this->save($data);
		}

		return $result;

	}
}
