<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages'] = array();

$autoload['libraries'] = array('database','form_validation','session','template');

$autoload['drivers'] = array();

$autoload['helper'] = array('url','monthname','formatrp','prefix','formatoption');

$autoload['config'] = array('custom');

$autoload['language'] = array();

$autoload['model'] = array('core/general_model','core/Layout_model');