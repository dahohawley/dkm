<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['framework'] = array(
	'default_controller' => 'Welcome',
);
$config['required_module'] = array(
	'core' => array(
		'name' => 'core',
		'url' => 'https://gitlab.com/itfwlib/core/-/archive/v1.5/core-v1.5.zip',
		'version' => 'v1.5'
	),
	'rbac' => array(
		'name' => 'rbac',
		'url' => 'https://gitlab.com/itfwlib/rbac/-/archive/v1.2/rbac-v1.2.zip',
		'version' => 'v1.2'
	),
	'user' => array(
		'name' => 'user',
		'url' => 'https://gitlab.com/itfwlib/user/-/archive/v1.1/user-v1.1.zip',
		'version' => 'v1.1'
	)
);
$config['required_library'] = array(
	'jquery.validate' => array(
		'name' => 'jquery.validate',
		'url' => 'https://gitlab.com/itfwlib/plugin/raw/master/jquery.validate.zip',
	),
	'jquery' => array(
		'name' => 'jquery',
		'url' => 'https://gitlab.com/itfwlib/plugin/raw/master/jquery.zip',
	),
	'font-awesome' => array(
		'name' => 'font-awesome',
		'url' => 'https://gitlab.com/itfwlib/plugin/raw/master/font-awesome.zip',
	),
);