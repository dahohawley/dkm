<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ITFW extends CI_Controller {
	public function index(){
		// die(APPPATH . '/modules/core/controllers/Controller.php');
		// check if its already installed or not
		if(file_exists(APPPATH . '/modules/core/controllers/Controller.php')){
			// if installed redirect to default controller from config ITFW.php
			if(file_exists(FCPATH.'installed.itfw')){
				$this->load->config('ITFW');
				$config = $this->config->item('framework');
				redirect($config['default_controller']);
			}else{
				$this->setupUser();
			}
		}else{
			$this->load->view('Framework/Setup');
		}
	}
	public function setup(){
		$result 		= new \stdClass;
		// loading module contain required module to install in framework
		$this->load->config('ITFW');

		// installing module
			$config = $this->config->item('required_module');
			$this->load->model('installation_model');
			foreach ($config as $config => $value) {
				$this->installation_model->installModule($value);
			}
		// installing library
			$config = $this->config->item('required_library');
			$this->load->model('installation_model');
			foreach ($config as $config => $value) {
				$url = $value['url'];
				$name = $value['name'];
				$this->installation_model->extractLibrary($url,$name);
			}

		$result->code = 200;
		$result->info = 'Success';
		echo '___';
		echo json_encode($result);
	}
	public function setup_old(){ 
		$result = new stdClass;
		if($data = system("cmd /c ".FCPATH.'setup.bat')){
			$result->code = 200;
			$result->info = 'Success';
		}else{
			$result->code = 501;
			$result->info = 'Error';
		}
		echo '___';
		echo json_encode($result);
	}
	public function setupUser(){
		$this->load->view('Framework/setupUser');
	}
	public function installDB(){
		$post = $this->input->post();
		$result = new stdClass;
		$this->load->library('form_validation');
		$this->form_validation->set_data($post);
		$this->form_validation->set_rules('host', 'Host', 'required');
		$this->form_validation->set_rules('db-username', 'Username', 'required');
		$this->form_validation->set_rules('schema_name', 'Schema Name', 'required');
		if ($this->form_validation->run() == FALSE) {
			$result->code = 401;
			$result->info = validation_errors();
		} else {
			$this->load->model('installation_model');
			// createDB Config
				$result = $this->installation_model->createDBConfig($post);
				if($result->code == 200){
					// create database autoloader
					$result = $this->installation_model->addDBAutoload();
					if($result->code == 200){
						// Install Resource
						$result = $this->installation_model->installResource();
					}else{
						$result->code = 502;
						$result->info = 'Failed changing DB Config';
					}
				}else{
					$result->code = 501;
					$result->info = 'Failed creating database';
				}
		}
		echo json_encode($result);
	}

	public function createUser(){
		$result = new stdClass;

		$post = $this->input->post();
		$post['id_role'] = 1;
		$post['account_status'] = 1;
		$this->load->model('rbac/account_model');
		$this->load->model('core/general_model');
		$createUser = $this->account_model->save($post);
		if($createUser){
			$result->code = 200;
			$result->info = 'Success';
			$result->data = $createUser;
		}else{
			$result->code = 501;
			$result->info = 'Error';
		}
		echo json_encode($result);
		return true;
	}
	public function createUserInformation(){
		$post = $this->input->post();
		$post['user_type_id'] = 1;

		$this->load->model('core/general_model');
		$this->load->model('user/user_model');
		$this->load->model('user/userMap_model');
		$saveUser = $this->user_model->save($post);
		$dataUserMap = array(
			'id_account' => 1,
			'id_user' => 1,
			'create_date' => date('Y-m-d'),
		);
		$saveUserMap = $this->userMap_model->save($dataUserMap);
		$result = new stdClass;
		if($saveUserMap['status']){
			$result->code = 200;
			$result->info = 'Success';
			// create installation file
			$my_file = 'installed.itfw';
			$handle = fopen($my_file, 'w');
		}else{
			$result->code = 501;
			$result->info = 'Error';
		}
		echo json_encode($result);
		return true;
	}
}
/* End of file ITFW.php */
/* Location: ./application/controllers/ITFW.php */