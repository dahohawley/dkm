<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . '/modules/core/controllers/Controller.php';

class Welcome extends Controller{
	public function __construct(){
		parent::__construct();
		$this->grant();
		$this->load->model('core/layout_model');
	}
	public function index(){
		$session = $this->session->userdata();
		if(isset($session['id_account'])){
			$this->dispatch(DEF_TEMPLATE_INSIDE,'dashboard');
		}else{
			$this->dispatch(DEF_TEMPLATE_OUTSIDE,'form_login');
		}
	}
	public function error_404(){
		echo '404';
	}
}