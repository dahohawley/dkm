<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Installation_model extends CI_Model {
	public function __construct(){
		parent::__construct();
	}
	public function getResult(){
		$result = new stdClass;
		$result->code = 200;
		$result->info = 'Success';
		$result->data = array();
		return $result;
	}
	public function createDBConfig($post){
		$result = new stdClass;
		$filePath = FCPATH.'application/config/database.php';
		// $fileOpen = fopen($filePath,'w');
		$file = file($filePath);
		$lines = array_map(function ($value) { return rtrim($value, PHP_EOL); }, $file);

		$lines[8] = "'hostname' => '".$post['host']."',";
		$lines[9] = "'username' => '".$post['db-username']."',";
		$lines[10] = "'password' => '".$post['db-password']."',";
		$lines[11] = "'database' => '".$post['schema_name']."',";
		
		$lines = array_values($lines);
		$content = implode(PHP_EOL, $lines);


		if(file_put_contents($filePath, $content)){
			$result->code = 200;
			$result->info = 'Success';
		}else{
			$result->code = 500;
			$result->info = 'Error Editing File';
		}
		return $result;
	}
	public function installResource(){
		$result = new stdClass;

		$CI = & get_instance();
	    $templine = '';
	    // Executing RBAC Resource
	    $sql = file_get_contents(APPPATH.'modules/rbac/resource/db.sql');
	    if(!$this->executeQuery($sql)){
	    	$result->code = 502;
	    	$result->info = 'Failed executing RBAC resource';
	    	return $result;
	    }


	    // Executing User Resource
	    $sql = file_get_contents(APPPATH.'modules/user/resource/db.sql');
	    if(!$this->executeQuery($sql)){
	    	$result->code = 502;
	    	$result->info = 'Failed executing User resource';
	    	return $result;
	    }

	    // Executing User Resource
	    if(file_exists(APPPATH.'modules/notification/resource/db.sql')){
		    $sql = file_get_contents(APPPATH.'modules/notification/resource/db.sql');
		    if(!$this->executeQuery($sql)){
		    	$result->code = 502;
		    	$result->info = 'Failed executing Notification resource';
		    	return $result;
		    }
	    }

	    $result->code = 200;
	    $result->info = 'Success';
	    return $result;
	}
	public function executeQuery($sql){
		$this->load->database();
		$sqls = explode(';', $sql);
		array_pop($sqls);

		foreach($sqls as $statement){
		    $statment = $statement . ";";
		    if(!$this->db->query($statement)){
		    	return false;
		    	break;
		    }   
		}
		return true;
	}
	public function addDBAutoload(){
		$result =  new stdClass;
		$filePath = FCPATH.'application/config/autoload.php';
		// $fileOpen = fopen($filePath,'w');
		$file = file($filePath);
		$lines = array_map(function ($value) { return rtrim($value, PHP_EOL); }, $file);

		$lines[5] = "\$autoload['libraries'] = array('database','form_validation','session','template');";
		$lines[15] = "\$autoload['model'] = array('core/general_model','core/Layout_model');";

		$lines = array_values($lines);
		$content = implode(PHP_EOL, $lines);
		
		if(file_put_contents($filePath, $content)){
			$result->code = 200;
			$result->info = 'Success';
		}else{
			$result->code = 500;
			$result->info = 'Error Editing File';
		}
		return $result;
	}
	public function installModule($config){
		$result = $this->getResult();
		if(!isset($config['name'])){
			$result->code = 401;
			$result->info = 'Module name is missing!';
		}else if(!isset($config['url'])){
			$result->code = 402;
			$result->info = 'Module url is missing!';
		}else {
			$url = $config['url'];
			$name = $config['name'];
			$version = $config['version'];
			$this->downloadUnzipGetContents($url,$name,$version);
		}
		return $result;
	}
	private function downloadUnzipGetContents($url = null, $name = 'Test',$version = 'v1.2') {
		// delete if module exists
		if(file_exists(APPPATH.'modules/'.$name)){
			$this->deleteDirectory(APPPATH.'modules/'.$name);
		}
	    // temp folder for temporary downloading file
	    if(!file_exists(FCPATH.'temp')){
            mkdir(FCPATH . 'temp', 0775, true);
	    }
	    // download file
	    if($file = @file_get_contents($url, false, 
	        stream_context_create(array(
	            'ssl' => array(
	                'verify_peer' => false,
	                'verify_peer_name' => false,
	            )
	        ))
	    )){
		    if(@file_put_contents(FCPATH.'/temp/'.$name.'.zip', $file)){
		    	$zip = new ZipArchive();
		    	$zipFile = FCPATH.'/temp/'.$name.'.zip';
		    	if($zip->open($zipFile)){
		            $libPath = APPPATH.'modules/';
		    	    $extract = @$zip->extractTo($libPath);
		    	    if(@$zip->extractTo($libPath)){
		    	        $zip->close();
		    	        // raw location itu nama file setelah extract. biasanya kebawa version nya contoh : core-v1.2
		    	        $rawLocation = $libPath.$name.'-'.$version;
		    	        $newName = $libPath.$name;
		    	        if(rename($rawLocation,$newName)){
		    	        	chmod($newName,0777);
		    	        	unlink($zipFile);
		    	        	return true;
		    	        }else{
		    	        	return false;
		    	        }
		    	        
		    	    } else {
		    	        error_log('1105 : invalid zip file');
		    	        return false;
		    	    }

		    	} else {
		    	    error_log('1104 : failed to extract libraries file');
		    	    return false;
		    	}
		    	/* Delete tmp file */
		    }
	    }
	}
	public function extractLibrary($url = null, $name = 'Test') {
	    // temp folder for temporary downloading file
	    if(!$url){
	    	$url = 'https://gitlab.com/itfwlib/plugin/raw/master/'.$name.'.zip';
	    }
	    $basePath = FCPATH.'assets/library/';
	    $fileName = $name.'.zip';

	    if(!file_exists(FCPATH.'temp')){
            mkdir(FCPATH . 'temp', 0775, true);
	    }
	    // download file
	    $file = @file_get_contents($url, false, 
	        stream_context_create(array(
	            'ssl' => array(
	                'verify_peer' => false,
	                'verify_peer_name' => false,
	            )
	        ))
	    );
	    // get http response code
	    	$headers = get_headers($url);
    	    $httpResponseCode = substr($headers[0], 9, 3);
    	if($httpResponseCode == '200'){
		    if($file = @file_get_contents($url, false, 
		        stream_context_create(array(
		            'ssl' => array(
		                'verify_peer' => false,
		                'verify_peer_name' => false,
		            )
		        ))
		    )){	
			    if(@file_put_contents(FCPATH.'/temp/'.$fileName, $file)){
			    	if(file_exists($basePath.$fileName)){
						$this->deleteDirectory($basePath.$fileName);
					}
			    	$zip = new ZipArchive();
			    	$zipFile = FCPATH.'/temp/'.$fileName;
			    	if($open = $zip->open($zipFile)){
			    	    if(@$zip->extractTo($basePath)){
			    	        $zip->close();
			    	        // raw location itu nama file setelah extract. biasanya kebawa version nya contoh : core-v1.2
		    	        	unlink($zipFile);
		    	        	return true;
			    	    } else {
			    	        error_log('1105 : invalid zip file');
			    	        return false;
			    	    }

			    	} else {
			    	    error_log('1104 : failed to extract libraries file');
			    	    return false;
			    	}
			    	/* Delete tmp file */
			    }
		    }
    	}else{
    		return false;
    	}
	}
	private function deleteDirectory($dir) {
	    if (!file_exists($dir)) {
	        return true;
	    }

	    if (!is_dir($dir)) {
	        return unlink($dir);
	    }

	    foreach (scandir($dir) as $item) {
	        if ($item == '.' || $item == '..') {
	            continue;
	        }

	        if (!$this->deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
	            return false;
	        }

	    }

	    return rmdir($dir);
	}
	

}

/* End of file installation_model.php */
/* Location: ./application/models/installation_model.php */