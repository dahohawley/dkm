/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 10.1.37-MariaDB : Database - itfw
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `notification` */

DROP TABLE IF EXISTS `notification`;

CREATE TABLE `notification` (
  `id_notification` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `messages` text NOT NULL,
  `action` text,
  `receiver` int(11) DEFAULT NULL,
  `receive_date` datetime NOT NULL,
  `read_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_notification`),
  KEY `receiver` (`receiver`),
  CONSTRAINT `notification_ibfk_1` FOREIGN KEY (`receiver`) REFERENCES `rbac_account` (`id_account`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `notification` */

insert  into `notification`(`id_notification`,`title`,`messages`,`action`,`receiver`,`receive_date`,`read_date`) values 
(2,'Test Notification','This is just a notification from me to me ','notification/form/notification',1,'2019-03-11 00:00:00','2019-03-12 20:19:28'),
(3,'Test Notification','This is just a notification from me to me ','notification/form/notification',1,'2019-03-11 22:47:21','2019-03-12 20:19:32');

/*Table structure for table `rbac_account` */

DROP TABLE IF EXISTS `rbac_account`;

CREATE TABLE `rbac_account` (
  `id_account` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(40) NOT NULL,
  `password` text NOT NULL,
  `email` text,
  `id_role` int(11) NOT NULL,
  `account_status` char(1) NOT NULL,
  PRIMARY KEY (`id_account`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `rbac_account` */

insert  into `rbac_account`(`id_account`,`username`,`password`,`email`,`id_role`,`account_status`) values 
(1,'superadmin','$2y$10$S7YwE1xY6AQIJTS.KWkh7Ou9ff7AkExFWFgR1KHPngGyOoX2suonO','superadmin@izi-techno.com',1,'1');

/*Table structure for table `rbac_menu` */

DROP TABLE IF EXISTS `rbac_menu`;

CREATE TABLE `rbac_menu` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `label` text NOT NULL,
  `icon` text NOT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `rbac_menu` */

insert  into `rbac_menu`(`id_menu`,`label`,`icon`) values 
(1,'Role Based Access Controll','fa fa-gear'),
(2,'User','fa fa-user'),
(3,'Notification','fa fa-bell');

/*Table structure for table `rbac_privileges` */

DROP TABLE IF EXISTS `rbac_privileges`;

CREATE TABLE `rbac_privileges` (
  `id_privileges` int(11) NOT NULL AUTO_INCREMENT,
  `id_role` text NOT NULL,
  `id_menu` int(11) NOT NULL,
  `id_submenu` int(11) NOT NULL,
  PRIMARY KEY (`id_privileges`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

/*Data for the table `rbac_privileges` */

insert  into `rbac_privileges`(`id_privileges`,`id_role`,`id_menu`,`id_submenu`) values 
(13,'1',1,5),
(14,'1',1,6),
(15,'1',1,7),
(16,'1',1,8),
(17,'1',2,9),
(18,'1',2,10),
(19,'1',2,11),
(20,'1',2,12),
(21,'1',3,13);

/*Table structure for table `rbac_role` */

DROP TABLE IF EXISTS `rbac_role`;

CREATE TABLE `rbac_role` (
  `id_role` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  PRIMARY KEY (`id_role`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `rbac_role` */

insert  into `rbac_role`(`id_role`,`name`) values 
(1,'Superadmin');

/*Table structure for table `rbac_submenu` */

DROP TABLE IF EXISTS `rbac_submenu`;

CREATE TABLE `rbac_submenu` (
  `id_submenu` int(11) NOT NULL AUTO_INCREMENT,
  `id_menu` int(11) NOT NULL,
  `label` text NOT NULL,
  `url` text NOT NULL,
  `icon` text NOT NULL,
  PRIMARY KEY (`id_submenu`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `rbac_submenu` */

insert  into `rbac_submenu`(`id_submenu`,`id_menu`,`label`,`url`,`icon`) values 
(1,9,'Account','rbac/form/account',''),
(2,9,'Role','rbac/form/role',''),
(3,9,'Privileges','rbac/form/privileges',''),
(4,9,'Menu','rbac/form/menu',''),
(5,1,'Menu','rbac/form/menu',''),
(6,1,'Role','rbac/form/role',''),
(7,1,'Privileges','rbac/form/privileges',''),
(8,1,'Account','rbac/form/account',''),
(9,2,'User','user/form/user',''),
(10,2,'User Maping','user/form/userMap',''),
(11,2,'User Group','user/form/userGroup',''),
(12,2,'User Type','user/form/userType',''),
(13,3,'Notification','notification/form/notification','');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(40) NOT NULL,
  `last_name` varchar(40) NOT NULL,
  `gender` char(1) NOT NULL,
  `birth_date` date NOT NULL,
  `birth_place` varchar(45) NOT NULL,
  `user_type_id` int(11) NOT NULL,
  `create_date` date NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`id_user`,`first_name`,`last_name`,`gender`,`birth_date`,`birth_place`,`user_type_id`,`create_date`) values 
(17,'Ramdhani','Lukman','m','1997-02-06','Bandung',31,'2019-03-06');

/*Table structure for table `user_detail` */

DROP TABLE IF EXISTS `user_detail`;

CREATE TABLE `user_detail` (
  `id_user_detail` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `picture` text,
  `xs1` text,
  `xs2` text,
  `xs3` text,
  `xs4` text,
  `xs5` text,
  `xn1` int(11) DEFAULT NULL,
  `xn2` int(11) DEFAULT NULL,
  `xn3` int(11) DEFAULT NULL,
  `xd1` date DEFAULT NULL,
  `xd2` date DEFAULT NULL,
  `xd3` date DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  PRIMARY KEY (`id_user_detail`),
  KEY `FK_IDUSER_USERDETAIL` (`id_user`),
  CONSTRAINT `FK_IDUSER_USERDETAIL` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `user_detail` */

insert  into `user_detail`(`id_user_detail`,`id_user`,`picture`,`xs1`,`xs2`,`xs3`,`xs4`,`xs5`,`xn1`,`xn2`,`xn3`,`xd1`,`xd2`,`xd3`,`last_update`) values 
(7,17,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-03-06 00:00:00');

/*Table structure for table `user_file` */

DROP TABLE IF EXISTS `user_file`;

CREATE TABLE `user_file` (
  `id_user_file` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `file1` text,
  `file2` text,
  `file3` text,
  `file4` text,
  `file5` text,
  `picture1` text,
  `picture2` text,
  `picture3` text,
  `picture4` text,
  `picture5` text,
  PRIMARY KEY (`id_user_file`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user_file` */

/*Table structure for table `user_group` */

DROP TABLE IF EXISTS `user_group`;

CREATE TABLE `user_group` (
  `user_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_group_name` text NOT NULL,
  `create_date` date NOT NULL,
  PRIMARY KEY (`user_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user_group` */

/*Table structure for table `user_group_member` */

DROP TABLE IF EXISTS `user_group_member`;

CREATE TABLE `user_group_member` (
  `id_user_group_member` int(11) NOT NULL AUTO_INCREMENT,
  `user_group_id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `create_date` date NOT NULL,
  PRIMARY KEY (`id_user_group_member`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user_group_member` */

/*Table structure for table `user_map` */

DROP TABLE IF EXISTS `user_map`;

CREATE TABLE `user_map` (
  `id_user_map` int(11) NOT NULL AUTO_INCREMENT,
  `id_account` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `create_date` date NOT NULL,
  PRIMARY KEY (`id_user_map`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `user_map` */

insert  into `user_map`(`id_user_map`,`id_account`,`id_user`,`create_date`) values 
(8,1,17,'2019-03-06');

/*Table structure for table `user_type` */

DROP TABLE IF EXISTS `user_type`;

CREATE TABLE `user_type` (
  `user_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type_name` varchar(35) NOT NULL,
  PRIMARY KEY (`user_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

/*Data for the table `user_type` */

insert  into `user_type`(`user_type_id`,`user_type_name`) values 
(31,'Superadmin');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
